This page is  a work in progress, ultimately intended to provide a curated list of active LGBT resources. While the nature of lists means that not every resource on here is guaranteed to work or continue to exist at all times, the hope is that this list will provide enough resources that you'll find at least something on it useful. All links on this page, unless noted otherwise, are known to work as of ***January 4, 2020***. Should you be unable to connect to one of the sites, try accessing them over HTTP instead of HTTPS. If you don't have wiki permissions, please send a message or @deing if you find a dead link or have additional resources to suggest for this page.

---

# Generalized Resources
## Transgender Resources
- [World Professional Association for Transgender Health](https://www.wpath.org/)
- [@FantasyCookie17's Global Transgender Resources Registry](https://gtrr.artemislena.eu/)

## Nonbinary Resources
- [Nonbinary Wiki](https://nonbinary.wiki/wiki/Main_Page)


## Bisexuality Resources
- [Bisexual Resource Center](https://biresource.org/)

## Asexuality Resources
- [AVEN Community](https://www.asexuality.org/en/)

## HIV/AIDS Resources
- [CDC](http://www.cdc.gov/hiv/)
- [NHS](https://www.nhs.uk/conditions/hiv-and-aids/)
- [Avert.org](https://www.avert.org/)
- [amFAR](http://www.amfar.org/)
- [San Francisco Aids Foundation](http://www.sfaf.org/)
- [AIDS Healthcare Foundation](http://www.aidshealth.org/)
- [WebMD](https://www.webmd.com/hiv-aids/guide/hiv-aids-resources-index)

### Finding HIV/AIDS Testing
- [Canada](https://www.canada.ca/en/public-health/services/diseases/hiv-aids.html#find)
- [USA](http://hivtest.cdc.gov/)

# National Resources

## United States
### General Resources
- [Trans Lifeline](https://www.translifeline.org/)
- [Refuge Restrooms](https://www.refugerestrooms.org/)

### Charities, Support Groups, etc.
- [Human Rights Campaign](https://hrc.org/)
- [American Civil Liberties Union](https://www.aclu.org/)
- [Lambda Legal](https://www.lambdalegal.org/)
- [GLBTQ Legal Advocates & Defenders](https://www.glad.org/)
- [Transgender Law Center](https://transgenderlawcenter.org/)
- [National Center for Transgender Equality](https://transequality.org/)
- [National Center for Lesbian Rights](http://www.nclrights.org/)
- [LGBT Victory Fund](https://victoryfund.org/)
- [GLAAD](http://glaad.org/)
- [The Trevor Project](https://www.thetrevorproject.org/)
- [Gay, Lesbian, Bisexual, and Transgender Round Table](http://www.ala.org/rt/glbtrt)

## United Kingdom
### General Resources
- [Gender Construction Kit](http://genderkit.org.uk/)
- [Transition FtM UK](http://transitionftmuk.co.uk/) (currently undergoing maintenance)

### Charities, Support Groups, etc.
- [Wiki List](http://www.gires.org.uk/the-wiki), comprehensively lists UK support groups
- [The Angels](http://www.theangels.co.uk), an internet support group for trans women
- [Mermaids](http://www.mermaidsuk.org.uk/), support for transgender teens under 19
- [Galop](http://www.galop.org.uk/), charity place to report anti LGBT crimes
- [GIRES](http://www.gires.org.uk/), gender identity research and education
- [Press For Change](http://www.pfc.org.uk), legal and civil rights support
- [Regard](http://www.regard.org.uk/), support for disabled LGBT people
- [The Samaritans](http://www.samaritans.org/) or call **116 123** Free from any phone in UK or ROI; support for people who need someone to talk to. Available 24/7 365 days a year.

### Gender Identity Clinics
#### England
- The Laurels, Exeter: [West of England Specialist Gender Identity Clinic](https://www.dpt.nhs.uk/our-services/gender-identity)
- Leeds: [Leeds Gender Identity Service](https://www.leedsandyorkpft.nhs.uk/our-services/services-list/gender-identity-service/)
- [PRIVATE] London: [Harley Street Gender Clinic](http://www.harleystgenderclinic.com/)
- Hammersmith, London: [Charing Cross Gender Identity Clinic](https://gic.nhs.uk/)
- [PRIVATE] London: [GenderCare](http://www.gendercare.co.uk/)
- London: [Gender Identity Development Service aka GIDS](http://gids.nhs.uk/) [Serves <18]
- London: [London Transgender Clinic](https://www.thelondontransgenderclinic.uk/) - [Private HRT and Surgery]
- Newcastle: [Northern Regional Gender Dysphoria Service (Northumberland, Tyne and Wear)](https://www.ntw.nhs.uk/services/northern-region-gender-dysphoria-service-specialist-service-walkergate-park/) (expired certificate but site is still up)
- Northampton: [Northampton Gender Identity Clinic](https://www.nhft.nhs.uk/gender-identity-clinic)
- Nottingham: [Nottingham Centre for Transgender Health](https://www.nottinghamshirehealthcare.nhs.uk/nottingham-centre-for-transgender-health)
- Sheffield: [Porterbrook Clinic](https://www.shsc.nhs.uk/services/gender-identity-clinic)  

#### Northern Ireland
- Belfast: [Belfast Brackenburn Clinic](http://www.belfasttrust.hscni.net/BrackenburnClinic.htm) [Serves <18s and adults]

#### Scotland
- Edinburgh: [Chalmers Centre Clinic](https://www.lothiansexualhealth.scot.nhs.uk/Services/GIC/Pages/default.aspx)
- Glasgow: [Sandyford Gender Identity services](http://www.sandyford.org/)

#### Online
- [PRIVATE] [GenderGP](https://gendergp.co.uk/)

## Italy
- [Wikisessualità](https://www.wikisessualita.org/)
  - [Lista di associazioni LGBT](https://www.wikisessualita.org/wiki/Lista_di_associazioni_LGBT) ([archive.is snapshot](https://archive.fo/X90jk))
  - [Lista di endocrinologi (transizione di genere)](https://www.wikisessualita.org/wiki/Lista_di_endocrinologi_%28transizione_di_genere%29) (returns "Access Denied"; no archived version; it *might* hopefully come back online)
- [Ftm : Centri WPATH (e Onig) in Italia](https://ftmnotizieutili.blogspot.com/2013/09/centri-wpath-e-onig-in-italia.html) ([archive.is snapshot](https://archive.fo/jRSN4); last updated on 2013)
- [ONIG - Osservatorio Nazionale sull'Identità di Genere](http://www.onig.it/drupal8/)

### DISEM
_Viale Benedetto XV, 6 - 16132 Genova, Italy_
**Centralino:** +39 010 5551

Might have changed name, still active as of 2018. Seemingly the only Italian hospital following the WPATH protocol. Large backlog of people, expect ~3 weeks to get an appointment. The phone numbers available online about it are mostly wrong, and calling [S. Martino's](http://www.ospedalesanmartino.it/) centralino (+39 010 5551) is probably the best option.

## Germany

- [Regenbogenportal](https://www.regenbogenportal.de/): General information site run by the Federal Ministry for Family Affairs. Content is also available in [English](https://www.regenbogenportal.de/meta-navigation/english/), [Turkish](https://www.regenbogenportal.de/meta-navigation/tuerkce/), [Arabic](https://www.regenbogenportal.de/meta-navigation/%F8%A7%F9%84%F8%B9%F8%B1%F8%A8%F9%8A%F8%A9/), [Spanish](https://www.regenbogenportal.de/meta-navigation/espanol/) and simple German.

---

Resource dumps to be evaluated:

* https://genderqueerid.com/gq-links
