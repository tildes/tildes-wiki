>Resources for watching, reading, discovering, and learning about anime and manga on the internet.

## Streaming Services
Use [because.moe](https://because.moe/) to see what services a particular anime is on.

|Service     |Description|
|:------------|:---------------|
|[Crunchyroll](https://www.crunchyroll.com/)|The default anime streaming site for most purposes due to dominance in securing simulcasts and decent backlog.  Limited library outside of the US.  Primarily for subtitled anime.          |
|[Funimation](https://www.funimation.com/)           |Decent library, many included anime are exclusive.  Library skews older than other services.  Particularly useful for dubbed anime.          |
|[Netflix](https://www.netflix.com/browse/genre/7424)     |Limited library, occasionally have exclusive currently airing anime but do not simulcast, instead they release all at once after completion.          |
|[Hulu](https://www.hulu.com/genre/anime-a8767e06-4b88-4ea3-b688-33f864a5c424)     |Solid backlog with less of a skew toward recency than other services.          |
|Amazon Prime Video [TV Anime](https://www.amazon.com/s?i=instant-video&bbn=2858778011&rh=n%3A2858778011%2Cp_85%3A1&adult-product=0&field-theme_browse-bin=2650364011&field-ways_to_watch=12007865011&p_n_entity_type=14069185011&qs-av_request_type=4&qs-is-prime-customer=0&ref=atv_hm_hom_c_eofolo_66_smr) [Anime Movies](https://www.amazon.com/s?i=instant-video&bbn=2858778011&rh=n%3A2858778011%2Cp_85%3A1&adult-product=0&field-theme_browse-bin=2650364011&field-ways_to_watch=12007865011&p_n_entity_type=14069184011&qs-av_request_type=4&qs-is-prime-customer=0&ref=atv_hm_hom_c_ml2qg5_67_smr)     |Limited library, still has many classics that are not on other services.          |
|[VRV](https://vrv.co/)     | Crunchyroll, FUNimation and a few others in one, currently US-only(?), some anime are not shared         |
|[Hidive](https://www.hidive.com/welcome)     |          |
|[Yahoo View](https://view.yahoo.com/browse/tv/genre/anime)     |          |
|[TubiTV](https://tubitv.com/category/anime)     |  Mostly old anime but decent catalog        |
|[Viewster](https://www.viewster.com/)     |  "Moving"        |


## Manga Reading

|Service     |Description|
|:------------|:---------------|
|[Viz](https://www.viz.com/shonenjump) | Extensive library, new chapters for the former Weekly Shonen Jump are free to access but the back catalog requires a paid subscription.          |
|[Manga Plus](https://mangaplus.shueisha.co.jp/updates) | |
|[Crunchyroll](https://www.crunchyroll.com/comics/manga)|Extremely limited library, but comes with the CR subscription.          |

## Databases and Tracking Sites

|Site     |Description|
|:------------|:---------------|
|[AniDB](https://anidb.net) | Essential all-encompassing anime database.  If info has been compiled anywhere, it'll be here. |
|[AniList](https://anilist.co/home)| Tracking and community site, integrates with [AniChart](https://anichart.net/). |
|[MyAnimeList](https://www.animeherald.com/) | Historically the default tracking and news site, has struggled lately due to security issues.  A jack of all trades.          |
|[Kitsu](https://kitsu.io/)|Tracking site.          |
|[Anime Planet](https://www.anime-planet.com/) | |
|[Baka-Updates Manga](https://www.mangaupdates.com/) | Database for manga and webtoons. |


## News

|Site     |Description|
|:------------|:---------------|
|[AnimeNewsNetwork](https://www.animenewsnetwork.com/)|The default source for most anime news in English. |
|[Anime Herald](https://www.animeherald.com/)|News, interviews, and editorials. |

## Reviews, Opinions, and Essays

|Site / Person     |Description|
|:------------|:---------------|
|[AnimeFeminist](https://www.animefeminist.com/)|Opinion articles, reviews, and recommendations from a feminist angle. |
|[Glass Reflection / Arkada](https://www.youtube.com/user/GRArkada/videos) | YouTube reviewer. |
|[The Canpia Effect](https://www.youtube.com/user/TheCanipaEffect/videos) | YouTube video essayist, has a particularly strong focus on highlighting anime staff and following careers |
|[ThePedanticRomantic](https://www.youtube.com/channel/UCAIKWqE5-SjyeL1wQDaOb0g/videos) | YouTube video essayist, often focused on social justice or related issues in anime. |
|[The Cart Driver](http://thecartdriver.com/) / [Day with the Cart Driver](http://daywith.thecartdriver.com/) | Well-loved anime blogger with a very large back catalog of reviews, retrospectives, and opinions worth diving into.|
|[Under The Scope](https://www.youtube.com/channel/UCI91jQdeMzuQRgg4VOSZZmw/videos) | YouTube video essayist and reviewer. |
|[Pause and Select](https://www.youtube.com/channel/UCfpDNSkMPnmMwZHhUyplbZg/videos) | YouTube video essayist with a more "academic" and serious approach than most. |
|[Mother's Basement](https://www.youtube.com/channel/UCBs2Y3i14e1NWQxOGliatmg/videos) | YouTube video essayist and reviewer, work is much lighter than the others listed here. |


## Misc Tools and Toys

|Site     |Description|
|:------------|:---------------|
|[because.moe](https://because.moe/) | Use to determine what legal streaming services have a particular anime. |
|[AniChart](https://anichart.net/)| Use to browse the current season of anime and those that are upcoming.  Primarily for planning what anime to watch in a new season |
|[Sakugabooru](https://www.sakugabooru.com/post)|Compiles [sakuga](https://en.wiktionary.org/wiki/sakuga), come here to explore and discover particiularly beautiful cuts of animation. |
| [Spin.moe](https://spin.moe/) | Choose a random anime to watch from your AniList, Kitsu, or MAL profile. |
| [Themes.moe](https://themes.moe/) | Watch/listen to your favorite anime openings and endings. You can search by AniList or MAL profiles too. |
| [Monthly.moe](https://www.monthly.moe/) | Automated calendar showing airtimes for current anime series. It also shows release dates for anison singles and features AniList/Taiga integration. |
