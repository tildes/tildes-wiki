>Posts about anime and the related media/genres including TV shows, manga, movies, and games.

## Recurring Topics

### What have you been watching this week?

> Here's where you find out if you're the only one who's rewatched K-ON for the sixth time in a row.

Thread currently maintained by @Whom and @Cleb. All weekly threads can be found [here (tag `recurring` in ~anime)](https://tildes.net/~anime?order=new&tag=recurring), directly on Tildes.

### This Week in Anime (Archive)

> ~ Welcome to the storage closet ~

Threads currently maintained by @clerical_terrors. The last *This Week in Anime* topic was posted on December 1st, 2018. All threads can be found [here (tag `currently airing` in ~anime)](https://tildes.net/~anime?order=new&tag=currently_airing), directly on Tildes.
