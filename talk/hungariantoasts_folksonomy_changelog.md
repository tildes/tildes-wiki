This page is meant to keep track of the various changes I make to the [folksonomy](https://en.wikipedia.org/wiki/Folksonomy) across the site.

2021-07-29: Retired [`federated`](https://tildes.net/?tag=federated) in favor of [`federation`](https://tildes.net/?tag=federation).

2021-04-09: Retired [`chips`](https://tildes.net/?tag=chips) in favor of [`semiconductors`](https://tildes.net/?tag=semiconductors). Someone was going to post a topic about potato chips... one day.

2024-09-27: Retired [`left`](https://tildes.net/?tag=left), [`leftist`](https://tildes.net/?tag=leftist), [`leftists`](https://tildes.net/?tag=leftists) and [`leftism`](https://tildes.net/?tag=leftism) in favor of [`left wing`](https://tildes.net/?tag=left_wing).

2024-09-29: Retired [`graphics cards`](https://tildes.net/?tag=graphics_cards) in favor of [`gpu`](https://tildes.net/?tag=gpu).
