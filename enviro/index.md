>Topics related to the environment - conservation, recycling, alternate energy, etc.

## Notable Discussion Topics

  * [What are you doing to reduce your impact?](https://tild.es/7bo)
  * [What recent changes have you made to reduce your environmental impact?](https://tild.es/ajd)
  * [The next president of the US makes climate change their top priority. What should be their first actions?](https://tild.es/7ox)
  * [What are the primary pressures leading us toward collapse?](https://tild.es/8sv)
  * [On Thorium Power (and the 'hype' thereof)](https://tild.es/9yb)
