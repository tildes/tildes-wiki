> The ~music Listening Club chooses an album to collectively listen to and discuss every week, to foster a sense of community and broaden their musical perspectives. Every odd‐numbered week, the album will be an acclaimed/“important” albums; every even‐numbered week, it will be more “obscure”. It was started (and is managed by) @Whom.

### Notice
The last Listening Club was posted on December 6th, 2018.

## Archive

 - [Week 1](https://tildes.net/~music/2m4/music_listening_club_1_ok_computer): Radiohead — OK Computer
 - [Week 2](https://tildes.net/~music/2vb/music_listening_club_2_dancing_time): The Funkees — Dancing Time: The Best of Eastern Nigeria’s Afro Rock Exponents 1973-77
 - [Week 3](https://tildes.net/~music/33v/music_listening_club_3_enter_the_wu_tang_36_chambers): Wu‐Tang Clan — Enter the Wu‐Tang (36 Chambers)
 - [Week 4](https://tildes.net/~music/3b1/music_listening_club_4_weather_systems): Anathema — Weather Systems
 - [Week 5](https://tildes.net/~music/3l5/music_listening_club_5_a_love_supreme): John Coltrane — A Love Supreme
 - [Week 6](https://tildes.net/~music/3x0/music_listening_club_6_postcards): Meadowlark — Postcards
 - [Week 7](https://tildes.net/~music/4a6/music_listening_club_7_highway_61_revisited): Bob Dylan — Highway 61 Revisited
 - [Week 8](https://tildes.net/~music/4p5/music_listening_club_8_exuma): Exuma — Exuma
 - [Week 9](https://tildes.net/~music/53d/music_listening_club_9_the_man_machine): Kraftwerk — The Man‐Machine
 - [Week 10](https://tildes.net/~music/5hm/music_listening_club_10_%E4%B8%96%E7%95%8C%E3%81%8B%E3%82%89%E8%A7%A3%E6%94%BE%E3%81%95%E3%82%8C): ░▒▓新しいデラックスライフ▓▒░ — ▣世界から解放され▣
 - [Week 11](https://tildes.net/~music/5rl/music_listening_club_11_lift_yr_skinny_fists_like_antennas_to_heaven): Godspeed You! Black Emperor  —  Lift Yr. Skinny Fists Like Antennas to Heaven!
 - [Week 12](https://tildes.net/~music/61q/music_listening_club_12_heart_of_my_own): Basia Bulat — Heart of My Own
 - [Week 13](https://tildes.net/~music/6ce/music_listening_club_13_paranoid): Black Sabbath — Paranoid
 - [Week 14](https://tildes.net/~music/6mq/music_listening_club_14_public_strain): Women — Public Strain
 - [Week 15](https://tildes.net/~music/6vx/music_listening_club_15_songs_in_the_key_of_life): Stevie Wonder — Songs in the Key of Life
 - [Week 16](https://tildes.net/~music/750/music_listening_club_16_body_mind_spirit): Body, Mind & Spirit — Black Renaissance
 - [Week 17](https://tildes.net/~music/7dt/music_listening_club_17_unknown_pleasures): Unknown Pleasures — Joy Division
 - [Week 18](https://tildes.net/~music/7mz/music_listening_club_18_venture_ep): Venture EP — Televisor
 - [Week 19](https://tildes.net/~music/7vj/music_listening_club_19_the_beatles_the_white_album): The Beatles (aka The White Album) — The Beatles
 - [Week 20](https://tildes.net/~music/856/music_listening_club_20_washing_machine): Washing Machine — Sonic Youth
 - [Week 21](https://tildes.net/~music/8c3/music_listening_club_21_at_folsom_prison): At Folsom Prison — Johnny Cash
 - [Week 22](https://tildes.net/~music/8i1/music_listening_club_22_survival): Survival — Bob Marley & The Wailers
 - [Week 23](https://tildes.net/~music/8p3/music_listening_club_23_dark_side_of_the_moon): Dark Side of the Moon — Pink Floyd
 - [Week 24](https://tildes.net/~music/8up/music_listening_club_24_intro_to_shoegaze): Intro to Shoegaze (_New format introduced_)
 - [Week 25](https://tildes.net/~music/8zn/music_listening_club_25_1992): The Year of 1992
 
All Listening Club posts are [here](https://tildes.net/~music?tag=listening_club&order=new), directly on Tildes.
