## RYM Hierarchy

(currently working on copying RYM hierarchy here)

### Ambient

<details><summary>click to open/close</summary>
<table>
<tbody>
 <tr><td>Ambient</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Ambient</td><td>Ambient Dub</td><td>&nbsp;</td></tr>
 <tr><td>Ambient</td><td>Dark Ambient</td><td>&nbsp;</td></tr>
 <tr><td>Ambient</td><td>Dark Ambient</td><td>Black Ambient</td></tr>
 <tr><td>Ambient</td><td>Dark Ambient</td><td>Ritual Ambient</td></tr>
 <tr><td>Ambient</td><td>Space Ambient</td><td>&nbsp;</td></tr>
 <tr><td>Ambient</td><td>Tribal Ambient</td><td></td></tr>
</tbody></table>
</details>

### Blues

<details><summary>click to open/close</summary>
<table>
<tbody>
 <tr><td>Blues</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Blues</td><td>Acoustic Blues</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Blues</td><td>Acoustic Blues</td><td>Acoustic Chicago Blues</td><td>&nbsp;</td></tr>
 <tr><td>Blues</td><td>Acoustic Blues</td><td>Country Blues</td><td>&nbsp;</td></tr>
 <tr><td>Blues</td><td>Acoustic Blues</td><td>Country Blues</td><td>Acoustic Texas Blues</td></tr>
 <tr><td>Blues</td><td>Acoustic Blues</td><td>Country Blues</td><td>Delta Blues</td></tr>
 <tr><td>Blues</td><td>Acoustic Blues</td><td>Country Blues</td><td>Piedmont Blues</td></tr>
 <tr><td>Blues</td><td>Jug Band</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Blues</td><td>Boogie Woogie</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Blues</td><td>Electric Blues</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Blues</td><td>Electric Blues</td><td>British Blues</td><td>&nbsp;</td></tr>
 <tr><td>Blues</td><td>Electric Blues</td><td>Chicago Blues</td><td>&nbsp;</td></tr>
 <tr><td>Blues</td><td>Electric Blues</td><td>Electric Texas Blues</td><td>&nbsp;</td></tr>
 <tr><td>Blues</td><td>Electric Blues</td><td>Swamp Blues</td><td>&nbsp;</td></tr>
 <tr><td>Blues</td><td>Hill Country Blues</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Blues</td><td>Jump-Blues</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Blues</td><td>New Orleans Blues</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Blues</td><td>Piano Blues</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Blues</td><td>Soul Blues</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Blues</td><td>Vaudeville Blues</td><td>&nbsp;</td><td></td></tr>
</tbody></table>
</details>

### Classical Music

<details><summary>click to open/close</summary>
<table>
<tbody>
 <tr><td>Classical Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Arabic Classical Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Arabic Classical Music</td><td>Al-Maqam Al-Iraqi</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Arabic Classical Music</td><td>Andalusian Classical Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Brazilian Classical Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>East Asian Classical Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>East Asian Classical Music</td><td>Chinese Classical Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>East Asian Classical Music</td><td>Chinese Classical Music</td><td>Baisha Xiyue</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>East Asian Classical Music</td><td>Chinese Classical Music</td><td>Chinese Opera</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>East Asian Classical Music</td><td>Chinese Classical Music</td><td>Chinese Opera</td><td>Cantonese Opera</td></tr>
 <tr><td>Classical Music</td><td>East Asian Classical Music</td><td>Chinese Classical Music</td><td>Chinese Opera</td><td>Peking Opera</td></tr>
 <tr><td>Classical Music</td><td>East Asian Classical Music</td><td>Chinese Classical Music</td><td>Chinese Opera</td><td>Shaoxing Opera</td></tr>
 <tr><td>Classical Music</td><td>East Asian Classical Music</td><td>Chinese Classical Music</td><td>Dongjing</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>East Asian Classical Music</td><td>Chinese Classical Music</td><td>Yayue</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>East Asian Classical Music</td><td>Japanese Classical Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>East Asian Classical Music</td><td>Japanese Classical Music</td><td>Gagaku</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>East Asian Classical Music</td><td>Japanese Classical Music</td><td>Heikyoku</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>East Asian Classical Music</td><td>Japanese Classical Music</td><td>Jiuta</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>East Asian Classical Music</td><td>Japanese Classical Music</td><td>Jōruri</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>East Asian Classical Music</td><td>Japanese Classical Music</td><td>Nagauta</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>East Asian Classical Music</td><td>Japanese Classical Music</td><td>Noh</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>East Asian Classical Music</td><td>Japanese Classical Music</td><td>Shomyo</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>East Asian Classical Music</td><td>Japanese Classical Music</td><td>Soukyoku</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>East Asian Classical Music</td><td>Japanese Classical Music</td><td>Soukyoku</td><td>Danmono</td></tr>
 <tr><td>Classical Music</td><td>East Asian Classical Music</td><td>Japanese Classical Music</td><td>Soukyoku</td><td>Kumiuta</td></tr>
 <tr><td>Classical Music</td><td>East Asian Classical Music</td><td>Korean Classical Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>East Asian Classical Music</td><td>Korean Classical Music</td><td>Aak</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>East Asian Classical Music</td><td>Korean Classical Music</td><td>Dang-ak</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>East Asian Classical Music</td><td>Korean Classical Music</td><td>Hyang-ak</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>East Asian Classical Music</td><td>Korean Classical Music</td><td>Jeong-ak</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>East Asian Classical Music</td><td>Vietnamese Classical Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>East Asian Classical Music</td><td>Vietnamese Classical Music</td><td>Vietnamese Opera</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Latin Classical Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Modern Classical</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Modern Classical</td><td>Expressionism</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Modern Classical</td><td>Futurism</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Modern Classical</td><td>Indeterminacy</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Modern Classical</td><td>Microtonal</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Modern Classical</td><td>Minimalism</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Modern Classical</td><td>New Complexity</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Modern Classical</td><td>Post-Minimalism</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Modern Classical</td><td>Post-Minimalism</td><td>Totalism</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Modern Classical</td><td>Serialism</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Modern Classical</td><td>Serialism</td><td>Integral Serialism</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Modern Classical</td><td>Sonorism</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Modern Classical</td><td>Spaghetti Western</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Modern Classical</td><td>Spectralism</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Modern Classical</td><td>Stochastic Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Persian Classical Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Pibroch</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Shashmaqam</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>South Asian Classical Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>South Asian Classical Music</td><td>Carnatic Classical Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>South Asian Classical Music</td><td>Hindustani Classical Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>South Asian Classical Music</td><td>Hindustani Classical Music</td><td>Dhrupad</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>South Asian Classical Music</td><td>Hindustani Classical Music</td><td>Khyal</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>South Asian Classical Music</td><td>Hindustani Classical Music</td><td>Qawwali</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>South Asian Classical Music</td><td>Hindustani Classical Music</td><td>Qawwali</td><td>Kafi</td></tr>
 <tr><td>Classical Music</td><td>South Asian Classical Music</td><td>Hindustani Classical Music</td><td>Thumri</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Southeast Asian Classical Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Southeast Asian Classical Music</td><td>Burmese Classical Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Southeast Asian Classical Music</td><td>Caklempong</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Southeast Asian Classical Music</td><td>Gamelan</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Southeast Asian Classical Music</td><td>Gamelan</td><td>Gamelan angklung</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Southeast Asian Classical Music</td><td>Gamelan</td><td>Gamelan Degung</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Southeast Asian Classical Music</td><td>Gamelan</td><td>Gamelan gong gede</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Southeast Asian Classical Music</td><td>Gamelan</td><td>Gamelan Gong Kebyar</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Southeast Asian Classical Music</td><td>Gamelan</td><td>Gamelan Jawa</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Southeast Asian Classical Music</td><td>Gamelan</td><td>Gamelan semar pegulingan</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Southeast Asian Classical Music</td><td>Kecapi Suling</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Southeast Asian Classical Music</td><td>Kulintang</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Southeast Asian Classical Music</td><td>Mahori</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Southeast Asian Classical Music</td><td>Pinpeat</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Southeast Asian Classical Music</td><td>Saluang Klasik</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Southeast Asian Classical Music</td><td>Tembang Cianjuran</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Southeast Asian Classical Music</td><td>Thai Classical Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Southeast Asian Classical Music</td><td>Thai Classical Music</td><td>Piphat</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Turkish Classical Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Western Classical Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Western Classical Music</td><td>Baroque Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Western Classical Music</td><td>Baroque Music</td><td>Tragédie en musique</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Western Classical Music</td><td>Byzantine Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Western Classical Music</td><td>Byzantine Music</td><td>Byzantine Chant</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Western Classical Music</td><td>Chamber Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Western Classical Music</td><td>Choral</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Western Classical Music</td><td>Classical Marches</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Western Classical Music</td><td>Classical Marches</td><td>Circus March</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Western Classical Music</td><td>Classical Period</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Western Classical Music</td><td>Classical Waltz</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Western Classical Music</td><td>Impressionism</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Western Classical Music</td><td>Lieder</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Western Classical Music</td><td>Madrigal</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Western Classical Music</td><td>Medieval Classical Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Western Classical Music</td><td>Medieval Classical Music</td><td>Ars antiqua</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Western Classical Music</td><td>Medieval Classical Music</td><td>Ars nova</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Western Classical Music</td><td>Medieval Classical Music</td><td>Ars subtilior</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Western Classical Music</td><td>Medieval Classical Music</td><td>Plainsong</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Western Classical Music</td><td>Medieval Classical Music</td><td>Plainsong</td><td>Gregorian Chant</td></tr>
 <tr><td>Classical Music</td><td>Western Classical Music</td><td>Neoclassicism</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Western Classical Music</td><td>Opera</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Western Classical Music</td><td>Opera</td><td>Opera buffa</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Western Classical Music</td><td>Opera</td><td>Opera semiseria</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Western Classical Music</td><td>Opera</td><td>Opera Seria</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Western Classical Music</td><td>Opera</td><td>Operetta</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Western Classical Music</td><td>Opera</td><td>Operetta</td><td>Zarzuela</td></tr>
 <tr><td>Classical Music</td><td>Western Classical Music</td><td>Opera</td><td>Tragédie en musique</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Western Classical Music</td><td>Oratorio</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Western Classical Music</td><td>Orchestral</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Western Classical Music</td><td>Orchestral</td><td>Concerto</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Western Classical Music</td><td>Orchestral</td><td>Symphony</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Western Classical Music</td><td>Orchestral</td><td>Tone Poem</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Western Classical Music</td><td>Renaissance Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Western Classical Music</td><td>Romanticism</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Western Classical Music</td><td>Spanish Classical Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Classical Music</td><td>Western Classical Music</td><td>Spanish Classical Music</td><td>Zarzuela</td><td></td></tr>
</tbody></table>
</details>

### Comedy

<details><summary>click to open/close</summary>
<table>
<tbody>
 <tr><td>Comedy</td><td>Dutch Cabaret</td><td>&nbsp;</td></tr>
 <tr><td>Comedy</td><td>Kabarett</td><td>&nbsp;</td></tr>
 <tr><td>Comedy</td><td>Musical Comedy</td><td>&nbsp;</td></tr>
 <tr><td>Comedy</td><td>Musical Comedy</td><td>Comedy Rap</td></tr>
 <tr><td>Comedy</td><td>Musical Comedy</td><td>Comedy Rock</td></tr>
 <tr><td>Comedy</td><td>Musical Comedy</td><td>Scrumpy and Western</td></tr>
 <tr><td>Comedy</td><td>Prank Calls</td><td>&nbsp;</td></tr>
 <tr><td>Comedy</td><td>Sketch Comedy</td><td>&nbsp;</td></tr>
 <tr><td>Comedy</td><td>Stand-up Comedy</td><td></td></tr>
</tbody></table>
</details>

### Country

<details><summary>click to open/close</summary>
<table>
<tbody>
 <tr><td>Country</td><td>Alt-Country</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Country</td><td>Alt-Country</td><td>Gothic Country</td><td>&nbsp;</td></tr>
 <tr><td>Country</td><td>Americana</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Country</td><td>Contemporary Country</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Country</td><td>Contemporary Country</td><td>Country Rap</td><td>&nbsp;</td></tr>
 <tr><td>Country</td><td>Contemporary Country</td><td>Neo-Traditionalist Country</td><td>&nbsp;</td></tr>
 <tr><td>Country</td><td>Country & Irish</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Country</td><td>Country Pop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Country</td><td>Country Pop</td><td>Bro-Country</td><td>&nbsp;</td></tr>
 <tr><td>Country</td><td>Country Pop</td><td>Nashville Sound</td><td>&nbsp;</td></tr>
 <tr><td>Country</td><td>Country Pop</td><td>Urban Cowboy</td><td>&nbsp;</td></tr>
 <tr><td>Country</td><td>Honky Tonk</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Country</td><td>Honky Tonk</td><td>Bakersfield Sound</td><td>&nbsp;</td></tr>
 <tr><td>Country</td><td>Honky Tonk</td><td>Truck Driving Country</td><td>&nbsp;</td></tr>
 <tr><td>Country</td><td>Progressive Country</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Country</td><td>Progressive Country</td><td>Outlaw Country</td><td>&nbsp;</td></tr>
 <tr><td>Country</td><td>Progressive Country</td><td>Outlaw Country</td><td>Red Dirt</td></tr>
 <tr><td>Country</td><td>Progressive Country</td><td>Progressive Bluegrass</td><td>&nbsp;</td></tr>
 <tr><td>Country</td><td>Traditional Country</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Country</td><td>Traditional Country</td><td>Bluegrass</td><td>&nbsp;</td></tr>
 <tr><td>Country</td><td>Traditional Country</td><td>Bluegrass</td><td>Bluegrass Gospel</td></tr>
 <tr><td>Country</td><td>Traditional Country</td><td>Close Harmony</td><td>&nbsp;</td></tr>
 <tr><td>Country</td><td>Traditional Country</td><td>Country Boogie</td><td>&nbsp;</td></tr>
 <tr><td>Country</td><td>Traditional Country</td><td>Country Gospel</td><td>&nbsp;</td></tr>
 <tr><td>Country</td><td>Traditional Country</td><td>Country Gospel</td><td>Bluegrass Gospel</td></tr>
 <tr><td>Country</td><td>Traditional Country</td><td>Country Yodeling</td><td>&nbsp;</td></tr>
 <tr><td>Country</td><td>Traditional Country</td><td>Cowboy</td><td>&nbsp;</td></tr>
 <tr><td>Country</td><td>Traditional Country</td><td>Old-Time</td><td>&nbsp;</td></tr>
 <tr><td>Country</td><td>Western Swing</td><td>&nbsp;</td><td></td></tr>
</tbody></table>
</details>

### Dance

<details><summary>click to open/close</summary>
<table>
<tbody>
 <tr><td>Dance</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Alternative Dance</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Alternative Dance</td><td>Grebo</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Dance-Pop</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Dance-Pop</td><td>Afrobeats</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Dance-Pop</td><td>Bubblegum [South Africa]</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Dance-Pop</td><td>Disco polo</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Dance-Pop</td><td>Eurodance</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Dance-Pop</td><td>Eurodance</td><td>Bubblegum Dance</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Dance-Pop</td><td>Eurodance</td><td>Italo Dance</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Dance-Pop</td><td>Freestyle</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Dance-Pop</td><td>Freestyle</td><td>Latin Freestyle</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Dance-Pop</td><td>Funk Melody</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Dance-Pop</td><td>Romanian Dance-Pop</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Dance-Pop</td><td>Tecnorumba</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Dance-Punk</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Disco</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Disco</td><td>Boogie</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Disco</td><td>Electro-Disco</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Disco</td><td>Electro-Disco</td><td>Hi-NRG</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Disco</td><td>Electro-Disco</td><td>Italo-Disco</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Disco</td><td>Electro-Disco</td><td>Italo-Disco</td><td>Eurobeat</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Disco</td><td>Electro-Disco</td><td>Space Disco</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Disco</td><td>Euro-Disco</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Disco</td><td>Latin Disco</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Disco</td><td>Nu-Disco</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Disco</td><td>Nu-Disco</td><td>Future Funk</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Bérite Club</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Breakbeat</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Breakbeat</td><td>Baltimore Club</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Breakbeat</td><td>Big Beat</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Breakbeat</td><td>Breakbeat Hardcore</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Breakbeat</td><td>Breakbeat Hardcore</td><td>Hardcore Breaks</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Breakbeat</td><td>Jersey Club</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Broken Beat</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Bubblegum Bass</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Coupé-Décalé</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Deconstructed Club</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Digital Cumbia</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Drum and Bass</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Drum and Bass</td><td>Atmospheric Drum and Bass</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Drum and Bass</td><td>Dancefloor Drum and Bass</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Drum and Bass</td><td>Darkstep</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Drum and Bass</td><td>Drumfunk</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Drum and Bass</td><td>Drumstep</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Drum and Bass</td><td>Halftime</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Drum and Bass</td><td>Hardstep</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Drum and Bass</td><td>Jazzstep</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Drum and Bass</td><td>Jump-Up</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Drum and Bass</td><td>Jungle</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Drum and Bass</td><td>Jungle</td><td>Ragga Jungle</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Drum and Bass</td><td>Liquid Funk</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Drum and Bass</td><td>Minimal Drum and Bass</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Drum and Bass</td><td>Neurofunk</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Drum and Bass</td><td>Techstep</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Drum and Bass</td><td>Trancestep</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Dubstep</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Dubstep</td><td>Brostep</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Dubstep</td><td>Brostep</td><td>Deathstep</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Dubstep</td><td>Brostep</td><td>Drumstep</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Dubstep</td><td>Brostep</td><td>Riddim</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Dubstep</td><td>Chillstep</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Dubstep</td><td>Purple Sound</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>EBM (Electronic Body Music)</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>EBM (Electronic Body Music)</td><td>Aggrotech</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>EBM (Electronic Body Music)</td><td>Futurepop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>EBM (Electronic Body Music)</td><td>New Beat</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Electro</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Electro</td><td>Skweee</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Electroclash</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Electro-Disco</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Electro-Disco</td><td>Hi-NRG</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Electro-Disco</td><td>Italo-Disco</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Electro-Disco</td><td>Italo-Disco</td><td>Eurobeat</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Electro-Disco</td><td>Space Disco</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Electro latino</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Electro Swing</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Eurodance</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Eurodance</td><td>Bubblegum Dance</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Eurodance</td><td>Italo Dance</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Flex Dance Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Freestyle</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Freestyle</td><td>Latin Freestyle</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Funk carioca</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Funk carioca</td><td>Funk Melody</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Funk carioca</td><td>Funk ostentação</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Funk carioca</td><td>Funk proibidão</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Funktronica</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Future Bass</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Hardcore [EDM]</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Hardcore [EDM]</td><td>Acidcore</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Hardcore [EDM]</td><td>Breakbeat Hardcore</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Hardcore [EDM]</td><td>&nbsp;</td><td>Hardcore Breaks</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Hardcore [EDM]</td><td>Digital Hardcore</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Hardcore [EDM]</td><td>Freeform</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Hardcore [EDM]</td><td>Frenchcore</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Hardcore [EDM]</td><td>Funkot</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Hardcore [EDM]</td><td>Gabber</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Hardcore [EDM]</td><td>Gabber</td><td>Nu Style Gabber</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Hardcore [EDM]</td><td>Happy Hardcore</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Hardcore [EDM]</td><td>J-core</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Hardcore [EDM]</td><td>Speedcore</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Hardcore [EDM]</td><td>Speedcore</td><td>Extratone</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Hardcore [EDM]</td><td>Speedcore</td><td>Splittercore</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Hardcore [EDM]</td><td>Terrorcore</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Acid House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Afro-House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Ambient House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Balearic Beat</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Ballroom</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Baltimore Club</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Bass House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Chicago House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Deep House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Deep House</td><td>Outsider House</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Dutch House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Electro House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Electro House</td><td>Big Room</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Electro House</td><td>Complextro</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Electro House</td><td>Fidget House</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Electro House</td><td>Jungle Terror</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Electro House</td><td>Melbourne Bounce</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Euro House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>French House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Funky House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Future House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Garage House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Ghetto House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Ghetto House</td><td>Ghettotech</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Ghetto House</td><td>Juke</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Ghetto House</td><td>Juke</td><td>Footwork</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Gqom</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Hard House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Hard House</td><td>NRG</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Hip House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Italo House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Jersey Club</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Kwaito</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Latin House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Microhouse</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Nu-Disco</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Nu-Disco</td><td>Future Funk</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Progressive House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Progressive House</td><td>Big Room</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Scouse House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Tech House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Tribal House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>House</td><td>Tropical House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Kuduro</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Kuduro</td><td>Batida</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Mahraganat</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Makina</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Moombahton</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Moombahton</td><td>Moombahcore</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Romanian Dance-Pop</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Shangaan Electro</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Singeli</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Spacesynth</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Techno</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Techno</td><td>Acid Techno</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Techno</td><td>Ambient Techno</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Techno</td><td>Bleep Techno</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Techno</td><td>Detroit Techno</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Techno</td><td>Freetekno</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Techno</td><td>Industrial Techno</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Techno</td><td>Minimal Techno</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Techno</td><td>Minimal Techno</td><td>Dub Techno</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Techno</td><td>Schranz</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Techno</td><td>Tech House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Techno</td><td>Wonky Techno</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Tecnobrega</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Tecnorumba</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Trance</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Trance</td><td>Acid Trance</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Trance</td><td>Ambient Trance</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Trance</td><td>Dream Trance</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Trance</td><td>Euro-Trance</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Trance</td><td>Goa Trance</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Trance</td><td>Goa Trance</td><td>Nitzhonot</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Trance</td><td>Hard Trance</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Trance</td><td>Hard Trance</td><td>Hardstyle</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Trance</td><td>Hard Trance</td><td>Hardstyle</td><td>Dubstyle</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Trance</td><td>Hard Trance</td><td>Hardstyle</td><td>Jumpstyle</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Trance</td><td>Ibiza Trance</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Trance</td><td>NRG</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Trance</td><td>Progressive Trance</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Trance</td><td>Psytrance</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Trance</td><td>Psytrance</td><td>Dark Psytrance</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Trance</td><td>Psytrance</td><td>Dark Psytrance</td><td>Hi-Tech Psytrance</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Trance</td><td>Psytrance</td><td>Full-On Psytrance</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Trance</td><td>Psytrance</td><td>Progressive Psytrance</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Trance</td><td>Psytrance</td><td>Suomisaundi</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Trance</td><td>Tech Trance</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Trance</td><td>Uplifting Trance</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Trance</td><td>Vocal Trance</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Trap [EDM]</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Tribal Guarachero</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>UK Bass</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>UK Garage</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>UK Garage</td><td>2-Step</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>UK Garage</td><td>Bassline</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>UK Garage</td><td>Breakstep</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>UK Garage</td><td>Future Garage</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>UK Garage</td><td>Speed Garage</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>UK Garage</td><td>UK Funky</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Dance</td><td>Electronic Dance Music</td><td>Wonky</td><td>&nbsp;</td><td>&nbsp;</td><td></td></tr>
</tbody></table>
</details>

### Electronic

<details><summary>click to open/close</summary>
<table>
<tbody>
 <tr><td>Electronic</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Acid Croft</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Ambient Dub</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Bit Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Bit Music</td><td>16-bit</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Bit Music</td><td>Chiptune</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Bit Music</td><td>FM Synthesis</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Bit Music</td><td>Sequencer & MIDI</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Bitpop</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Bitpop</td><td>Picopop</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Downtempo</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Downtempo</td><td>Trip Hop</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Dungeon Synth</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electroacoustic</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electroacoustic</td><td>EAI (Electroacoustic Improvisation)</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electroacoustic</td><td>Musique concrète</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electro-Industrial</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electro-Industrial</td><td>Dark Electro</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electro-Industrial</td><td>Dark Electro</td><td>Aggrotech</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Bérite Club</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Breakbeat</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Breakbeat</td><td>Baltimore Club</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Breakbeat</td><td>Big Beat</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Breakbeat</td><td>Breakbeat Hardcore</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Breakbeat</td><td>Breakbeat Hardcore</td><td>Hardcore Breaks</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Breakbeat</td><td>Jersey Club</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Broken Beat</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Bubblegum Bass</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Coupé-Décalé</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Deconstructed Club</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Digital Cumbia</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Drum and Bass</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Drum and Bass</td><td>Atmospheric Drum and Bass</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Drum and Bass</td><td>Dancefloor Drum and Bass</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Drum and Bass</td><td>Darkstep</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Drum and Bass</td><td>Drumfunk</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Drum and Bass</td><td>Drumstep</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Drum and Bass</td><td>Halftime</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Drum and Bass</td><td>Hardstep</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Drum and Bass</td><td>Jazzstep</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Drum and Bass</td><td>Jump-Up</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Drum and Bass</td><td>Jungle</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Drum and Bass</td><td>Jungle</td><td>Ragga Jungle</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Drum and Bass</td><td>Liquid Funk</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Drum and Bass</td><td>Minimal Drum and Bass</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Drum and Bass</td><td>Neurofunk</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Drum and Bass</td><td>Techstep</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Drum and Bass</td><td>Trancestep</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Dubstep</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Dubstep</td><td>Brostep</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Dubstep</td><td>Brostep</td><td>Deathstep</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Dubstep</td><td>Brostep</td><td>Drumstep</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Dubstep</td><td>Brostep</td><td>Riddim</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Dubstep</td><td>Chillstep</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Dubstep</td><td>Purple Sound</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>EBM (Electronic Body Music)</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>EBM (Electronic Body Music)</td><td>Aggrotech</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>EBM (Electronic Body Music)</td><td>Futurepop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>EBM (Electronic Body Music)</td><td>New Beat</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Electro</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Electro</td><td>Skweee</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Electroclash</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Electro-Disco</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Electro-Disco</td><td>Hi-NRG</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Electro-Disco</td><td>Italo-Disco</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Electro-Disco</td><td>Italo-Disco</td><td>Eurobeat</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Electro-Disco</td><td>Space Disco</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Electro latino</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Electro Swing</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Eurodance</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Eurodance</td><td>Bubblegum Dance</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Eurodance</td><td>Italo Dance</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Flex Dance Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Freestyle</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Freestyle</td><td>Latin Freestyle</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Funk carioca</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Funk carioca</td><td>Funk Melody</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Funk carioca</td><td>Funk ostentação</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Funk carioca</td><td>Funk proibidão</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Funktronica</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Future Bass</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Hardcore [EDM]</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Hardcore [EDM]</td><td>Acidcore</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Hardcore [EDM]</td><td>Breakbeat Hardcore</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Hardcore [EDM]</td><td>Breakbeat Hardcore</td><td>Hardcore Breaks</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Hardcore [EDM]</td><td>Digital Hardcore</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Hardcore [EDM]</td><td>Freeform</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Hardcore [EDM]</td><td>Frenchcore</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Hardcore [EDM]</td><td>Funkot</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Hardcore [EDM]</td><td>Gabber</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Hardcore [EDM]</td><td>Gabber</td><td>Nu Style Gabber</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Hardcore [EDM]</td><td>Happy Hardcore</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Hardcore [EDM]</td><td>J-core</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Hardcore [EDM]</td><td>Speedcore</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Hardcore [EDM]</td><td>Speedcore</td><td>Extratone</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Hardcore [EDM]</td><td>Speedcore</td><td>Splittercore</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Hardcore [EDM]</td><td>Terrorcore</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Acid House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Afro-House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Ambient House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Balearic Beat</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Ballroom</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Baltimore Club</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Bass House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Chicago House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Deep House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Deep House</td><td>Outsider House</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Dutch House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Electro House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Electro House</td><td>Big Room</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Electro House</td><td>Complextro</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Electro House</td><td>Fidget House</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Electro House</td><td>Jungle Terror</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Electro House</td><td>Melbourne Bounce</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Euro House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>French House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Funky House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Future House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Garage House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Ghetto House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Ghetto House</td><td>Ghettotech</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Ghetto House</td><td>Juke</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Ghetto House</td><td>Juke</td><td>Footwork</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Gqom</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Hard House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Hard House</td><td>NRG</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Hip House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Italo House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Jersey Club</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Kwaito</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Latin House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Microhouse</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Nu-Disco</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Nu-Disco</td><td>Future Funk</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Progressive House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Progressive House</td><td>Big Room</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Scouse House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Tech House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Tribal House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>House</td><td>Tropical House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Kuduro</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Kuduro</td><td>Batida</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Mahraganat</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Makina</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Moombahton</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Moombahton</td><td>Moombahcore</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Romanian Dance-Pop</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Shangaan Electro</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Singeli</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Spacesynth</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Techno</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Techno</td><td>Acid Techno</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Techno</td><td>Ambient Techno</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Techno</td><td>Bleep Techno</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Techno</td><td>Detroit Techno</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Techno</td><td>Freetekno</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Techno</td><td>Industrial Techno</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Techno</td><td>Minimal Techno</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Techno</td><td>Minimal Techno</td><td>Dub Techno</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Techno</td><td>Schranz</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Techno</td><td>Tech House</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Techno</td><td>Wonky Techno</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Tecnobrega</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Tecnorumba</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Trance</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Trance</td><td>Acid Trance</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Trance</td><td>Ambient Trance</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Trance</td><td>Dream Trance</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Trance</td><td>Euro-Trance</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Trance</td><td>Goa Trance</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Trance</td><td>Goa Trance</td><td>Nitzhonot</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Trance</td><td>Hard Trance</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Trance</td><td>Hard Trance</td><td>Hardstyle</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Trance</td><td>Hard Trance</td><td>Hardstyle</td><td>Dubstyle</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Trance</td><td>Hard Trance</td><td>Hardstyle</td><td>Jumpstyle</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Trance</td><td>Ibiza Trance</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Trance</td><td>NRG</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Trance</td><td>Progressive Trance</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Trance</td><td>Psytrance</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Trance</td><td>Psytrance</td><td>Dark Psytrance</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Trance</td><td>Psytrance</td><td>Dark Psytrance</td><td>Hi-Tech Psytrance</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Trance</td><td>Psytrance</td><td>Full-On Psytrance</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Trance</td><td>Psytrance</td><td>Progressive Psytrance</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Trance</td><td>Psytrance</td><td>Suomisaundi</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Trance</td><td>Tech Trance</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Trance</td><td>Uplifting Trance</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Trance</td><td>Vocal Trance</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Trap [EDM]</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Tribal Guarachero</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>UK Bass</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>UK Garage</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>UK Garage</td><td>2-Step</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>UK Garage</td><td>Bassline</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>UK Garage</td><td>Breakstep</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>UK Garage</td><td>Future Garage</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>UK Garage</td><td>Speed Garage</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>UK Garage</td><td>UK Funky</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electronic Dance Music</td><td>Wonky</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Electropop</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Folktronica</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Glitch</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Glitch Hop</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Glitch Hop</td><td>Neurohop</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Glitch Pop</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Grime</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Grime</td><td>Weightless</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Horror Synth</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>IDM (Intelligent Dance Music)</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>IDM (Intelligent Dance Music)</td><td>Breakcore</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>IDM (Intelligent Dance Music)</td><td>Breakcore</td><td>Lolicore</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>IDM (Intelligent Dance Music)</td><td>Breakcore</td><td>Raggacore</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>IDM (Intelligent Dance Music)</td><td>Drill and Bass</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>IDM (Intelligent Dance Music)</td><td>Flashcore</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Illbient</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Indietronica</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Indietronica</td><td>Chillwave</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Latin Electronic</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Latin Electronic</td><td>Digital Cumbia</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Latin Electronic</td><td>Electro latino</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Latin Electronic</td><td>Electrotango</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Latin Electronic</td><td>Latin House</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Latin Electronic</td><td>Nortec</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Latin Electronic</td><td>Tribal Guarachero</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Microsound</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Minimal Wave</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Minimal Wave</td><td>Minimal Synth</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Nightcore</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Nu Jazz</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Nu Jazz</td><td>Electro Swing</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Progressive Electronic</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Progressive Electronic</td><td>Berlin School</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Psybient</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Synthpop</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Synthpop</td><td>Futurepop</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Synthpop</td><td>Techno Kayō</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Synth Punk</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Synthwave</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Synthwave</td><td>Darksynth</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Vaportrap</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Vaporwave</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Vaporwave</td><td>Future Funk</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Electronic</td><td>Witch House</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td></td></tr>
</tbody></table>
</details>

### Experimental

<details><summary>click to open/close</summary>
<table>
<tbody>
 <tr><td>Experimental</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Experimental</td><td>Drone</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Experimental</td><td>Electroacoustic</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Experimental</td><td>Electroacoustic</td><td>EAI (Electroacoustic Improvisation)</td><td>&nbsp;</td></tr>
 <tr><td>Experimental</td><td>Electroacoustic</td><td>Musique concrète</td><td>&nbsp;</td></tr>
 <tr><td>Experimental</td><td>Free Improvisation</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Experimental</td><td>Free Improvisation</td><td>EAI (Electroacoustic Improvisation)</td><td>&nbsp;</td></tr>
 <tr><td>Experimental</td><td>Futurism</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Experimental</td><td>Glitch</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Experimental</td><td>Indeterminacy</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Experimental</td><td>Industrial</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Experimental</td><td>Industrial</td><td>Power Electronics</td><td>&nbsp;</td></tr>
 <tr><td>Experimental</td><td>Industrial</td><td>Power Electronics</td><td>Death Industrial</td></tr>
 <tr><td>Experimental</td><td>Noise</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Experimental</td><td>Noise</td><td>Gorenoise</td><td>&nbsp;</td></tr>
 <tr><td>Experimental</td><td>Noise</td><td>Harsh Noise</td><td>&nbsp;</td></tr>
 <tr><td>Experimental</td><td>Noise</td><td>Harsh Noise</td><td>Harsh Noise Wall</td></tr>
 <tr><td>Experimental</td><td>Noise</td><td>Power Electronics</td><td>&nbsp;</td></tr>
 <tr><td>Experimental</td><td>Noise</td><td>Power Electronics</td><td>Death Industrial</td></tr>
 <tr><td>Experimental</td><td>Noise</td><td>Power Noise</td><td>&nbsp;</td></tr>
 <tr><td>Experimental</td><td>Plunderphonics</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Experimental</td><td>Plunderphonics</td><td>Vaporwave</td><td>&nbsp;</td></tr>
 <tr><td>Experimental</td><td>Plunderphonics</td><td>Vaporwave</td><td>Future Funk</td></tr>
 <tr><td>Experimental</td><td>Reductionism</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Experimental</td><td>Reductionism</td><td>Lowercase</td><td>&nbsp;</td></tr>
 <tr><td>Experimental</td><td>Reductionism</td><td>Onkyo</td><td>&nbsp;</td></tr>
 <tr><td>Experimental</td><td>Sound Collage</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Experimental</td><td>Sound Collage</td><td>Microsound</td><td>&nbsp;</td></tr>
 <tr><td>Experimental</td><td>Sound Poetry</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Experimental</td><td>Tape Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Experimental</td><td>Turntable Music</td><td>&nbsp;</td><td></td></tr>
</tbody></table>
</details>

### Field Recordings

<details><summary>click to open/close</summary>
<table>
<tbody>
 <tr><td>Field Recordings</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Field Recordings</td><td>Nature Recordings</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Field Recordings</td><td>Nature Recordings</td><td>Animal Sounds</td><td>&nbsp;</td></tr>
 <tr><td>Field Recordings</td><td>Nature Recordings</td><td>Animal Sounds</td><td>Birdsong</td></tr>
 <tr><td>Field Recordings</td><td>Nature Recordings</td><td>Animal Sounds</td><td>Whale Song</td></tr>
 <tr><td>Field Recordings</td><td>Radio Broadcast Recordings</td><td>&nbsp;</td><td></td></tr>
</tbody></table>
</details>

### Folk

<details><summary>click to open/close</summary>
<table>
<tbody>
 <tr><td>Folk</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Contemporary Folk</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Contemporary Folk</td><td>American Primitivism</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Contemporary Folk</td><td>Anti-Folk</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Contemporary Folk</td><td>Avant-Folk</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Contemporary Folk</td><td>Bard Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Contemporary Folk</td><td>Canzone d'autore</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Contemporary Folk</td><td>Chamber Folk</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Contemporary Folk</td><td>Euskal Kantagintza Berria</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Contemporary Folk</td><td>Folk Baroque</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Contemporary Folk</td><td>Folk Pop</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Contemporary Folk</td><td>Indie Folk</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Contemporary Folk</td><td>Música de intervenção</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Contemporary Folk</td><td>Neofolk</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Contemporary Folk</td><td>Neofolk</td><td>Dark Folk</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Contemporary Folk</td><td>Nova cançó</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Contemporary Folk</td><td>Nòva cançon</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Contemporary Folk</td><td>Nueva canción</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Contemporary Folk</td><td>Progressive Folk</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Contemporary Folk</td><td>Psychedelic Folk</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Contemporary Folk</td><td>Psychedelic Folk</td><td>Freak Folk</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Contemporary Folk</td><td>Psychedelic Folk</td><td>Free Folk</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Contemporary Folk</td><td>Skiffle</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Contemporary Folk</td><td>Tamborera</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Ambasse bey</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Apala</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Batuque</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Dagomba Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Gnawa</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Kabye Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Kilapanga</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Mbenga-Mbuti Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Moutya</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Ngoma</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Ngoma</td><td>Unyago</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Semba</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Southern African Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Southern African Folk Music</td><td>Khoisan Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Southern African Folk Music</td><td>Nguni Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Southern African Folk Music</td><td>Shona Mbira Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Southern African Folk Music</td><td>Sotho-Tswana Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Tchinkoumé</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Traditional Maloya</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Traditional Séga</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Zinli</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Appalachian Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Barbershop</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Boogie Woogie</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Cajun</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Cajun</td><td>Traditional Cajun</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Cajun</td><td>Zydeco</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Country Blues</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Country Blues</td><td>Acoustic Texas Blues</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Country Blues</td><td>Delta Blues</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Country Blues</td><td>Piedmont Blues</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Gospel</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Gospel</td><td>Country Gospel</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Gospel</td><td>&nbsp;</td><td>Bluegrass Gospel</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Gospel</td><td>Southern Gospel</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Gospel</td><td>Traditional Black Gospel</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Jug Band</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Ragtime</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Ragtime</td><td>Cakewalk</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Ragtime</td><td>Coon Song</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Ragtime</td><td>Novelty Piano</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Ragtime</td><td>Stride</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Sacred Harp Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Spirituals</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Talking Blues</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Traditional Country</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Traditional Country</td><td>Bluegrass</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Traditional Country</td><td>Bluegrass</td><td>Bluegrass Gospel</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Traditional Country</td><td>Close Harmony</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Traditional Country</td><td>Country Boogie</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Traditional Country</td><td>Country Gospel</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Traditional Country</td><td>Country Gospel</td><td>Bluegrass Gospel</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Traditional Country</td><td>Country Yodeling</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Traditional Country</td><td>Cowboy</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Traditional Country</td><td>Old-Time</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Arabic Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Arabic Folk Music</td><td>Arabic Bellydance Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Australian Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Austronesian Traditional Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Austronesian Traditional Music</td><td>Gondang</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Austronesian Traditional Music</td><td>Indigenous Taiwanese Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Austronesian Traditional Music</td><td>Pakacaping Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Balitaw</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Canadian Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Canadian Folk Music</td><td>Canadian Maritime Folk</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Canadian Folk Music</td><td>Canadian Maritime Folk</td><td>Cape Breton Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Canadian Folk Music</td><td>Canadian Maritime Folk</td><td>Cape Breton Folk Music</td><td>Cape Breton Fiddling</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Canadian Folk Music</td><td>French-Canadian Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Canadian Folk Music</td><td>Newfoundland Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Caribbean Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Caribbean Folk Music</td><td>Fungi</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Caribbean Folk Music</td><td>Garifuna Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Caribbean Folk Music</td><td>Junkanoo</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Caribbean Folk Music</td><td>Méringue</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Caribbean Folk Music</td><td>Ripsaw</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Caribbean Folk Music</td><td>Trinidadian Cariso</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Caribbean Folk Music</td><td>Tumba</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Caribbean Folk Music</td><td>Virgin Islander Cariso</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Caucasian Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Caucasian Folk Music</td><td>Abkhazian Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Caucasian Folk Music</td><td>Avar Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Caucasian Folk Music</td><td>Chechen Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Caucasian Folk Music</td><td>Circassian Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Caucasian Folk Music</td><td>Georgian Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Caucasian Folk Music</td><td>Ossetian Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Chuvash Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Ainu Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Ainu Folk Music</td><td>Upopo</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Ainu Folk Music</td><td>Yukar</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Chinese Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Chinese Folk Music</td><td>Guoyue</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Chinese Folk Music</td><td>GHan Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Chinese Folk Music</td><td>GJiangnan Sizhu</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Indigenous Taiwanese Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Japanese Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Japanese Folk Music</td><td>Amami Shimauta</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Japanese Folk Music</td><td>Heikyoku</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Japanese Folk Music</td><td>Kagura</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Japanese Folk Music</td><td>Kouta</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Japanese Folk Music</td><td>Min'yō</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Japanese Folk Music</td><td>Ondō</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Japanese Folk Music</td><td>Rōkyoku</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Japanese Folk Music</td><td>Taiko</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Japanese Folk Music</td><td>Tsugaru Shamisen</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Korean Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Korean Folk Music</td><td>Sanjo</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Korean Folk Music</td><td>Sinawi</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Alpine Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Alpine Folk Music</td><td>Naturjodel</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td>Albanian Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td>Albanian Folk Music</td><td>Lab Polyphony</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td>Albanian Folk Music</td><td>Musika popullore</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td>Albanian Folk Music</td><td>Tosk Polyphony</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td>Balkan Brass Band</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td>Bulgarian Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td>Csango Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td>Gagauz Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td>Greek Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td>Greek Folk Music</td><td>Dimotika</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td>Greek Folk Music</td><td>Kritika</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td>Greek Folk Music</td><td>Nisiotika Aigaiou</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td>Greek Folk Music</td><td>Nisiotika Ioniou</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td>Greek Folk Music</td><td>Rembetika</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td>Lăutărească</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td>Macedonian Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td>Macedonian Folk Music</td><td>Čalgija</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td>Sevdalinka</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td>Starogradska muzika</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td>Starogradska muzika</td><td>Čalgija</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Basque Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Catalan Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Catalan Folk Music</td><td>Sardana</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td>Asturian Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td>Breton Celtic Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td>Breton Celtic Folk Music</td><td>Bagad</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td>Cape Breton Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td>Cape Breton Folk Music</td><td>Cape Breton Fiddling</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td>Cornish Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td>Galician Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td>Galician Folk Music</td><td>Country & Irish</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td>Galician Folk Music</td><td>Sean-nós</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td>Irish Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td>Manx Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td>Newfoundland Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td>Scottish Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td>Scottish Folk Music</td><td>Gaelic Psalm</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td>Scottish Folk Music</td><td>Pibroch</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td>Scottish Folk Music</td><td>Pipe Band</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td>Scottish Folk Music</td><td>Shetland and Orkney Islands Folk Music</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td>Scottish Folk Music</td><td>Waulking Song</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td>Trás-os-Montes Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td>Welsh Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Dutch Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>English Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>English Folk Music</td><td>Cornish Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>English Folk Music</td><td>Northumbrian Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>English Folk Music</td><td>Scrumpy and Western</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Estonian Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Fanfare</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Fanfare</td><td>Brass Band</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Flemish Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>French Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>French Folk Music</td><td>Alsatian Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>French Folk Music</td><td>Breton Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>French Folk Music</td><td>Breton Folk Music</td><td>Breton Celtic Folk Music</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>French Folk Music</td><td>Breton Folk Music</td><td>Breton Celtic Folk Music</td><td>Bagad</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>French Folk Music</td><td>Corsican Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>French Folk Music</td><td>Musette</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>French Folk Music</td><td>Occitan Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>French Folk Music</td><td>Occitan Folk Music</td><td>Auvergnat Folk Music</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>French Folk Music</td><td>Occitan Folk Music</td><td>Gascon Folk Music</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>German Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Għana</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Hungarian Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Italian Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Italian Folk Music</td><td>Cantu a tenore</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Italian Folk Music</td><td>Canzone napoletana</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Latvian Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Lithuanian Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Lithuanian Folk Music</td><td>Sutartinės</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Mari Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Neo-Medieval Folk</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Nordic Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Nordic Folk Music</td><td>Danish Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Nordic Folk Music</td><td>Faroese Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Nordic Folk Music</td><td>Finnish Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Nordic Folk Music</td><td>Icelandic Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Nordic Folk Music</td><td>Joik</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Nordic Folk Music</td><td>Nordic Old Time Dance Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Nordic Folk Music</td><td>Nordic Old Time Dance Music</td><td>Humppa</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Nordic Folk Music</td><td>Norwegian Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Nordic Folk Music</td><td>Runolaulu</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Nordic Folk Music</td><td>Swedish Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Nordic Folk Music</td><td>Swedish Folk Music</td><td>Visor</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Polka</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Portuguese Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Portuguese Folk Music</td><td>Cante alentejano</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Portuguese Folk Music</td><td>Fado</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Portuguese Folk Music</td><td>Fado</td><td>Fado de Coimbra</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Portuguese Folk Music</td><td>Trás-os-Montes Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Romanian Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Romanian Folk Music</td><td>Bocet</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Romanian Folk Music</td><td>Colinde</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Romanian Folk Music</td><td>Doina</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Romanian Folk Music</td><td>Lăutărească</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td>Belarusian Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td>Bulgarian Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td>Croatian Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td>Croatian Folk Music</td><td>Klapa</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td>Czech Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td>Goral Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td>Goral Music</td><td>Polish Carpathian Folk Music</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td>Macedonian Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td>Moravian Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td>Polish Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td>Russian Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td>Sevdalinka</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td>Slovak Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td>Slovenian Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td>Slovenian Folk Music</td><td>Narodno zabavna glasba</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td>Starogradska muzika</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td>Starogradska muzika</td><td>Čalgija</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td>Ukrainian Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td>Ukrainian Folk Music</td><td>Duma</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Spanish Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Spanish Folk Music</td><td>Andalusian Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Spanish Folk Music</td><td>&nbsp;</td><td>Saeta</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Spanish Folk Music</td><td>Aragonese Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Spanish Folk Music</td><td>Asturian Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Spanish Folk Music</td><td>Canarian Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Spanish Folk Music</td><td>Chotis Madrileño</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Spanish Folk Music</td><td>Copla</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Spanish Folk Music</td><td>Cuplé</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Spanish Folk Music</td><td>Flamenco</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Spanish Folk Music</td><td>Flamenco</td><td>Flamenco nuevo</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Spanish Folk Music</td><td>Flamenco</td><td>Flamenco nuevo</td><td>Flamenco Jazz</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Spanish Folk Music</td><td>Flamenco</td><td>Rumba Flamenca</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Spanish Folk Music</td><td>Galician Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Spanish Folk Music</td><td>Pasodoble</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Spanish Folk Music</td><td>Sevillanas</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Warsaw City Folk</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Hill Tribe Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Hill Tribe Music</td><td>Hmong Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Indigenous Australian Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Indigenous Australian Music</td><td>Djanba</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Kundiman</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Malagasy Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Mexican Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Mexican Folk Music</td><td>Canto cardenche</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Mexican Folk Music</td><td>Ranchera</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Mexican Folk Music</td><td>Son Calentano</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Mexican Folk Music</td><td>Son Huasteco</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Mexican Folk Music</td><td>Son Istmeño</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Mexican Folk Music</td><td>Son Jarocho</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Mexican Folk Music</td><td>Trova yucateca</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Native American Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Native American Music</td><td>Harawi</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Native American Music</td><td>Huayno</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Native American Music</td><td>Inuit Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Native American Music</td><td>Inuit Music</td><td>Inuit Vocal Games</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Native American Music</td><td>Powwow Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Papuan Traditional Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Rapai dabõih</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Romani Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Samoyedic Traditional Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Sephardic Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Sephardic Music</td><td>Maftirim</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Andean Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Andean Folk Music</td><td>Chacarera</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Andean Folk Music</td><td>Harawi</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Andean Folk Music</td><td>Huayno</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Andean Folk Music</td><td>Muliza</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Bambuco</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Canto a lo poeta</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Cueca</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Joropo</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Malagueña venezolana</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Mapuche Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Milonga</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Música criolla peruana</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Música criolla peruana</td><td>Marinera</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Música criolla peruana</td><td>Tondero</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Música criolla peruana</td><td>Vals criollo</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Sertanejo de raiz</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Sertanejo de raiz</td><td>Moda de viola</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Tamborito</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Taquirari</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Vallenato</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Zamba</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>South Asian Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>South Asian Folk Music</td><td>Bengali Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>South Asian Folk Music</td><td>Kirtan</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>South Asian Folk Music</td><td>Malayali Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>South Asian Folk Music</td><td>Newa Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>South Asian Folk Music</td><td>Pashto Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>South Asian Folk Music</td><td>Punjabi Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>South Asian Folk Music</td><td>Sri Lankan Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>South Asian Folk Music</td><td>Sri Lankan Folk Music</td><td>Sarala Gee</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>South Asian Folk Music</td><td>Urumi melam</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Southeast Asian Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Southeast Asian Folk Music</td><td>Gondang</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Southeast Asian Folk Music</td><td>Hmong Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Southeast Asian Folk Music</td><td>Keroncong</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Southeast Asian Folk Music</td><td>Kuda Lumping</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Southeast Asian Folk Music</td><td>Malay Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Southeast Asian Folk Music</td><td>Molam</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Tatar Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Throat Singing</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Throat Singing</td><td>Mongolian Throat Singing</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Throat Singing</td><td>Tuvan Throat Singing</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Tonada</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Vietnamese Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>West Asian Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>West Asian Folk Music</td><td>Armenian Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>West Asian Folk Music</td><td>Armenian Folk Music</td><td>Kef Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>West Asian Folk Music</td><td>Iranian Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>West Asian Folk Music</td><td>Israeli Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>West Asian Folk Music</td><td>Kurdish Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>West Asian Folk Music</td><td>Turkish Folk Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>West Asian Folk Music</td><td>Turkish Folk Music</td><td>Karadeniz Folk Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>West Asian Folk Music</td><td>Turkish Folk Music</td><td>Uzun Hava</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>West Asian Folk Music</td><td>Turkish Folk Music</td><td>Zeybek</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Work Songs</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Work Songs</td><td>Field Hollers</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Work Songs</td><td>Sea Shanties</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Work Songs</td><td>Waulking Song</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Yodeling</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Yodeling</td><td>Country Yodeling</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Folk</td><td>Traditional Folk Music</td><td>Yodeling</td><td>Naturjodel</td><td>&nbsp;</td><td>&nbsp;</td><td></td></tr>
</tbody></table>
</details>

### Hip Hop

<details><summary>click to open/close</summary>
<table>
<tbody>
 <tr><td>Hip Hop</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Abstract Hip Hop</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Arabesque Rap</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Bongo Flava</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Bounce</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Chopped and Screwed</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Christian Hip Hop</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Cloud Rap</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Comedy Rap</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Conscious Hip Hop</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Country Rap</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Crunk</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Crunk</td><td>Crunkcore</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Dirty South</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Disco Rap</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>East Coast Hip Hop</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Emo Rap</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Experimental Hip Hop</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Experimental Hip Hop</td><td>Industrial Hip Hop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>French Hip Hop</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Genge</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>G-Funk</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Hardcore Hip Hop</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Hardcore Hip Hop</td><td>Boom Bap</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Hardcore Hip Hop</td><td>Britcore</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Hardcore Hip Hop</td><td>Gangsta Rap</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Hardcore Hip Hop</td><td>Gangsta Rap</td><td>Chicano Rap</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Hardcore Hip Hop</td><td>Gangsta Rap</td><td>Drill</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Hardcore Hip Hop</td><td>Gangsta Rap</td><td>Drill</td><td>Bop</td></tr>
 <tr><td>Hip Hop</td><td>Hardcore Hip Hop</td><td>Gangsta Rap</td><td>Drill</td><td>UK Drill</td></tr>
 <tr><td>Hip Hop</td><td>Hardcore Hip Hop</td><td>Horrorcore</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Hardcore Hip Hop</td><td>Memphis Rap</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Hardcore Hip Hop</td><td>Trap Metal</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Hiplife</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Hyphy</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Hyphy</td><td>Jerk Rap</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Instrumental Hip Hop</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Instrumental Hip Hop</td><td>Lo-Fi Hip Hop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Japanese Hip Hop</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Jazz Rap</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Jook</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Latin Rap</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Latin Rap</td><td>Chicano Rap</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Miami Bass</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Miami Bass</td><td>Atlanta Bass</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Miami Bass</td><td>Car Audio Bass</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Mobb Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Nerdcore</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Political Hip Hop</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Pop Rap</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Pop Rap</td><td>Bop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Snap</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Southern Hip Hop</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Trap</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Trap</td><td>Drill</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Trap</td><td>Drill</td><td>Bop</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Trap</td><td>Drill</td><td>UK Drill</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Trap</td><td>Trap Metal</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Trap</td><td>Tread</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>Turntablism</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>UK Hip Hop</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Hip Hop</td><td>West Coast Hip Hop</td><td>&nbsp;</td><td>&nbsp;</td><td></td></tr>
</tbody></table>
</details>

### Industrial Music

<details><summary>click to open/close</summary>
<table>
<tbody>
 <tr><td>Industrial Music</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Industrial Music</td><td>Industrial</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Industrial Music</td><td>Industrial</td><td>Power Electronics</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Industrial Music</td><td>Industrial</td><td>Power Electronics</td><td>Death Industrial</td><td>&nbsp;</td></tr>
 <tr><td>Industrial Music</td><td>Post-Industrial</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Industrial Music</td><td>Post-Industrial</td><td>Dark Ambient</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Industrial Music</td><td>Post-Industrial</td><td>Dark Ambient</td><td>Black Ambient</td><td>&nbsp;</td></tr>
 <tr><td>Industrial Music</td><td>Post-Industrial</td><td>Dark Ambient</td><td>Ritual Ambient</td><td>&nbsp;</td></tr>
 <tr><td>Industrial Music</td><td>Post-Industrial</td><td>Deconstructed Club</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Industrial Music</td><td>Post-Industrial</td><td>EBM (Electronic Body Music)</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Industrial Music</td><td>Post-Industrial</td><td>EBM (Electronic Body Music)</td><td>Aggrotech</td><td>&nbsp;</td></tr>
 <tr><td>Industrial Music</td><td>Post-Industrial</td><td>EBM (Electronic Body Music)</td><td>Futurepop</td><td>&nbsp;</td></tr>
 <tr><td>Industrial Music</td><td>Post-Industrial</td><td>EBM (Electronic Body Music)</td><td>New Beat</td><td>&nbsp;</td></tr>
 <tr><td>Industrial Music</td><td>Post-Industrial</td><td>Electro-Industrial</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Industrial Music</td><td>Post-Industrial</td><td>Electro-Industrial</td><td>Dark Electro</td><td>&nbsp;</td></tr>
 <tr><td>Industrial Music</td><td>Post-Industrial</td><td>Electro-Industrial</td><td>Dark Electro</td><td>Aggrotech</td></tr>
 <tr><td>Industrial Music</td><td>Post-Industrial</td><td>Industrial Hip Hop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Industrial Music</td><td>Post-Industrial</td><td>Industrial Metal</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Industrial Music</td><td>Post-Industrial</td><td>Industrial Metal</td><td>Cyber Metal</td><td>&nbsp;</td></tr>
 <tr><td>Industrial Music</td><td>Post-Industrial</td><td>Industrial Metal</td><td>Neue Deutsche Härte</td><td>&nbsp;</td></tr>
 <tr><td>Industrial Music</td><td>Post-Industrial</td><td>Industrial Rock</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Industrial Music</td><td>Post-Industrial</td><td>Industrial Techno</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Industrial Music</td><td>Post-Industrial</td><td>Martial Industrial</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Industrial Music</td><td>Power Noise</td><td>&nbsp;</td><td>&nbsp;</td><td></td></tr>
</tbody></table>
</details>

### Jazz

<details><summary>click to open/close</summary>
<table>
<tbody>
 <tr><td>Jazz</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>Acid Jazz</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>Afro-Jazz</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>Arabic Jazz</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>Avant-Garde Jazz</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>Avant-Garde Jazz</td><td>Experimental Big Band</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>Avant-Garde Jazz</td><td>Free Jazz</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>Avant-Garde Jazz</td><td>Free Jazz</td><td>European Free Jazz</td></tr>
 <tr><td>Jazz</td><td>Avant-Garde Jazz</td><td>Spiritual Jazz</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>Avant-Garde Jazz</td><td>Yass</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>Bebop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>Bebop</td><td>Hard Bop</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>Bebop</td><td>Post-Bop</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>Big Band</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>Big Band</td><td>Experimental Big Band</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>Big Band</td><td>Progressive Big Band</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>British Dance Band</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>Bulawayo Jazz</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>Cape Jazz</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>Cartoon Music</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>Chamber Jazz</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>Cool Jazz</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>Dark Jazz</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>Dixieland</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>ECM Style Jazz</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>Ethio-Jazz</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>Flamenco Jazz</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>Gypsy Jazz</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>Jazz-Funk</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>Jazz Fusion</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>Jazz Poetry</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>Latin Jazz</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>Latin Jazz</td><td>Afro-Cuban Jazz</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>Marabi</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>Marabi</td><td>Kwela</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>Marabi</td><td>Mbaqanga</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>Modal Jazz</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>New Orleans Brass Band</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>Samba-Jazz</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>Smooth Jazz</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>Soul Jazz</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>Stride</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>Swing</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>Swing</td><td>Swing Revival</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>Third Stream</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Jazz</td><td>Vocal Jazz</td><td>&nbsp;</td><td></td></tr>
</tbody></table>
</details>

### Metal

<details><summary>click to open/close</summary>
<table>
<tbody>
 <tr><td>Metal</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Alternative Metal</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Alternative Metal</td><td>Funk Metal</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Alternative Metal</td><td>Nu Metal</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Alternative Metal</td><td>Rap Metal</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Avant-Garde Metal</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Black Metal</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Black Metal</td><td>Atmospheric Black Metal</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Black Metal</td><td>Atmospheric Black Metal</td><td>Blackgaze</td></tr>
 <tr><td>Metal</td><td>Black Metal</td><td>Black 'n' Roll</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Black Metal</td><td>Depressive Black Metal</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Black Metal</td><td>Melodic Black Metal</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Black Metal</td><td>Pagan Black Metal</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Black Metal</td><td>Symphonic Black Metal</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Black Metal</td><td>War Metal</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Death Metal</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Death Metal</td><td>Brutal Death Metal</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Death Metal</td><td>Brutal Death Metal</td><td>Slam Death Metal</td></tr>
 <tr><td>Metal</td><td>Death Metal</td><td>Death 'n' Roll</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Death Metal</td><td>Deathgrind</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Death Metal</td><td>Melodic Death Metal</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Death Metal</td><td>Technical Death Metal</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Doom Metal</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Doom Metal</td><td>Death Doom Metal</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Doom Metal</td><td>Funeral Doom Metal</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Doom Metal</td><td>Traditional Doom Metal</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Drone Metal</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Folk Metal</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Folk Metal</td><td>Celtic Metal</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Folk Metal</td><td>Medieval Folk Metal</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Gothic Metal</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Grindcore</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Grindcore</td><td>Cybergrind</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Grindcore</td><td>Deathgrind</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Grindcore</td><td>Goregrind</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Grindcore</td><td>Goregrind</td><td>Gorenoise</td></tr>
 <tr><td>Metal</td><td>Groove Metal</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Heavy Metal</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Heavy Metal</td><td>New Wave of British Heavy Metal</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Industrial Metal</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Industrial Metal</td><td>Cyber Metal</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Industrial Metal</td><td>Neue Deutsche Härte</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Melodic Metalcore</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Melodic Metalcore</td><td>Nintendocore</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Melodic Metalcore</td><td>Trancecore</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Metalcore</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Metalcore</td><td>Deathcore</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Metalcore</td><td>Mathcore</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Neoclassical Metal</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Post-Metal</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Post-Metal</td><td>Atmospheric Sludge Metal</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Power Metal</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Progressive Metal</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Progressive Metal</td><td>Djent</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Sludge Metal</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Sludge Metal</td><td>Atmospheric Sludge Metal</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Speed Metal</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Stoner Metal</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Symphonic Metal</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Thrash Metal</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Thrash Metal</td><td>Crossover Thrash</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Thrash Metal</td><td>Technical Thrash Metal</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Trance Metal</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Metal</td><td>Viking Metal</td><td>&nbsp;</td><td></td></tr>
</tbody></table>
</details>

### Musical Theatre and Entertainment

<details><summary>click to open/close</summary>
<table>
<tbody>
 <tr><td>Musical Theatre and Entertainment</td><td>Cabaret</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Musical Theatre and Entertainment</td><td>Cải lương</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Musical Theatre and Entertainment</td><td>Cuplé</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Musical Theatre and Entertainment</td><td>Dutch Cabaret</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Musical Theatre and Entertainment</td><td>Kabarett</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Musical Theatre and Entertainment</td><td>Kanto</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Musical Theatre and Entertainment</td><td>Murga</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Musical Theatre and Entertainment</td><td>Music Hall</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Musical Theatre and Entertainment</td><td>Revue</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Musical Theatre and Entertainment</td><td>Show Tunes</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Musical Theatre and Entertainment</td><td>Show Tunes</td><td>Operetta</td><td>&nbsp;</td></tr>
 <tr><td>Musical Theatre and Entertainment</td><td>Show Tunes</td><td>Operetta</td><td>Zarzuela</td></tr>
 <tr><td>Musical Theatre and Entertainment</td><td>Vaudeville</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Musical Theatre and Entertainment</td><td>Vaudeville</td><td>Vaudeville Blues</td><td></td></tr>
</tbody></table>
</details>

### New Age

<details><summary>click to open/close</summary>
<table>
<tbody>
 <tr><td>New Age</td><td>&nbsp;</td></tr>
 <tr><td>New Age</td><td>Andean New Age</td></tr>
 <tr><td>New Age</td><td>Celtic New Age</td></tr>
 <tr><td>New Age</td><td>Neoclassical New Age</td></tr>
</tbody></table>
</details>

### Pop

<details><summary>click to open/close</summary>
<table>
<tbody>
 <tr><td>Pop</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Adult Contemporary</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Adult Contemporary</td><td>City Pop</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Ambient Pop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Arabic Pop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Arabic Pop</td><td>Al Jeel</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Art Pop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Balkan Pop-Folk</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Balkan Pop-Folk</td><td>Chalga</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Balkan Pop-Folk</td><td>Manele</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Balkan Pop-Folk</td><td>Modern Laika</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Balkan Pop-Folk</td><td>Musika popullore</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Balkan Pop-Folk</td><td>Muzică de mahala</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Balkan Pop-Folk</td><td>Skiladika</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Balkan Pop-Folk</td><td>Tallava</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Balkan Pop-Folk</td><td>Turbo-Folk</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Baroque Pop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Bitpop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Bitpop</td><td>Picopop</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Blue-Eyed Soul</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Boy Band</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Brill Building</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Bubblegum</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Cambodian Pop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>CCM (Contemporary Christian Music)</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>CCM (Contemporary Christian Music)</td><td>Praise & Worship</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Classical Crossover</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Country Pop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Country Pop</td><td>Bro-Country</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Country Pop</td><td>Nashville Sound</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Country Pop</td><td>Urban Cowboy</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>C-Pop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>C-Pop</td><td>Cantopop</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>C-Pop</td><td>Mandopop</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Dance-Pop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Dance-Pop</td><td>Afrobeats</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Dance-Pop</td><td>Bubblegum [South Africa]</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Dance-Pop</td><td>Disco polo</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Dance-Pop</td><td>Eurodance</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Dance-Pop</td><td>Eurodance</td><td>Bubblegum Dance</td></tr>
 <tr><td>Pop</td><td>Dance-Pop</td><td>Eurodance</td><td>Italo Dance</td></tr>
 <tr><td>Pop</td><td>Dance-Pop</td><td>Freestyle</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Dance-Pop</td><td>Freestyle</td><td>Latin Freestyle</td></tr>
 <tr><td>Pop</td><td>Dance-Pop</td><td>Funk Melody</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Dance-Pop</td><td>Romanian Dance-Pop</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Dance-Pop</td><td>Tecnorumba</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Dangdut</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Dangdut</td><td>Dangdut koplo</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Dansbandsmusik</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Dansktop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Easy Listening</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Easy Listening</td><td>Exotica</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Easy Listening</td><td>Light Music</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Easy Listening</td><td>Lounge</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Easy Listening</td><td>Lounge</td><td>Cocktail</td></tr>
 <tr><td>Pop</td><td>Easy Listening</td><td>Pops Orchestra</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Easy Listening</td><td>Space Age Pop</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Electropop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Europop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Flamenco Pop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Folk Pop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>French Pop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Girl Group</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Glitch Pop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Indian Pop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Indie Pop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Indie Pop</td><td>C86</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Indie Pop</td><td>Chamber Pop</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Indie Pop</td><td>Twee Pop</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Italo Pop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Jazz Pop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>J-Pop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>J-Pop</td><td>Denpa</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>K-Pop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Latin Pop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Latin Pop</td><td>Cumbia cheta</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Latin Pop</td><td>Tropipop</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Nederpop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>New Romantic</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Palingsound</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Persian Pop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Pop Ghazal</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Pop Raï</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Pop Rock</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Pop Rock</td><td>Beat Music</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Pop Rock</td><td>Beat Music</td><td>Freakbeat</td></tr>
 <tr><td>Pop</td><td>Pop Rock</td><td>Beat Music</td><td>Group Sounds</td></tr>
 <tr><td>Pop</td><td>Pop Rock</td><td>Beat Music</td><td>Jovem Guarda</td></tr>
 <tr><td>Pop</td><td>Pop Rock</td><td>Beat Music</td><td>Merseybeat</td></tr>
 <tr><td>Pop</td><td>Pop Rock</td><td>Beat Music</td><td>Nederbeat</td></tr>
 <tr><td>Pop</td><td>Pop Rock</td><td>Britpop</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Pop Rock</td><td>Jangle Pop</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Pop Rock</td><td>Manila Sound</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Pop Rock</td><td>Piano Rock</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Pop Rock</td><td>Power Pop</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Pop Rock</td><td>Soft Rock</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Pop Rock</td><td>Stereo</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Pop Rock</td><td>Vocal Surf</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Pop Rock</td><td>Yacht Rock</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Pop Soul</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Pop Sunda</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Progressive Pop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Psychedelic Pop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Rabiz</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Rigsar</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Rumba catalana</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Russian Chanson</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Schlager</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Schlager</td><td>Humppa</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Schlager</td><td>Levenslied</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Schlager</td><td>Volkstümliche Musik</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Sertanejo universitário</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Shibuya-kei</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Sophisti-Pop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Sunshine Pop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Synthpop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Synthpop</td><td>Futurepop</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Synthpop</td><td>Techno Kayō</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Teen Pop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Traditional Pop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Traditional Pop</td><td>British Dance Band</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Traditional Pop</td><td>Romanţe</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Traditional Pop</td><td>Standards</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Turkish Pop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>V-Pop</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>Pop</td><td>Yé-yé</td><td>&nbsp;</td><td></td></tr>
</tbody></table>
</details>

### Regional Music

<details><summary>click to open/close</summary>
<table>
<tr><td>Regional Music</td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td></td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>African Folk Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>African Folk Music</td><td>Ambasse bey</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>African Folk Music</td><td>Apala</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>African Folk Music</td><td>Batuque</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>African Folk Music</td><td>Dagomba Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>African Folk Music</td><td>Gnawa</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>African Folk Music</td><td>Kabye Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>African Folk Music</td><td>Kilapanga</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>African Folk Music</td><td>Mbenga-Mbuti Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>African Folk Music</td><td>Moutya</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>African Folk Music</td><td>Ngoma</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>African Folk Music</td><td>Ngoma</td><td>Unyago</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>African Folk Music</td><td>Semba</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>African Folk Music</td><td>Southern African Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>African Folk Music</td><td>Southern African Folk Music</td><td>Khoisan Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>African Folk Music</td><td>Southern African Folk Music</td><td>Nguni Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>African Folk Music</td><td>Southern African Folk Music</td><td>Shona Mbira Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>African Folk Music</td><td>Southern African Folk Music</td><td>Sotho-Tswana Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>African Folk Music</td><td>Tchinkoumé</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>African Folk Music</td><td>Traditional Maloya</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>African Folk Music</td><td>Traditional Séga</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>African Folk Music</td><td>Zinli</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Afro-Jazz</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Cape Verdean Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Cape Verdean Music</td><td>Batuque</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Cape Verdean Music</td><td>Coladeira</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Cape Verdean Music</td><td>Funaná</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Cape Verdean Music</td><td>Morna</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Central African Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Central African Music</td><td>Ambasse bey</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Central African Music</td><td>Assiko</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Central African Music</td><td>Bend-Skin</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Central African Music</td><td>Bikutsi</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Central African Music</td><td>Kalindula</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Central African Music</td><td>Kilapanga</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Central African Music</td><td>Kizomba</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Central African Music</td><td>Kizomba</td><td>Tarraxinha</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Central African Music</td><td>Kuduro</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Central African Music</td><td>Kuduro</td><td>Batida</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Central African Music</td><td>Makossa</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Central African Music</td><td>Mbenga-Mbuti Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Central African Music</td><td>Semba</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Central African Music</td><td>Soukous</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Central African Music</td><td>Soukous</td><td>Kwassa kwassa</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Central African Music</td><td>Tradi-Modern</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Central African Music</td><td>Zamrock</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>East African Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>East African Music</td><td>Benga</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>East African Music</td><td>Bongo Flava</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>East African Music</td><td>Comorian Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>East African Music</td><td>Genge</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>East African Music</td><td>Kapuka</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>East African Music</td><td>Kidumbak</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>East African Music</td><td>Marrabenta</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>East African Music</td><td>Muziki wa dansi</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>East African Music</td><td>Ngoma</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>East African Music</td><td>Ngoma</td><td>Unyago</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>East African Music</td><td>Singeli</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>East African Music</td><td>Taarab</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Malagasy Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Malagasy Music</td><td>Malagasy Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Malagasy Music</td><td>Salegy</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>North African Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>North African Music</td><td>Algerian Chaabi</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>North African Music</td><td>Ancient Egyptian Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>North African Music</td><td>Andalusian Classical Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>North African Music</td><td>Berber Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>North African Music</td><td>Berber Music</td><td>Izlan</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>North African Music</td><td>Berber Music</td><td>Sahrawi Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>North African Music</td><td>Berber Music</td><td>Tuareg Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>North African Music</td><td>Berber Music</td><td>Tuareg Music</td><td>Takamba</td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>North African Music</td><td>Berber Music</td><td>Tuareg Music</td><td>Tishoumaren</td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>North African Music</td><td>Coptic Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>North African Music</td><td>Gnawa</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>North African Music</td><td>Moorish Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>North African Music</td><td>Raï</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>North African Music</td><td>Raï</td><td>Pop Raï</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>North African Music</td><td>Raï</td><td>Traditional Raï</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>North African Music</td><td>Sha'abi</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>North African Music</td><td>Sha'abi</td><td>Mahraganat</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Northeastern African Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Northeastern African Music</td><td>Ethio-Jazz</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Northeastern African Music</td><td>Ethiopian Church Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Northeastern African Music</td><td>Nubian Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Northeastern African Music</td><td>Qaraami</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Northeastern African Music</td><td>Tigrinya</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Northeastern African Music</td><td>Tizita</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Southern African Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Southern African Music</td><td>Bubblegum [South Africa]</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Southern African Music</td><td>Bulawayo Jazz</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Southern African Music</td><td>Cape Jazz</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Southern African Music</td><td>Chimurenga</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Southern African Music</td><td>Famo</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Southern African Music</td><td>Gqom</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Southern African Music</td><td>Jit</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Southern African Music</td><td>Kwaito</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Southern African Music</td><td>Marabi</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Southern African Music</td><td>Marabi</td><td>Kwela</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Southern African Music</td><td>Marabi</td><td>Mbaqanga</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Southern African Music</td><td>Mbube</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Southern African Music</td><td>Shangaan Electro</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Southern African Music</td><td>Southern African Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Southern African Music</td><td>Southern African Folk Music</td><td>Khoisan Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Southern African Music</td><td>Southern African Folk Music</td><td>Nguni Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Southern African Music</td><td>Southern African Folk Music</td><td>Shona Mbira Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Southern African Music</td><td>Southern African Folk Music</td><td>Sotho-Tswana Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Southern African Music</td><td>Township Jive</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>Southern African Music</td><td>Tsonga Disco</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>West African Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>West African Music</td><td>Afro-Funk</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>West African Music</td><td>Afro-Rock</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>West African Music</td><td>Akan Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>West African Music</td><td>Dagomba Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>West African Music</td><td>Ewe Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>West African Music</td><td>Fon Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>West African Music</td><td>Fon Music</td><td>Tchinkoumé</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>West African Music</td><td>Fon Music</td><td>Tchink System</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>West African Music</td><td>Fon Music</td><td>Zinli</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>West African Music</td><td>Gumbe</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>West African Music</td><td>Highlife</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>West African Music</td><td>Highlife</td><td>Burger-Highlife</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>West African Music</td><td>Highlife</td><td>Hiplife</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>West African Music</td><td>Kabye Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>West African Music</td><td>Mande Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>West African Music</td><td>Palm Wine Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>West African Music</td><td>Songhai Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>West African Music</td><td>Wassoulou</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>West African Music</td><td>Wolof Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>West African Music</td><td>Wolof Music</td><td>Mbalax</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>West African Music</td><td>Wolof Music</td><td>Tassu</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>West African Music</td><td>Yoruba Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>West African Music</td><td>Yoruba Music</td><td>Afrobeat</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>West African Music</td><td>Yoruba Music</td><td>Apala</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>West African Music</td><td>Yoruba Music</td><td>Fuji</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>West African Music</td><td>Yoruba Music</td><td>Jùjú</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>West African Music</td><td>Yoruba Music</td><td>Santería Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>West African Music</td><td>Yoruba Music</td><td>Waka</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>West African Music</td><td>Zouglou</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>African Music</td><td>West African Music</td><td>Zouglou</td><td>Coupé-Décalé</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Ancient Music</td><td></td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Ancient Music</td><td>Ancient Chinese Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Ancient Music</td><td>Ancient Egyptian Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Ancient Music</td><td>Ancient Greek Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Ancient Music</td><td>Ancient Roman Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Ancient Music</td><td>Hyang-ak</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Ancient Music</td><td>Mesopotamian Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Arabic Music</td><td></td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Arabic Music</td><td>Algerian Chaabi</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Arabic Music</td><td>Arabic Classical Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Arabic Music</td><td>Arabic Classical Music</td><td>Al-Maqam Al-Iraqi</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Arabic Music</td><td>Arabic Classical Music</td><td>Andalusian Classical Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Arabic Music</td><td>Arabic Folk Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Arabic Music</td><td>Arabic Folk Music</td><td>Arabic Bellydance Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Arabic Music</td><td>Arabic Jazz</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Arabic Music</td><td>Arabic Pop</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Arabic Music</td><td>Arabic Pop</td><td>Al Jeel</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Arabic Music</td><td>Dabke</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Arabic Music</td><td>Raï</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Arabic Music</td><td>Raï</td><td>Pop Raï</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Arabic Music</td><td>Raï</td><td>Traditional Raï</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Arabic Music</td><td>Sha'abi</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Arabic Music</td><td>Sha'abi</td><td>Mahraganat</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Arabic Music</td><td>Traditional Arabic Pop</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Balkan Music</td><td></td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Balkan Music</td><td>Balkan Folk Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Balkan Music</td><td>Balkan Folk Music</td><td>Albanian Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Balkan Music</td><td>Balkan Folk Music</td><td>Albanian Folk Music</td><td>Lab Polyphony</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Balkan Music</td><td>Balkan Folk Music</td><td>Albanian Folk Music</td><td>Musika popullore</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Balkan Music</td><td>Balkan Folk Music</td><td>Albanian Folk Music</td><td>Tosk Polyphony</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Balkan Music</td><td>Balkan Folk Music</td><td>Balkan Brass Band</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Balkan Music</td><td>Balkan Folk Music</td><td>Bulgarian Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Balkan Music</td><td>Balkan Folk Music</td><td>Csango Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Balkan Music</td><td>Balkan Folk Music</td><td>Gagauz Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Balkan Music</td><td>Balkan Folk Music</td><td>Greek Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Balkan Music</td><td>Balkan Folk Music</td><td>Greek Folk Music</td><td>Dimotika</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Balkan Music</td><td>Balkan Folk Music</td><td>Greek Folk Music</td><td>Kritika</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Balkan Music</td><td>Balkan Folk Music</td><td>Greek Folk Music</td><td>Nisiotika Aigaiou</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Balkan Music</td><td>Balkan Folk Music</td><td>Greek Folk Music</td><td>Nisiotika Ioniou</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Balkan Music</td><td>Balkan Folk Music</td><td>Greek Folk Music</td><td>Rembetika</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Balkan Music</td><td>Balkan Folk Music</td><td>Lautareasca</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Balkan Music</td><td>Balkan Folk Music</td><td>Macedonian Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Balkan Music</td><td>Balkan Folk Music</td><td>Macedonian Folk Music</td><td>Čalgija</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Balkan Music</td><td>Balkan Folk Music</td><td>Sevdalinka</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Balkan Music</td><td>Balkan Folk Music</td><td>Starogradska muzika</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Balkan Music</td><td>Balkan Folk Music</td><td>Starogradska muzika</td><td>Čalgija</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Balkan Music</td><td>Balkan Pop-Folk</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Balkan Music</td><td>Balkan Pop-Folk</td><td>Chalga</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Balkan Music</td><td>Balkan Pop-Folk</td><td>Manele</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Balkan Music</td><td>Balkan Pop-Folk</td><td>Modern Laika</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Balkan Music</td><td>Balkan Pop-Folk</td><td>Musika popullore</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Balkan Music</td><td>Balkan Pop-Folk</td><td>Muzica de mahala</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Balkan Music</td><td>Balkan Pop-Folk</td><td>Skiladika</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Balkan Music</td><td>Balkan Pop-Folk</td><td>Tallava</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Balkan Music</td><td>Balkan Pop-Folk</td><td>Turbo-Folk</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Balkan Music</td><td>Entechna</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Balkan Music</td><td>Entechna</td><td>Entechna Laika</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Balkan Music</td><td>Entechna</td><td>Neo Kyma</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Balkan Music</td><td>Laika</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Balkan Music</td><td>Laika</td><td>Entechna Laika</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Balkan Music</td><td>Laika</td><td>Modern Laika</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Balkan Music</td><td>Laika</td><td>Skiladika</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Balkan Music</td><td>Romante</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td></td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Afoxé</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Axé</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Axé</td><td>Samba-Reggae</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Axé</td><td>Swingueira</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Baião</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Bossa nova</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Bossa nova</td><td>Samba-Jazz</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Brazilian Classical Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Brega</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Brega</td><td>Tecnobrega</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Candomblé Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Capoeira Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Carimbó</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Choro</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Choro</td><td>Samba-choro</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Coco</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Coco</td><td>Ciranda</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Forró</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Forró</td><td>Forró eletrônico</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Forró</td><td>Forró universitário</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Frevo</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Funk carioca</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Funk carioca</td><td>Funk Melody</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Funk carioca</td><td>Funk ostentação</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Funk carioca</td><td>Funk proibidão</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Jongo</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Lambada</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Mangue Beat</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Maracatu</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Maxixe</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Música Popular Brasileira</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Música Popular Brasileira</td><td>Tropicália</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Música gaúcha</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Música gaúcha</td><td>Vanera</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Repente</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Samba</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Samba</td><td>Batucada</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Samba</td><td>Marchinha</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Samba</td><td>Pagode</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Samba</td><td>Pagode</td><td>Pagode romântico</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Samba</td><td>Pagode</td><td>Swingueira</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Samba</td><td>Partido alto</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Samba</td><td>Samba-canção</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Samba</td><td>Samba-choro</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Samba</td><td>Samba de breque</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Samba</td><td>Samba de roda</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Samba</td><td>Samba-enredo</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Samba</td><td>Samba-exaltação</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Samba</td><td>Samba-Rock</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Samba</td><td>Samba Soul</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Sertanejo</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Sertanejo</td><td>Rasqueado</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Sertanejo</td><td>Sertanejo de raiz</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Sertanejo</td><td>Sertanejo de raiz</td><td>Moda de viola</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Sertanejo</td><td>Sertanejo romântico</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Sertanejo</td><td>Sertanejo romântico</td><td>Sertanejo universitário</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Vanguarda paulista</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Brazilian Music</td><td>Xote</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td></td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>Calypso</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>Calypso</td><td>Spouge</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>Caribbean Folk Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>Caribbean Folk Music</td><td>Fungi</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>Caribbean Folk Music</td><td>Garifuna Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>Caribbean Folk Music</td><td>Junkanoo</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>Caribbean Folk Music</td><td>Méringue</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>Caribbean Folk Music</td><td>Ripsaw</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>Caribbean Folk Music</td><td>Trinidadian Cariso</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>Caribbean Folk Music</td><td>Tumba</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>Caribbean Folk Music</td><td>Virgin Islander Cariso</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>French Caribbean Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>French Caribbean Music</td><td>Biguine</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>French Caribbean Music</td><td>Bouyon</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>French Caribbean Music</td><td>Cadence Lypso</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>French Caribbean Music</td><td>Cadence Rampa</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>French Caribbean Music</td><td>Compas</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>French Caribbean Music</td><td>Dennery Segment</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>French Caribbean Music</td><td>Gwo Ka</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>French Caribbean Music</td><td>Méringue</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>French Caribbean Music</td><td>Rara</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>French Caribbean Music</td><td>Rasin</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>French Caribbean Music</td><td>Tumbélé</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>French Caribbean Music</td><td>Zouk</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>French Caribbean Music</td><td>Zouk</td><td>Cabo-Zouk</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>French Caribbean Music</td><td>Zouk</td><td>Zouk Love</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>Goombay</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>Jamaican Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>Jamaican Music</td><td>Dancehall</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>Jamaican Music</td><td>Dancehall</td><td>Digital Dancehall</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>Jamaican Music</td><td>Dancehall</td><td>Flex Dance Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>Jamaican Music</td><td>Dancehall</td><td>Ragga</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>Jamaican Music</td><td>Jamaican Ska</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>Jamaican Music</td><td>Mento</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>Jamaican Music</td><td>Nyahbinghi</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>Jamaican Music</td><td>Reggae</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>Jamaican Music</td><td>Reggae</td><td>Deejay</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>Jamaican Music</td><td>Reggae</td><td>Digital Dancehall</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>Jamaican Music</td><td>Reggae</td><td>Dub</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>Jamaican Music</td><td>Reggae</td><td>Dub</td><td>Novo Dub</td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>Jamaican Music</td><td>Reggae</td><td>Lovers Rock</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>Jamaican Music</td><td>Reggae</td><td>Pop Reggae</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>Jamaican Music</td><td>Reggae</td><td>Roots Reggae</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>Jamaican Music</td><td>Reggae</td><td>Roots Reggae</td><td>Dub Poetry</td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>Jamaican Music</td><td>Reggae</td><td>Seggae</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>Jamaican Music</td><td>Reggae</td><td>Skinhead Reggae</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>Jamaican Music</td><td>Rocksteady</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>Kaseko</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>Parang</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>Punta</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>Soca</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>Soca</td><td>Chutney</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>Soca</td><td>Dennery Segment</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caribbean Music</td><td>Soca</td><td>Rapso</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caucasian Music</td><td></td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caucasian Music</td><td>Caucasian Folk Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caucasian Music</td><td>Caucasian Folk Music</td><td>Abkhazian Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caucasian Music</td><td>Caucasian Folk Music</td><td>Avar Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caucasian Music</td><td>Caucasian Folk Music</td><td>Chechen Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caucasian Music</td><td>Caucasian Folk Music</td><td>Circassian Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caucasian Music</td><td>Caucasian Folk Music</td><td>Georgian Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caucasian Music</td><td>Caucasian Folk Music</td><td>Ossetian Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Caucasian Music</td><td>Rabiz</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Chanson</td><td></td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Chanson</td><td>Chanson alternative</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Chanson</td><td>Chanson à texte</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Chanson</td><td>Chanson réaliste</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Chanson</td><td>Nouvelle chanson française</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Christian Liturgical Music</td><td></td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Christian Liturgical Music</td><td>Armenian Church Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Christian Liturgical Music</td><td>Byzantine Chant</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Christian Liturgical Music</td><td>Coptic Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Christian Liturgical Music</td><td>Ethiopian Church Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Christian Liturgical Music</td><td>Gaelic Psalm</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Christian Liturgical Music</td><td>Plainsong</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Christian Liturgical Music</td><td>Plainsong</td><td>Gregorian Chant</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Christian Liturgical Music</td><td>Russian Orthodox Liturgical Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Christian Liturgical Music</td><td>Russian Orthodox Liturgical Music</td><td>Znamenny Chant</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Christian Liturgical Music</td><td>Syriac Chant</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Country</td><td></td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Country</td><td>Alt-Country</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Country</td><td>Alt-Country</td><td>Gothic Country</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Country</td><td>Americana</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Country</td><td>Contemporary Country</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Country</td><td>Contemporary Country</td><td>Country Rap</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Country</td><td>Contemporary Country</td><td>Neo-Traditionalist Country</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Country</td><td>Country & Irish</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Country</td><td>Country Pop</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Country</td><td>Country Pop</td><td>Bro-Country</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Country</td><td>Country Pop</td><td>Nashville Sound</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Country</td><td>Country Pop</td><td>Urban Cowboy</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Country</td><td>Honky Tonk</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Country</td><td>Honky Tonk</td><td>Bakersfield Sound</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Country</td><td>Honky Tonk</td><td>Truck Driving Country</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Country</td><td>Progressive Country</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Country</td><td>Progressive Country</td><td>Outlaw Country</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Country</td><td>Progressive Country</td><td>Outlaw Country</td><td>Red Dirt</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Country</td><td>Progressive Country</td><td>Progressive Bluegrass</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Country</td><td>Traditional Country</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Country</td><td>Traditional Country</td><td>Bluegrass</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Country</td><td>Traditional Country</td><td>Bluegrass</td><td>Bluegrass Gospel</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Country</td><td>Traditional Country</td><td>Close Harmony</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Country</td><td>Traditional Country</td><td>Country Boogie</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Country</td><td>Traditional Country</td><td>Country Gospel</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Country</td><td>Traditional Country</td><td>Country Gospel</td><td>Bluegrass Gospel</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Country</td><td>Traditional Country</td><td>Country Yodeling</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Country</td><td>Traditional Country</td><td>Cowboy</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Country</td><td>Traditional Country</td><td>Old-Time</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Country</td><td>Western Swing</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td></td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>Ancient Chinese Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>Cải lương</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Classical Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Classical Music</td><td>Chinese Classical Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Classical Music</td><td>Chinese Classical Music</td><td>Baisha Xiyue</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Classical Music</td><td>Chinese Classical Music</td><td>Chinese Opera</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Classical Music</td><td>Chinese Classical Music</td><td>Chinese Opera</td><td>Cantonese Opera</td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Classical Music</td><td>Chinese Classical Music</td><td>Chinese Opera</td><td>Peking Opera</td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Classical Music</td><td>Chinese Classical Music</td><td>Chinese Opera</td><td>Shaoxing Opera</td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Classical Music</td><td>Chinese Classical Music</td><td>Dongjing</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Classical Music</td><td>Chinese Classical Music</td><td>Yayue</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Classical Music</td><td>Japanese Classical Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Classical Music</td><td>Japanese Classical Music</td><td>Gagaku</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Classical Music</td><td>Japanese Classical Music</td><td>Heikyoku</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Classical Music</td><td>Japanese Classical Music</td><td>Jiuta</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Classical Music</td><td>Japanese Classical Music</td><td>Jōruri</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Classical Music</td><td>Japanese Classical Music</td><td>Nagauta</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Classical Music</td><td>Japanese Classical Music</td><td>Noh</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Classical Music</td><td>Japanese Classical Music</td><td>Shomyo</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Classical Music</td><td>Japanese Classical Music</td><td>Soukyoku</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Classical Music</td><td>Japanese Classical Music</td><td>Soukyoku</td><td>Danmono</td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Classical Music</td><td>Japanese Classical Music</td><td>Soukyoku</td><td>Kumiuta</td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Classical Music</td><td>Korean Classical Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Classical Music</td><td>Korean Classical Music</td><td>Aak</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Classical Music</td><td>Korean Classical Music</td><td>Dang-ak</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Classical Music</td><td>Korean Classical Music</td><td>Hyang-ak</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Classical Music</td><td>Korean Classical Music</td><td>Jeong-ak</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Classical Music</td><td>Vietnamese Classical Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Classical Music</td><td>Vietnamese Classical Music</td><td>Vietnamese Opera</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Folk Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Folk Music</td><td>Ainu Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Folk Music</td><td>Ainu Folk Music</td><td>Upopo</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Folk Music</td><td>Ainu Folk Music</td><td>Yukar</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Folk Music</td><td>Chinese Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Folk Music</td><td>Chinese Folk Music</td><td>Guoyue</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Folk Music</td><td>Chinese Folk Music</td><td>Han Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Folk Music</td><td>Chinese Folk Music</td><td>Jiangnan Sizhu</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Folk Music</td><td>Indigenous Taiwanese Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Folk Music</td><td>Japanese Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Folk Music</td><td>Japanese Folk Music</td><td>Amami Shimauta</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Folk Music</td><td>Japanese Folk Music</td><td>Heikyoku</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Folk Music</td><td>Japanese Folk Music</td><td>Kagura</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Folk Music</td><td>Japanese Folk Music</td><td>Kouta</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Folk Music</td><td>Japanese Folk Music</td><td>Min'yō</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Folk Music</td><td>Japanese Folk Music</td><td>Ondō</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Folk Music</td><td>Japanese Folk Music</td><td>Rōkyoku</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Folk Music</td><td>Japanese Folk Music</td><td>Taiko</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Folk Music</td><td>Japanese Folk Music</td><td>Tsugaru Shamisen</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Folk Music</td><td>Korean Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Folk Music</td><td>Korean Folk Music</td><td>Sanjo</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>East Asian Folk Music</td><td>Korean Folk Music</td><td>Sinawi</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>Japanese Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>Japanese Music</td><td>Enka</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>Japanese Music</td><td>Japanese Classical Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>Japanese Music</td><td>Japanese Classical Music</td><td>Gagaku</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>Japanese Music</td><td>Japanese Classical Music</td><td>Heikyoku</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>Japanese Music</td><td>Japanese Classical Music</td><td>Jiuta</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>Japanese Music</td><td>Japanese Classical Music</td><td>Jōruri</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>Japanese Music</td><td>Japanese Classical Music</td><td>Nagauta</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>Japanese Music</td><td>Japanese Classical Music</td><td>Noh</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>Japanese Music</td><td>Japanese Classical Music</td><td>Shomyo</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>Japanese Music</td><td>Japanese Classical Music</td><td>Soukyoku</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>Japanese Music</td><td>Japanese Classical Music</td><td>Soukyoku</td><td>Danmono</td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>Japanese Music</td><td>Japanese Classical Music</td><td>Soukyoku</td><td>Kumiuta</td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>Japanese Music</td><td>Japanese Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>Japanese Music</td><td>Japanese Folk Music</td><td>Amami Shimauta</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>Japanese Music</td><td>Japanese Folk Music</td><td>Heikyoku</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>Japanese Music</td><td>Japanese Folk Music</td><td>Kagura</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>Japanese Music</td><td>Japanese Folk Music</td><td>Kouta</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>Japanese Music</td><td>Japanese Folk Music</td><td>Min'yō</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>Japanese Music</td><td>Japanese Folk Music</td><td>Ondō</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>Japanese Music</td><td>Japanese Folk Music</td><td>Rōkyoku</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>Japanese Music</td><td>Japanese Folk Music</td><td>Taiko</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>Japanese Music</td><td>Japanese Folk Music</td><td>Tsugaru Shamisen</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>Japanese Music</td><td>Kayōkyoku</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>Japanese Music</td><td>Kayōkyoku</td><td>Mood Kayō</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>Japanese Music</td><td>Ryūkōka</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>Japanese Music</td><td>Shinkyoku</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>Japanese Music</td><td>Techno Kayō</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>Japanese Music</td><td>Visual kei</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>Nhạc vàng</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>Shidaiqu</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>Trot</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>East Asian Music</td><td>Vietnamese Bolero</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Greek Music</td><td></td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Greek Music</td><td>Ancient Greek Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Greek Music</td><td>Byzantine Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Greek Music</td><td>Byzantine Music</td><td>Byzantine Chant</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Greek Music</td><td>Entechna</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Greek Music</td><td>Entechna</td><td>Entechna Laika</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Greek Music</td><td>Entechna</td><td>Neo Kyma</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Greek Music</td><td>Greek Folk Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Greek Music</td><td>Greek Folk Music</td><td>Dimotika</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Greek Music</td><td>Greek Folk Music</td><td>Kritika</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Greek Music</td><td>Greek Folk Music</td><td>Nisiotika Aigaiou</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Greek Music</td><td>Greek Folk Music</td><td>Nisiotika Ioniou</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Greek Music</td><td>Greek Folk Music</td><td>Rembetika</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Greek Music</td><td>Laika</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Greek Music</td><td>Laika</td><td>Entechna Laika</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Greek Music</td><td>Laika</td><td>Modern Laika</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Greek Music</td><td>Laika</td><td>Skiladika</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td></td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Canción melódica</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Andean Rock</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Bachata</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Bambuco</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Bolero</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Bolero</td><td>Bolero son</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Bolero</td><td>Filin</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Bomba</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Boogaloo</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Candombe</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Canto a lo poeta</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Chacarera</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Chamamé</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Champeta</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Cuarteto</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Cuban Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Cuban Music</td><td>Chachachá</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Cuban Music</td><td>Changüí</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Cuban Music</td><td>Conga</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Cuban Music</td><td>Cuban Charanga</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Cuban Music</td><td>Cuban Rumba</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Cuban Music</td><td>Cuban Rumba</td><td>Guaguancó</td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Cuban Music</td><td>Danzón</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Cuban Music</td><td>Descarga</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Cuban Music</td><td>Filin</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Cuban Music</td><td>Guajira</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Cuban Music</td><td>Guaracha</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Cuban Music</td><td>Habanera</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Cuban Music</td><td>Mambo</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Cuban Music</td><td>Mozambique</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Cuban Music</td><td>Pachanga</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Cuban Music</td><td>Pilón</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Cuban Music</td><td>Santería Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Cuban Music</td><td>Son Cubano</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Cuban Music</td><td>Son Cubano</td><td>Bolero son</td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Cuban Music</td><td>Songo</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Cuban Music</td><td>Timba</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Cuban Music</td><td>Trova</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Cuban Music</td><td>Trova</td><td>Nueva trova</td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Cueca</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Cumbia</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Cumbia</td><td>Bullerengue</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Cumbia</td><td>Cumbia Argentina</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Cumbia</td><td>Cumbia Argentina</td><td>Cumbia cheta</td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Cumbia</td><td>Cumbia Argentina</td><td>Cumbia Villera</td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Cumbia</td><td>Cumbia Mexicana</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Cumbia</td><td>Cumbia Mexicana</td><td>Cumbia sonidera</td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Cumbia</td><td>Cumbia Peruana</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Cumbia</td><td>Digital Cumbia</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Cumbia</td><td>Merecumbé</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Cumbia</td><td>Porro</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Currulao</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Dembow</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Guarania</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Jibaro</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Joropo</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Latin Alternative</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Latin Classical Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Latin Disco</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Latin Electronic</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Latin Electronic</td><td>Digital Cumbia</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Latin Electronic</td><td>Electro latino</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Latin Electronic</td><td>Electrotango</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Latin Electronic</td><td>Latin House</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Latin Electronic</td><td>Nortec</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Latin Electronic</td><td>Tribal Guarachero</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Latin Freestyle</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Latin Funk</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Latin Jazz</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Latin Jazz</td><td>Afro-Cuban Jazz</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Latin Pop</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Latin Pop</td><td>Cumbia cheta</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Latin Pop</td><td>Tropipop</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Latin Rock</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Latin Soul</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Malagueña venezolana</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Merengue</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Merengue</td><td>Merecumbé</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Mexican Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Mexican Music</td><td>Bandas de viento de México</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Mexican Music</td><td>Bandas de viento de México</td><td>Banda Sinaloense</td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Mexican Music</td><td>Bandas de viento de México</td><td>Banda Sinaloense</td><td>Movimiento Alterado</td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Mexican Music</td><td>Bandas de viento de México</td><td>Banda Sinaloense</td><td>Tecnobanda</td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Mexican Music</td><td>Chilena</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Mexican Music</td><td>Corrido</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Mexican Music</td><td>Mariachi</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Mexican Music</td><td>Mexican Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Mexican Music</td><td>Mexican Folk Music</td><td>Canto cardenche</td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Mexican Music</td><td>Mexican Folk Music</td><td>Ranchera</td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Mexican Music</td><td>Mexican Folk Music</td><td>Son Calentano</td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Mexican Music</td><td>Mexican Folk Music</td><td>Son Huasteco</td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Mexican Music</td><td>Mexican Folk Music</td><td>Son Istmeño</td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Mexican Music</td><td>Mexican Folk Music</td><td>Son Jarocho</td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Mexican Music</td><td>Mexican Folk Music</td><td>Trova yucateca</td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Mexican Music</td><td>Norteño</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Mexican Music</td><td>Norteño</td><td>Duranguense</td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Mexican Music</td><td>Norteño</td><td>Movimiento Alterado</td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Mexican Music</td><td>Tejano</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Milonga</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Muliza</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Música criolla peruana</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Música criolla peruana</td><td>Marinera</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Música criolla peruana</td><td>Tondero</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Música criolla peruana</td><td>Vals criollo</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Nueva canción latinoamericana</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Nueva canción latinoamericana</td><td>Nueva trova</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Onda nueva</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Pasillo</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Plena</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Reggaeton</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Rhumba</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Salsa</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Salsa</td><td>Salsa Dura</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Salsa</td><td>Salsa Romántica</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Salsa</td><td>Timba</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Tamborera</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Tamborito</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Tango</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Tango</td><td>Finnish Tango</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Tango</td><td>Tango Nuevo</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Tango</td><td>Tango Nuevo</td><td>Electrotango</td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Taquirari</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Vallenato</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Latin American Music</td><td>Zamba</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Murga</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Spanish Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Spanish Music</td><td>Andalusian Rock</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Spanish Music</td><td>Bolero español</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Spanish Music</td><td>Flamenco Pop</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Spanish Music</td><td>Nueva canción española</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Spanish Music</td><td>Rumba catalana</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Spanish Music</td><td>Spanish Classical Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Spanish Music</td><td>Spanish Classical Music</td><td>Zarzuela</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Spanish Music</td><td>Spanish Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Spanish Music</td><td>Spanish Folk Music</td><td>Andalusian Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Spanish Music</td><td>Spanish Folk Music</td><td>Andalusian Folk Music</td><td>Saeta</td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Spanish Music</td><td>Spanish Folk Music</td><td>Aragonese Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Spanish Music</td><td>Spanish Folk Music</td><td>Asturian Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Spanish Music</td><td>Spanish Folk Music</td><td>Canarian Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Spanish Music</td><td>Spanish Folk Music</td><td>Chotis Madrileño</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Spanish Music</td><td>Spanish Folk Music</td><td>Copla</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Spanish Music</td><td>Spanish Folk Music</td><td>Cuplé</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Spanish Music</td><td>Spanish Folk Music</td><td>Flamenco</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Spanish Music</td><td>Spanish Folk Music</td><td>Flamenco</td><td>Flamenco nuevo</td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Spanish Music</td><td>Spanish Folk Music</td><td>Flamenco</td><td>Flamenco nuevo</td><td>Flamenco Jazz</td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Spanish Music</td><td>Spanish Folk Music</td><td>Flamenco</td><td>Rumba Flamenca</td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Spanish Music</td><td>Spanish Folk Music</td><td>Galician Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Spanish Music</td><td>Spanish Folk Music</td><td>Pasodoble</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Spanish Music</td><td>Spanish Folk Music</td><td>Sevillanas</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Spanish Music</td><td>Tecnorumba</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Hispanic Music</td><td>Tonada</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Islamic Modal Music</td><td></td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Islamic Modal Music</td><td>Anasheed</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Islamic Modal Music</td><td>Arabic Classical Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Islamic Modal Music</td><td>Arabic Classical Music</td><td>Al-Maqam Al-Iraqi</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Islamic Modal Music</td><td>Arabic Classical Music</td><td>Andalusian Classical Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Islamic Modal Music</td><td>Azerbaijani Traditional Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Islamic Modal Music</td><td>Balochi Traditional Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Islamic Modal Music</td><td>Ghazal</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Islamic Modal Music</td><td>Iranian Folk Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Islamic Modal Music</td><td>Kurdish Folk Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Islamic Modal Music</td><td>Moorish Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Islamic Modal Music</td><td>Pashto Folk Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Islamic Modal Music</td><td>Persian Classical Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Islamic Modal Music</td><td>Tajik Traditional Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Islamic Modal Music</td><td>Tajik Traditional Music</td><td>Shashmaqam</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Islamic Modal Music</td><td>Turkish Classical Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Islamic Modal Music</td><td>Turkish Sufi Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Islamic Modal Music</td><td>Turkmen Traditional Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Islamic Modal Music</td><td>Uyghur Traditional Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Islamic Modal Music</td><td>Uzbek Traditional Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Islamic Modal Music</td><td>Uzbek Traditional Music</td><td>Shashmaqam</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Jewish Music</td><td></td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Jewish Music</td><td>Cantillation</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Jewish Music</td><td>Klezmer</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Jewish Music</td><td>Mizrahi Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Jewish Music</td><td>Sephardic Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Jewish Music</td><td>Sephardic Music</td><td>Maftirim</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Philippine Music</td><td></td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Philippine Music</td><td>Balitaw</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Philippine Music</td><td>Kundiman</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Philippine Music</td><td>Manila Sound</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Philippine Music</td><td>Pinoy Folk Rock</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Polynesian Music</td><td></td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Polynesian Music</td><td>Fijian Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Polynesian Music</td><td>Hawaiian Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Polynesian Music</td><td>Hawaiian Music</td><td>Slack-Key Guitar</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Polynesian Music</td><td>Himene tarava</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Polynesian Music</td><td>Te Pūoro Māori</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Polynesian Music</td><td>Samoan Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Polyphonic Chant</td><td></td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Polyphonic Chant</td><td>Ars nova</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Polyphonic Chant</td><td>Ars subtilior</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Polyphonic Chant</td><td>Cante alentejano</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Polyphonic Chant</td><td>Cantu a tenore</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Polyphonic Chant</td><td>Himene tarava</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Polyphonic Chant</td><td>Lab Polyphony</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Polyphonic Chant</td><td>Madrigal</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Polyphonic Chant</td><td>Sutartinės</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Polyphonic Chant</td><td>Tosk Polyphony</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Portuguese Music</td><td></td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Portuguese Music</td><td>Música de intervenção</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Portuguese Music</td><td>Pimba</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Portuguese Music</td><td>Portuguese Folk Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Portuguese Music</td><td>Portuguese Folk Music</td><td>Cante alentejano</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Portuguese Music</td><td>Portuguese Folk Music</td><td>Fado</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Portuguese Music</td><td>Portuguese Folk Music</td><td>Fado</td><td>Fado de Coimbra</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Portuguese Music</td><td>Portuguese Folk Music</td><td>Trás-os-Montes Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Prehistoric Music</td><td></td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Romanian Music</td><td></td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Romanian Music</td><td>Muzică de mahala</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Romanian Music</td><td>Romanian Etno Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Romanian Music</td><td>Romanian Folk Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Romanian Music</td><td>Romanian Folk Music</td><td>Bocet</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Romanian Music</td><td>Romanian Folk Music</td><td>Colinde</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Romanian Music</td><td>Romanian Folk Music</td><td>Doina</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Romanian Music</td><td>Romanian Folk Music</td><td>Lăutărească</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Romanian Music</td><td>Romanţe</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Russian Music</td><td></td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Russian Music</td><td>Bard Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Russian Music</td><td>Russian Chanson</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Russian Music</td><td>Russian Folk Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Russian Music</td><td>Russian Orthodox Liturgical Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Russian Music</td><td>Russian Orthodox Liturgical Music</td><td>Znamenny Chant</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Russian Music</td><td>Russian Romance</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Seychelles & Mascarene Islands Music</td><td></td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Seychelles & Mascarene Islands Music</td><td>Maloya</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Seychelles & Mascarene Islands Music</td><td>Maloya</td><td>Traditional Maloya</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Seychelles & Mascarene Islands Music</td><td>Moutya</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Seychelles & Mascarene Islands Music</td><td>Santé engagé</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Seychelles & Mascarene Islands Music</td><td>Séga</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Seychelles & Mascarene Islands Music</td><td>Séga</td><td>Seggae</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Seychelles & Mascarene Islands Music</td><td>Séga</td><td>Traditional Séga</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>South Asian Music</td><td></td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>South Asian Music</td><td>Baila</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>South Asian Music</td><td>Bhangra</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>South Asian Music</td><td>Filmi</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>South Asian Music</td><td>Garba</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>South Asian Music</td><td>Indian Pop</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>South Asian Music</td><td>Pop Ghazal</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>South Asian Music</td><td>Rigsar</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>South Asian Music</td><td>South Asian Classical Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>South Asian Music</td><td>South Asian Classical Music</td><td>Carnatic Classical Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>South Asian Music</td><td>South Asian Classical Music</td><td>Hindustani Classical Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>South Asian Music</td><td>South Asian Classical Music</td><td>Hindustani Classical Music</td><td>Dhrupad</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>South Asian Music</td><td>South Asian Classical Music</td><td>Hindustani Classical Music</td><td>Khyal</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>South Asian Music</td><td>South Asian Classical Music</td><td>Hindustani Classical Music</td><td>Qawwali</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>South Asian Music</td><td>South Asian Classical Music</td><td>Hindustani Classical Music</td><td>Qawwali</td><td>Kafi</td><td></td></tr>
<tr><td>Regional Music</td><td>South Asian Music</td><td>South Asian Classical Music</td><td>Hindustani Classical Music</td><td>Thumri</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>South Asian Music</td><td>South Asian Folk Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>South Asian Music</td><td>South Asian Folk Music</td><td>Bengali Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>South Asian Music</td><td>South Asian Folk Music</td><td>Kirtan</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>South Asian Music</td><td>South Asian Folk Music</td><td>Malayali Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>South Asian Music</td><td>South Asian Folk Music</td><td>Newa Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>South Asian Music</td><td>South Asian Folk Music</td><td>Pashto Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>South Asian Music</td><td>South Asian Folk Music</td><td>Punjabi Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>South Asian Music</td><td>South Asian Folk Music</td><td>Sri Lankan Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>South Asian Music</td><td>South Asian Folk Music</td><td>Sri Lankan Folk Music</td><td>Sarala Gee</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>South Asian Music</td><td>South Asian Folk Music</td><td>Urumi melam</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td></td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Batak Pop</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Cambodian Pop</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Dangdut</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Dangdut</td><td>Dangdut koplo</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Hmong Pop</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Jaipongan</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Jaipongan</td><td>Kliningan</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Kecak</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Luk Krung</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Luk Krung</td><td>Lilat</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Luk Thung</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Mono</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Phleng phuea chiwit</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Pop Sunda</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Qasidah Modern</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Shadow Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Southeast Asian Classical Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Southeast Asian Classical Music</td><td>Burmese Classical Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Southeast Asian Classical Music</td><td>Caklempong</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Southeast Asian Classical Music</td><td>Gamelan</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Southeast Asian Classical Music</td><td>Gamelan</td><td>Gamelan angklung</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Southeast Asian Classical Music</td><td>Gamelan</td><td>Gamelan Degung</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Southeast Asian Classical Music</td><td>Gamelan</td><td>Gamelan gong gede</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Southeast Asian Classical Music</td><td>Gamelan</td><td>Gamelan Gong Kebyar</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Southeast Asian Classical Music</td><td>Gamelan</td><td>Gamelan Jawa</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Southeast Asian Classical Music</td><td>Gamelan</td><td>Gamelan semar pegulingan</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Southeast Asian Classical Music</td><td>Kecapi Suling</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Southeast Asian Classical Music</td><td>Kulintang</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Southeast Asian Classical Music</td><td>Mahori</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Southeast Asian Classical Music</td><td>Pinpeat</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Southeast Asian Classical Music</td><td>Saluang Klasik</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Southeast Asian Classical Music</td><td>Tembang Cianjuran</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Southeast Asian Classical Music</td><td>Thai Classical Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Southeast Asian Classical Music</td><td>Thai Classical Music</td><td>Piphat</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Southeast Asian Folk Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Southeast Asian Folk Music</td><td>Gondang</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Southeast Asian Folk Music</td><td>Hmong Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Southeast Asian Folk Music</td><td>Keroncong</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Southeast Asian Folk Music</td><td>Kuda Lumping</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Southeast Asian Folk Music</td><td>Malay Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Southeast Asian Folk Music</td><td>Molam</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Southeast Asian Music</td><td>Stereo</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Tibetan Traditional Music</td><td></td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Tibetan Traditional Music</td><td>Zhabdro Gorgom</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td></td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>African Folk Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Ambasse bey</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Apala</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Batuque</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Dagomba Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Gnawa</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Kabye Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Kilapanga</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Mbenga-Mbuti Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Moutya</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Ngoma</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Ngoma</td><td>Unyago</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Semba</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Southern African Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Southern African Folk Music</td><td>Khoisan Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Southern African Folk Music</td><td>Nguni Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Southern African Folk Music</td><td>Shona Mbira Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Southern African Folk Music</td><td>Sotho-Tswana Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Tchinkoumé</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Traditional Maloya</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Traditional Séga</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>African Folk Music</td><td>Zinli</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>American Folk Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Appalachian Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Barbershop</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Boogie Woogie</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Cajun</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Cajun</td><td>Traditional Cajun</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Cajun</td><td>Zydeco</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Country Blues</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Country Blues</td><td>Acoustic Texas Blues</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Country Blues</td><td>Delta Blues</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Country Blues</td><td>Piedmont Blues</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Gospel</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Gospel</td><td>Country Gospel</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Gospel</td><td>Country Gospel</td><td>Bluegrass Gospel</td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Gospel</td><td>Southern Gospel</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Gospel</td><td>Traditional Black Gospel</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Jug Band</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Ragtime</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Ragtime</td><td>Cakewalk</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Ragtime</td><td>Coon Song</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Ragtime</td><td>Novelty Piano</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Ragtime</td><td>Stride</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Sacred Harp Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Spirituals</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Talking Blues</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Traditional Country</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Traditional Country</td><td>Bluegrass</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Traditional Country</td><td>Bluegrass</td><td>Bluegrass Gospel</td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Traditional Country</td><td>Close Harmony</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Traditional Country</td><td>Country Boogie</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Traditional Country</td><td>Country Gospel</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Traditional Country</td><td>Country Gospel</td><td>Bluegrass Gospel</td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Traditional Country</td><td>Country Yodeling</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Traditional Country</td><td>Cowboy</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>American Folk Music</td><td>Traditional Country</td><td>Old-Time</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Arabic Folk Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Arabic Folk Music</td><td>Arabic Bellydance Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Australian Folk Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Austronesian Traditional Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Austronesian Traditional Music</td><td>Gondang</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Austronesian Traditional Music</td><td>Indigenous Taiwanese Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Austronesian Traditional Music</td><td>Pakacaping Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Balitaw</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Canadian Folk Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Canadian Folk Music</td><td>Canadian Maritime Folk</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Canadian Folk Music</td><td>Canadian Maritime Folk</td><td>Cape Breton Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Canadian Folk Music</td><td>Canadian Maritime Folk</td><td>Cape Breton Folk Music</td><td>Cape Breton Fiddling</td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Canadian Folk Music</td><td>French-Canadian Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Canadian Folk Music</td><td>Newfoundland Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Caribbean Folk Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Caribbean Folk Music</td><td>Fungi</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Caribbean Folk Music</td><td>Garifuna Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Caribbean Folk Music</td><td>Junkanoo</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Caribbean Folk Music</td><td>Méringue</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Caribbean Folk Music</td><td>Ripsaw</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Caribbean Folk Music</td><td>Trinidadian Cariso</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Caribbean Folk Music</td><td>Tumba</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Caribbean Folk Music</td><td>Virgin Islander Cariso</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Caucasian Folk Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Caucasian Folk Music</td><td>Abkhazian Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Caucasian Folk Music</td><td>Avar Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Caucasian Folk Music</td><td>Chechen Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Caucasian Folk Music</td><td>Circassian Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Caucasian Folk Music</td><td>Georgian Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Caucasian Folk Music</td><td>Ossetian Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Chuvash Folk Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Ainu Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Ainu Folk Music</td><td>Upopo</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Ainu Folk Music</td><td>Yukar</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Chinese Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Chinese Folk Music</td><td>Guoyue</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Chinese Folk Music</td><td>Han Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Chinese Folk Music</td><td>Jiangnan Sizhu</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Indigenous Taiwanese Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Japanese Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Japanese Folk Music</td><td>Amami Shimauta</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Japanese Folk Music</td><td>Heikyoku</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Japanese Folk Music</td><td>Kagura</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Japanese Folk Music</td><td>Kouta</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Japanese Folk Music</td><td>Min'yō</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Japanese Folk Music</td><td>Ondō</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Japanese Folk Music</td><td>Rōkyoku</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Japanese Folk Music</td><td>Taiko</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Japanese Folk Music</td><td>Tsugaru Shamisen</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Korean Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Korean Folk Music</td><td>Sanjo</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>East Asian Folk Music</td><td>Korean Folk Music</td><td>Sinawi</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Alpine Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Alpine Folk Music</td><td>Naturjodel</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td>Albanian Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td>Albanian Folk Music</td><td>Lab Polyphony</td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td>Albanian Folk Music</td><td>Musika popullore</td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td>Albanian Folk Music</td><td>Tosk Polyphony</td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td>Balkan Brass Band</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td>Bulgarian Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td>Csango Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td>Gagauz Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td>Greek Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td>Greek Folk Music</td><td>Dimotika</td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td>Greek Folk Music</td><td>Kritika</td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td>Greek Folk Music</td><td>Nisiotika Aigaiou</td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td>Greek Folk Music</td><td>Nisiotika Ioniou</td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td>Greek Folk Music</td><td>Rembetika</td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td>Lăutărească</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td>Macedonian Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td>Macedonian Folk Music</td><td>Čalgija</td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td>Sevdalinka</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td>Starogradska muzika</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Balkan Folk Music</td><td>Starogradska muzika</td><td>Čalgija</td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Basque Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Catalan Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Catalan Folk Music</td><td>Sardana</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td>Asturian Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td>Breton Celtic Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td>Breton Celtic Folk Music</td><td>Bagad</td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td>Cape Breton Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td>Cape Breton Folk Music</td><td>Cape Breton Fiddling</td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td>Cornish Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td>Galician Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td>Irish Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td>Irish Folk Music</td><td>Country & Irish</td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td>Irish Folk Music</td><td>Sean-nós</td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td>Manx Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td>Newfoundland Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td>Scottish Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td>Scottish Folk Music</td><td>Gaelic Psalm</td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td>Scottish Folk Music</td><td>Pibroch</td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td>Scottish Folk Music</td><td>Pipe Band</td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td>Scottish Folk Music</td><td>Shetland and Orkney Islands Folk Music</td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td>Scottish Folk Music</td><td>Waulking Song</td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td>Trás-os-Montes Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Celtic Folk Music</td><td>Welsh Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Dutch Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>English Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>English Folk Music</td><td>Cornish Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>English Folk Music</td><td>Northumbrian Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>English Folk Music</td><td>Scrumpy and Western</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Estonian Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Fanfare</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Fanfare</td><td>Brass Band</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Flemish Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>French Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>French Folk Music</td><td>Alsatian Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>French Folk Music</td><td>Breton Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>French Folk Music</td><td>Breton Folk Music</td><td>Breton Celtic Folk Music</td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>French Folk Music</td><td>Breton Folk Music</td><td>Breton Celtic Folk Music</td><td>Bagad</td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>French Folk Music</td><td>Corsican Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>French Folk Music</td><td>Musette</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>French Folk Music</td><td>Occitan Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>French Folk Music</td><td>Occitan Folk Music</td><td>Auvergnat Folk Music</td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>French Folk Music</td><td>Occitan Folk Music</td><td>Gascon Folk Music</td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>German Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Għana</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Hungarian Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Italian Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Italian Folk Music</td><td>Cantu a tenore</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Italian Folk Music</td><td>Canzone napoletana</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Latvian Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Lithuanian Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Lithuanian Folk Music</td><td>Sutartinės</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Mari Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Neo-Medieval Folk</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Nordic Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Nordic Folk Music</td><td>Danish Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Nordic Folk Music</td><td>Faroese Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Nordic Folk Music</td><td>Finnish Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Nordic Folk Music</td><td>Icelandic Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Nordic Folk Music</td><td>Joik</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Nordic Folk Music</td><td>Nordic Old Time Dance Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Nordic Folk Music</td><td>Nordic Old Time Dance Music</td><td>Humppa</td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Nordic Folk Music</td><td>Norwegian Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Nordic Folk Music</td><td>Runolaulu</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Nordic Folk Music</td><td>Swedish Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Nordic Folk Music</td><td>Swedish Folk Music</td><td>Visor</td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Polka</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Portuguese Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Portuguese Folk Music</td><td>Cante alentejano</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Portuguese Folk Music</td><td>Fado</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Portuguese Folk Music</td><td>Fado</td><td>Fado de Coimbra</td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Portuguese Folk Music</td><td>Trás-os-Montes Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Romanian Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Romanian Folk Music</td><td>Bocet</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Romanian Folk Music</td><td>Colinde</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Romanian Folk Music</td><td>Doina</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Romanian Folk Music</td><td>Lăutărească</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td>Belarusian Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td>Bulgarian Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td>Croatian Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td>Croatian Folk Music</td><td>Klapa</td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td>Czech Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td>Goral Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td>Goral Music</td><td>Polish Carpathian Folk Music</td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td>Macedonian Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td>Macedonian Folk Music</td><td>Čalgija</td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td>Moravian Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td>Polish Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td>Polish Folk Music</td><td>Polish Carpathian Folk Music</td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td>Russian Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td>Sevdalinka</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td>Slovak Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td>Slovenian Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td>Slovenian Folk Music</td><td>Narodno zabavna glasba</td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td>Starogradska muzika</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td>Starogradska muzika</td><td>Čalgija</td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td>Ukrainian Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Slavic Folk Music</td><td>Ukrainian Folk Music</td><td>Duma</td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Spanish Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Spanish Folk Music</td><td>Andalusian Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Spanish Folk Music</td><td>Andalusian Folk Music</td><td>Saeta</td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Spanish Folk Music</td><td>Aragonese Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Spanish Folk Music</td><td>Asturian Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Spanish Folk Music</td><td>Canarian Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Spanish Folk Music</td><td>Chotis Madrileño</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Spanish Folk Music</td><td>Copla</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Spanish Folk Music</td><td>Cuplé</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Spanish Folk Music</td><td>Flamenco</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Spanish Folk Music</td><td>Flamenco</td><td>Flamenco nuevo</td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Spanish Folk Music</td><td>Flamenco</td><td>Flamenco nuevo</td><td>Flamenco Jazz</td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Spanish Folk Music</td><td>Flamenco</td><td>Rumba Flamenca</td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Spanish Folk Music</td><td>Galician Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Spanish Folk Music</td><td>Pasodoble</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Spanish Folk Music</td><td>Sevillanas</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>European Folk Music</td><td>Warsaw City Folk</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Hill Tribe Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Hill Tribe Music</td><td>Hmong Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Indigenous Australian Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Indigenous Australian Music</td><td>Djanba</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Kundiman</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Malagasy Folk Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Mexican Folk Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Mexican Folk Music</td><td>Canto cardenche</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Mexican Folk Music</td><td>Ranchera</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Mexican Folk Music</td><td>Son Calentano</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Mexican Folk Music</td><td>Son Huasteco</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Mexican Folk Music</td><td>Son Istmeño</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Mexican Folk Music</td><td>Son Jarocho</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Mexican Folk Music</td><td>Trova yucateca</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Native American Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Native American Music</td><td>Harawi</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Native American Music</td><td>Huayno</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Native American Music</td><td>Inuit Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Native American Music</td><td>Inuit Music</td><td>Inuit Vocal Games</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Native American Music</td><td>Powwow Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Papuan Traditional Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Rapai dabõih</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Romani Folk Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Samoyedic Traditional Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Sephardic Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Sephardic Music</td><td>Maftirim</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Andean Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Andean Folk Music</td><td>Chacarera</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Andean Folk Music</td><td>Harawi</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Andean Folk Music</td><td>Huayno</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Andean Folk Music</td><td>Muliza</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Bambuco</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Canto a lo poeta</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Cueca</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Joropo</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Malagueña venezolana</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Mapuche Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Milonga</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Música criolla peruana</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Música criolla peruana</td><td>Marinera</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Música criolla peruana</td><td>Tondero</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Música criolla peruana</td><td>Vals criollo</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Sertanejo de raiz</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Sertanejo de raiz</td><td>Moda de viola</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Tamborito</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Taquirari</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Vallenato</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>South American Folk Music</td><td>Zamba</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>South Asian Folk Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>South Asian Folk Music</td><td>Bengali Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>South Asian Folk Music</td><td>Kirtan</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>South Asian Folk Music</td><td>Malayali Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>South Asian Folk Music</td><td>Newa Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>South Asian Folk Music</td><td>Pashto Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>South Asian Folk Music</td><td>Punjabi Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>South Asian Folk Music</td><td>Sri Lankan Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>South Asian Folk Music</td><td>Sri Lankan Folk Music</td><td>Sarala Gee</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>South Asian Folk Music</td><td>Urumi melam</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Southeast Asian Folk Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Southeast Asian Folk Music</td><td>Gondang</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Southeast Asian Folk Music</td><td>Hmong Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Southeast Asian Folk Music</td><td>Keroncong</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Southeast Asian Folk Music</td><td>Kuda Lumping</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Southeast Asian Folk Music</td><td>Malay Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Southeast Asian Folk Music</td><td>Molam</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Tatar Folk Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Throat Singing</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Throat Singing</td><td>Mongolian Throat Singing</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Throat Singing</td><td>Tuvan Throat Singing</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Tonada</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Vietnamese Folk Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>West Asian Folk Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>West Asian Folk Music</td><td>Armenian Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>West Asian Folk Music</td><td>Armenian Folk Music</td><td>Kef Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>West Asian Folk Music</td><td>Iranian Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>West Asian Folk Music</td><td>Israeli Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>West Asian Folk Music</td><td>Kurdish Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>West Asian Folk Music</td><td>Turkish Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>West Asian Folk Music</td><td>Turkish Folk Music</td><td>Karadeniz Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>West Asian Folk Music</td><td>Turkish Folk Music</td><td>Uzun Hava</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>West Asian Folk Music</td><td>Turkish Folk Music</td><td>Zeybek</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Work Songs</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Work Songs</td><td>Field Hollers</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Work Songs</td><td>Sea Shanties</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Work Songs</td><td>Waulking Song</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Yodeling</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Yodeling</td><td>Country Yodeling</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Traditional Folk Music</td><td>Yodeling</td><td>Naturjodel</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Turkic-Mongolic Traditional Music</td><td></td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Turkic-Mongolic Traditional Music</td><td>Altai Traditional Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Turkic-Mongolic Traditional Music</td><td>Bashkir Folk Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Turkic-Mongolic Traditional Music</td><td>Buryat Folk Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Turkic-Mongolic Traditional Music</td><td>Kalmyk Traditional Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Turkic-Mongolic Traditional Music</td><td>Karakalpak Traditional Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Turkic-Mongolic Traditional Music</td><td>Kazakh Traditional Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Turkic-Mongolic Traditional Music</td><td>Khakas Traditional Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Turkic-Mongolic Traditional Music</td><td>Kyrgyz Traditional Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Turkic-Mongolic Traditional Music</td><td>Mongolian Traditional Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Turkic-Mongolic Traditional Music</td><td>Mongolian Traditional Music</td><td>Bogino Duu</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Turkic-Mongolic Traditional Music</td><td>Mongolian Traditional Music</td><td>Mongolian Long Song</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Turkic-Mongolic Traditional Music</td><td>Mongolian Traditional Music</td><td>Mongolian Throat Singing</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Turkic-Mongolic Traditional Music</td><td>Sakha Traditional Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>Turkic-Mongolic Traditional Music</td><td>Tuvan Throat Singing</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>West Asian Music</td><td></td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>West Asian Music</td><td>Al-Maqam Al-Iraqi</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>West Asian Music</td><td>Armenian Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>West Asian Music</td><td>Armenian Music</td><td>Armenian Church Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>West Asian Music</td><td>Armenian Music</td><td>Armenian Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>West Asian Music</td><td>Armenian Music</td><td>Armenian Folk Music</td><td>Kef Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>West Asian Music</td><td>Armenian Music</td><td>Rabiz</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>West Asian Music</td><td>Azerbaijani Traditional Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>West Asian Music</td><td>Dabke</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>West Asian Music</td><td>Iranian Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>West Asian Music</td><td>Iranian Music</td><td>Iranian Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>West Asian Music</td><td>Iranian Music</td><td>Maddahi</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>West Asian Music</td><td>Iranian Music</td><td>Persian Classical Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>West Asian Music</td><td>Iranian Music</td><td>Persian Pop</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>West Asian Music</td><td>Maftirim</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>West Asian Music</td><td>Mesopotamian Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>West Asian Music</td><td>Mizrahi Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>West Asian Music</td><td>Turkish Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>West Asian Music</td><td>Turkish Music</td><td>Anatolian Rock</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>West Asian Music</td><td>Turkish Music</td><td>Arabesque</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>West Asian Music</td><td>Turkish Music</td><td>Fantezi</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>West Asian Music</td><td>Turkish Music</td><td>Kanto</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>West Asian Music</td><td>Turkish Music</td><td>Ottoman Military Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>West Asian Music</td><td>Turkish Music</td><td>Özgün Müzik</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>West Asian Music</td><td>Turkish Music</td><td>Turkish Classical Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>West Asian Music</td><td>Turkish Music</td><td>Turkish Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>West Asian Music</td><td>Turkish Music</td><td>Turkish Folk Music</td><td>Karadeniz Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>West Asian Music</td><td>Turkish Music</td><td>Turkish Folk Music</td><td>Uzun Hava</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>West Asian Music</td><td>Turkish Music</td><td>Turkish Folk Music</td><td>Zeybek</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>West Asian Music</td><td>Turkish Music</td><td>Turkish Pop</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>West Asian Music</td><td>Turkish Music</td><td>Turkish Sufi Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>West Asian Music</td><td>West Asian Folk Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>West Asian Music</td><td>West Asian Folk Music</td><td>Armenian Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>West Asian Music</td><td>West Asian Folk Music</td><td>Armenian Folk Music</td><td>Kef Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>West Asian Music</td><td>West Asian Folk Music</td><td>Iranian Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>West Asian Music</td><td>West Asian Folk Music</td><td>Israeli Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>West Asian Music</td><td>West Asian Folk Music</td><td>Kurdish Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>West Asian Music</td><td>West Asian Folk Music</td><td>Turkish Folk Music</td><td></td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>West Asian Music</td><td>West Asian Folk Music</td><td>Turkish Folk Music</td><td>Karadeniz Folk Music</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>West Asian Music</td><td>West Asian Folk Music</td><td>Turkish Folk Music</td><td>Uzun Hava</td><td></td><td></td></tr>
<tr><td>Regional Music</td><td>West Asian Music</td><td>West Asian Folk Music</td><td>Turkish Folk Music</td><td>Zeybek</td><td></td><td></td></tr>
</table>
</details>

### Rock

<details><summary>click to open/close</summary>
<table>
<tr><td>Rock</td><td></td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Acoustic Rock</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Afro-Rock</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Alternative Rock</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Alternative Rock</td><td>Alternative Dance</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Alternative Rock</td><td>Alternative Dance</td><td>Grebo</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Alternative Rock</td><td>Baggy / Madchester</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Alternative Rock</td><td>Britpop</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Alternative Rock</td><td>Dream Pop</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Alternative Rock</td><td>Emo-Pop</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Alternative Rock</td><td>Grunge</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Alternative Rock</td><td>Indie Rock</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Alternative Rock</td><td>Indie Rock</td><td>C86</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Alternative Rock</td><td>Indie Rock</td><td>Dunedin Sound</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Alternative Rock</td><td>Indie Rock</td><td>Hamburger Schule</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Alternative Rock</td><td>Indie Rock</td><td>Lo-Fi Indie</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Alternative Rock</td><td>Indie Rock</td><td>Midwest Emo</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Alternative Rock</td><td>Indie Rock</td><td>Post-Punk Revival</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Alternative Rock</td><td>Indie Rock</td><td>Slowcore</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Alternative Rock</td><td>Jangle Pop</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Alternative Rock</td><td>Noise Pop</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Alternative Rock</td><td>Paisley Underground</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Alternative Rock</td><td>Post-Grunge</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Alternative Rock</td><td>Shoegaze</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Andean Rock</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>AOR</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Art Punk</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Art Rock</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Blues Rock</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Blues Rock</td><td>Boogie Rock</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>British Rhythm & Blues</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Christian Rock</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Comedy Rock</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Country Rock</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Country Rock</td><td>Cowpunk</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Dark Cabaret</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Deutschrock</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Emo</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Emo</td><td>Emocore</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Emo</td><td>Emo-Pop</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Emo</td><td>Midwest Emo</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Emo</td><td>Screamo</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Emo</td><td>Screamo</td><td>Emoviolence</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Experimental Rock</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Experimental Rock</td><td>Avant-Prog</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Experimental Rock</td><td>Avant-Prog</td><td>Rock in Opposition</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Experimental Rock</td><td>Avant-Prog</td><td>Zeuhl</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Experimental Rock</td><td>Brutal Prog</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Experimental Rock</td><td>Krautrock</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Experimental Rock</td><td>No Wave</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Folk Rock</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Folk Rock</td><td>Alpenrock</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Folk Rock</td><td>Anatolian Rock</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Folk Rock</td><td>Andalusian Rock</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Folk Rock</td><td>British Folk Rock</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Folk Rock</td><td>Celtic Rock</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Folk Rock</td><td>Medieval Rock</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Folk Rock</td><td>Nordic Folk Rock</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Folk Rock</td><td>Phleng phuea chiwit</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Folk Rock</td><td>Pinoy Folk Rock</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Funk Rock</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Funk Rock</td><td>Funk Metal</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Garage Rock</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Garage Rock</td><td>Freakbeat</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Garage Rock</td><td>Garage Punk</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Garage Rock</td><td>Garage Rock Revival</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Glam Rock</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Hard Rock</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Hard Rock</td><td>Glam Metal</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Hard Rock</td><td>Heavy Psych</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Hard Rock</td><td>Stoner Rock</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Heartland Rock</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Industrial Rock</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Jam Band</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Jazz-Rock</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Latin Rock</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Math Rock</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Alternative Metal</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Alternative Metal</td><td>Funk Metal</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Alternative Metal</td><td>Nu Metal</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Alternative Metal</td><td>Rap Metal</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Avant-Garde Metal</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Black Metal</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Black Metal</td><td>Atmospheric Black Metal</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Black Metal</td><td>Atmospheric Black Metal</td><td>Blackgaze</td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Black Metal</td><td>Black 'n' Roll</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Black Metal</td><td>Depressive Black Metal</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Black Metal</td><td>Melodic Black Metal</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Black Metal</td><td>Pagan Black Metal</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Black Metal</td><td>Symphonic Black Metal</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Black Metal</td><td>War Metal</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Death Metal</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Death Metal</td><td>Brutal Death Metal</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Death Metal</td><td>Brutal Death Metal</td><td>Slam Death Metal</td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Death Metal</td><td>Death 'n' Roll</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Death Metal</td><td>Deathgrind</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Death Metal</td><td>Melodic Death Metal</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Death Metal</td><td>Technical Death Metal</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Doom Metal</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Doom Metal</td><td>Death Doom Metal</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Doom Metal</td><td>Funeral Doom Metal</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Doom Metal</td><td>Traditional Doom Metal</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Drone Metal</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Folk Metal</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Folk Metal</td><td>Celtic Metal</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Folk Metal</td><td>Medieval Folk Metal</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Gothic Metal</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Grindcore</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Grindcore</td><td>Cybergrind</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Grindcore</td><td>Deathgrind</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Grindcore</td><td>Goregrind</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Grindcore</td><td>Goregrind</td><td>Gorenoise</td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Groove Metal</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Heavy Metal</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Heavy Metal</td><td>New Wave of British Heavy Metal</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Industrial Metal</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Industrial Metal</td><td>Cyber Metal</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Industrial Metal</td><td>Neue Deutsche Härte</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Melodic Metalcore</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Melodic Metalcore</td><td>Nintendocore</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Melodic Metalcore</td><td>Trancecore</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Metalcore</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Metalcore</td><td>Deathcore</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Metalcore</td><td>Mathcore</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Neoclassical Metal</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Post-Metal</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Post-Metal</td><td>Atmospheric Sludge Metal</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Power Metal</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Progressive Metal</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Progressive Metal</td><td>Djent</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Sludge Metal</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Sludge Metal</td><td>Atmospheric Sludge Metal</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Speed Metal</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Stoner Metal</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Symphonic Metal</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Thrash Metal</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Thrash Metal</td><td>Crossover Thrash</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Thrash Metal</td><td>Technical Thrash Metal</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Trance Metal</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Metal</td><td>Viking Metal</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Mod</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Mod</td><td>Mod Revival</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>New Wave</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>New Wave</td><td>Neue Deutsche Welle</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>New Wave</td><td>New Romantic</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Noise Rock</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Pop Rock</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Pop Rock</td><td>Beat Music</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Pop Rock</td><td>Beat Music</td><td>Freakbeat</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Pop Rock</td><td>Beat Music</td><td>Group Sounds</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Pop Rock</td><td>Beat Music</td><td>Jovem Guarda</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Pop Rock</td><td>Beat Music</td><td>Merseybeat</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Pop Rock</td><td>Beat Music</td><td>Nederbeat</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Pop Rock</td><td>Britpop</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Pop Rock</td><td>Jangle Pop</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Pop Rock</td><td>Manila Sound</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Pop Rock</td><td>Piano Rock</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Pop Rock</td><td>Power Pop</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Pop Rock</td><td>Soft Rock</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Pop Rock</td><td>Stereo</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Pop Rock</td><td>Vocal Surf</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Pop Rock</td><td>Yacht Rock</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Post-Hardcore</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Post-Hardcore</td><td>Emocore</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Post-Hardcore</td><td>Screamo</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Post-Hardcore</td><td>Screamo</td><td>Emoviolence</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Post-Punk</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Post-Punk</td><td>Coldwave</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Post-Punk</td><td>Dance-Punk</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Post-Punk</td><td>Gothic Rock</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Post-Punk</td><td>Gothic Rock</td><td>Deathrock</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Post-Punk</td><td>No Wave</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Post-Punk</td><td>Post-Punk Revival</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Post-Rock</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Progressive Rock</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Progressive Rock</td><td>Andalusian Rock</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Progressive Rock</td><td>Avant-Prog</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Progressive Rock</td><td>Avant-Prog</td><td>Rock in Opposition</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Progressive Rock</td><td>Avant-Prog</td><td>Zeuhl</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Progressive Rock</td><td>Brutal Prog</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Progressive Rock</td><td>Canterbury Scene</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Progressive Rock</td><td>Krautrock</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Progressive Rock</td><td>Neo-Prog</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Progressive Rock</td><td>Symphonic Prog</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Proto-Punk</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Psychedelic Rock</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Psychedelic Rock</td><td>Acid Rock</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Psychedelic Rock</td><td>Heavy Psych</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Psychedelic Rock</td><td>Raga Rock</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Psychedelic Rock</td><td>Space Rock</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Pub Rock</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Anarcho-Punk</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Cowpunk</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Deathrock</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Deutschpunk</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Folk Punk</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Folk Punk</td><td>Celtic Punk</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Folk Punk</td><td>Gypsy Punk</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Garage Punk</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Glam Punk</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Hardcore Punk</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Hardcore Punk</td><td>Beatdown Hardcore</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Hardcore Punk</td><td>Crossover Thrash</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Hardcore Punk</td><td>Crust Punk</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Hardcore Punk</td><td>Crust Punk</td><td>D-Beat</td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Hardcore Punk</td><td>Grindcore</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Hardcore Punk</td><td>Grindcore</td><td>Cybergrind</td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Hardcore Punk</td><td>Grindcore</td><td>Deathgrind</td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Hardcore Punk</td><td>Grindcore</td><td>Goregrind</td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Hardcore Punk</td><td>Grindcore</td><td>Goregrind</td><td>Gorenoise</td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Hardcore Punk</td><td>Japanese Hardcore</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Hardcore Punk</td><td>Melodic Hardcore</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Hardcore Punk</td><td>Metalcore</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Hardcore Punk</td><td>Metalcore</td><td>Deathcore</td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Hardcore Punk</td><td>Metalcore</td><td>Mathcore</td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Hardcore Punk</td><td>New York Hardcore</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Hardcore Punk</td><td>Noisecore</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Hardcore Punk</td><td>Powerviolence</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Hardcore Punk</td><td>Powerviolence</td><td>Emoviolence</td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Hardcore Punk</td><td>Thrashcore</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Hardcore Punk</td><td>UK82</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Horror Punk</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Könsrock</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Oi!</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Pop Punk</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Pop Punk</td><td>Easycore</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Psychobilly</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Punk Blues</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Queercore</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Riot Grrrl</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Ska Punk</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Skate Punk</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Punk Rock</td><td>Surf Punk</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Rap Rock</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Rock & Roll</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Rock & Roll</td><td>Indorock</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Rock & Roll</td><td>Rockabilly</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Rock & Roll</td><td>Rockabilly</td><td>Psychobilly</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Rock Opera</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Rock urbano español</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Roots Rock</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Roots Rock</td><td>Swamp Rock</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Roots Rock</td><td>Tex-Mex</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Southern Rock</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Southern Rock</td><td>Red Dirt</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Sufi Rock</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Surf Music</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Surf Music</td><td>Surf Punk</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Surf Music</td><td>Surf Rock</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Surf Music</td><td>Surf Rock</td><td>Eleki</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Surf Music</td><td>Surf Rock</td><td>Rautalanka</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Surf Music</td><td>Surf Rock</td><td>Shadow Music</td><td></td><td></td></tr>
<tr><td>Rock</td><td>Surf Music</td><td>Vocal Surf</td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Symphonic Rock</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Zamrock</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Rock</td><td>Zolo</td><td></td><td></td><td></td><td></td></tr>
<tr><td></td></tr>
</table>
</details>

### Singer/Songwriter

<details><summary>click to open/close</summary>
<table>
   <tr>
      <td>Singer/Songwriter</td>
      <td></td>
      <td></td>
      <td></td>
   </tr>
   <tr>
      <td>Singer/Songwriter</td>
      <td>Bard Music</td>
      <td></td>
      <td></td>
   </tr>
   <tr>
      <td>Singer/Songwriter</td>
      <td>Canzone d'autore</td>
      <td></td>
      <td></td>
   </tr>
   <tr>
      <td>Singer/Songwriter</td>
      <td>Chanson à texte</td>
      <td></td>
      <td></td>
   </tr>
   <tr>
      <td>Singer/Songwriter</td>
      <td>Liedermacher</td>
      <td></td>
      <td></td>
   </tr>
   <tr>
      <td>Singer/Songwriter</td>
      <td>Música de intervenção</td>
      <td></td>
      <td></td>
   </tr>
   <tr>
      <td>Singer/Songwriter</td>
      <td>Nueva canción</td>
      <td></td>
      <td></td>
   </tr>
   <tr>
      <td>Singer/Songwriter</td>
      <td>Nueva canción</td>
      <td>Nueva canción española</td>
      <td></td>
   </tr>
   <tr>
      <td>Singer/Songwriter</td>
      <td>Nueva canción</td>
      <td>Nueva canción latinoamericana</td>
      <td></td>
   </tr>
   <tr>
      <td>Singer/Songwriter</td>
      <td>Nueva canción</td>
      <td>Nueva canción latinoamericana</td>
      <td>Nueva trova</td>
   </tr>
   <tr>
      <td>Singer/Songwriter</td>
      <td>Poezja spiewana</td>
      <td></td>
      <td></td>
   </tr>
</table>
</details>

### Ska

<details><summary>click to open/close</summary>
<table>
   <tr>
      <td>Ska</td>
      <td></td>
      <td></td>
   </tr>
   <tr>
      <td>Ska</td>
      <td>2 Tone</td>
      <td></td>
   </tr>
   <tr>
      <td>Ska</td>
      <td>Jamaican Ska</td>
      <td></td>
   </tr>
   <tr>
      <td>Ska</td>
      <td>Spouge</td>
      <td></td>
   </tr>
   <tr>
      <td>Ska</td>
      <td>Third Wave Ska</td>
      <td></td>
   </tr>
   <tr>
      <td>Ska</td>
      <td>Third Wave Ska</td>
      <td>Ska Punk</td>
   </tr>
</table>
</details>

### Spoken Word

<details><summary>click to open/close</summary>
<table>
   <tr>
      <td>Spoken Word</td>
      <td></td>
      <td></td>
   </tr>
   <tr>
      <td>Spoken Word</td>
      <td>Fairy Tales</td>
      <td></td>
   </tr>
   <tr>
      <td>Spoken Word</td>
      <td>Interview</td>
      <td></td>
   </tr>
   <tr>
      <td>Spoken Word</td>
      <td>Poetry</td>
      <td></td>
   </tr>
   <tr>
      <td>Spoken Word</td>
      <td>Poetry</td>
      <td>Beat Poetry</td>
   </tr>
   <tr>
      <td>Spoken Word</td>
      <td>Poetry</td>
      <td>Dub Poetry</td>
   </tr>
   <tr>
      <td>Spoken Word</td>
      <td>Poetry</td>
      <td>Jazz Poetry</td>
   </tr>
   <tr>
      <td>Spoken Word</td>
      <td>Poetry</td>
      <td>Nursery Rhymes</td>
   </tr>
   <tr>
      <td>Spoken Word</td>
      <td>Poetry</td>
      <td>Sound Poetry</td>
   </tr>
   <tr>
      <td>Spoken Word</td>
      <td>Radio Play</td>
      <td></td>
   </tr>
   <tr>
      <td>Spoken Word</td>
      <td>Speeches</td>
      <td></td>
   </tr>
</table>
</details>
