Note: This wiki page is a work in progress, so please don't take anything in it as gospel right now.

---

## References

Tildes ~music tags sorted by times used:
https://tildes.net/~music/wiki/topic_tags_2019_07_05

Every Noise at Once genre list:
http://everynoise.com/everynoise1d.cgi?scope=all

Ishkur's Guide to Electronic Music - version 3:
http://music.ishkur.com/

Up-to-date Psytrance styles guide:
http://psytranceguide.com/

Rate Your Music - Genres:
https://rateyourmusic.com/genres
https://tildes.net/~music/wiki/rym_genre_hierarchy

---
