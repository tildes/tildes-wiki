> A collection of resources for discovering new music

## YouTube

- [NPR Music/Tiny Desk Concert](https://www.youtube.com/user/nprmusic/videos)
- [KEXP](https://www.youtube.com/user/kexpradio/videos)
- [Jam in The Van](https://www.youtube.com/user/JamintheVan/videos)
- [OurVinyl Sessions](https://www.youtube.com/user/OurVinyl/videos)
- [Audiotree](https://www.youtube.com/user/Audiotreetv/videos)
- [Rohail Hyatt](https://www.youtube.com/user/cokestudio/videos)
- Coke Studio - [Pakistan](https://www.youtube.com/user/CokeStudioPk/videos), [India](https://www.youtube.com/user/cokestudioatmtv/videos)
- [Sofar Sounds](https://www.youtube.com/user/Sofarsounds/videos)
- [The Crypt Sessions](https://www.youtube.com/user/CryptSessions/videos)
- [Apartment Sessions](https://www.youtube.com/user/mcekul/videos)
- [deep cuts](https://www.youtube.com/channel/UCRYhCg0DHloE9gn-PAiAYNg/videos)
- [Indie88 Toronto](https://www.youtube.com/user/Indie88toronto)
- [KNKX Public Radio](https://www.youtube.com/channel/UCezH4cD1a1lzgjEJNcrn35w) (jazz/blues)
- [CandyRat Records](https://www.youtube.com/channel/UCMJecdKUslHToOEpeuRGwXg)
- [HI*Sessions](https://www.youtube.com/user/hisessionsshow/)
- [The Influences](https://www.youtube.com/channel/UCpiwAS1c21IltC1_6cT4W5Q)
- [WGBH Music](https://www.youtube.com/channel/UCyKaaVFqQEqdEu0OheOUwJA)
- [WXPN](https://www.youtube.com/channel/UCSDv-xyaQVzvepzYAD3yH3A)
- [HawaiiMusicSupply](https://www.youtube.com/channel/UCH6E4xDfxBqrkoKdAyVu2dg)
- [New Sounds](https://www.youtube.com/channel/UC-8reoPK4lLyGUZjANQKxuA)
- [La Blogothèque](https://www.youtube.com/user/LaBlogotheque)
- [Paste Magazine](https://www.youtube.com/channel/UC_PscE_7n8SiMspieMGMK_A)
- [KCRW](https://www.youtube.com/channel/UC54U5gpKkaa_BpXRVNip2Dw)

## Streaming Services

- [Soundcloud](https://www.soundcloud.com/)
- [Bandcamp](https://bandcamp.com/)
- [Spotify](https://open.spotify.com/)
- [Google Play Music](https://play.google.com/music)
- [YouTube Music](https://music.youtube.com/)
- [Apple Music](https://www.apple.com/apple-music/)
- [Tidal](https://tidal.com/)
- [Deezer](https://www.deezer.com/)


## Music Discovery Services and Databases

<table>
    <tbody>
        <tr>
            <th align="left">Name</th><th align="left">Description</th>
        </tr>
        <tr>
            <td><a href="https://rateyourmusic.com/">Rate Your Music</a>
                <ul>
                    <li><a href="https://rateyourmusic.com/customchart">Custom Charts</a></li>
                    <li><a href="https://rateyourmusic.com/lists/">User Lists</a></li>
                    <li><a href="https://rateyourmusic.com/list/TheScientist/rym-ultimate-box-set/">RYM Ultimate Box Set</a></li>
                </ul>
            </td>
            <td>RYM is a community-built music and film database where you can rate, review, catalog and discover new music and films as well as participate in contributing to the database itself. 
                    <br>Browse the charts to find releases by any criteria, such as genre, location, release type, and more.
            </td>
        </tr>
        <tr>
            <td><a href="https://hypem.com/">The Hype Machine</a></td>
            <td>Hype Machine indexes hundreds of music sites and collects their latest posts for easy streaming and discovery.</td>
        </tr>
        <tr>
            <td><a href="https://freemusicarchive.org/">Free Music Archive</a></td>
            <td> Free Music Archive is an interactive library of high-quality, legal audio downloads directed by WFMU.</td>
        </tr>
        <tr>
            <td><a href="https://boards.4channel.org/mu/catalog">/mu/</a>
                <ul>
                    <li><a href="https://4chanmusic.fandom.com/wiki/4chanmusic_Wiki">/mu/ wiki</a></li>
                    <li><a href="https://archive.rebeccablacktech.com/mu/">/mu/ Archive</a></li>
                </ul>    
                </td>
            <td>/mu/ is 4chan's music board
                <br>/mu/ wiki - wiki dedicated to all things relating to 4chan's /mu/ board and music-related information.
                <br>/mu/ archive - An archive of old threads from /mu/
            </td>
        </tr>
        <tr>
            <td><a href="https://www.allmusic.com/discover">AllMusic Discover</a></td>
            <td>AllMusic is a comprehensive and in-depth resource for finding out more about the albums, bands, musicians</td>
        </tr>
        <tr>
            <td><a href="https://www.indieshuffle.com/">Indie Shuffle</a></td>
            <td>Music discovery platform for indie rock, hip hop, electronic, and everything in between.</td>
        </tr>
        <tr>
            <td><a href="http://liveplasma.com/">Live Plasma</a></td>
            <td>Recommendation engine based on bands you like</td>
        </tr>
        <tr>
            <td><a href="https://www.last.fm/home">Last.fm</a></td>
            <td>Create your own profile, track what you listen to and get your own music charts, new music recommendations, and a big community of other music lovers. </td>
        </tr>
        <tr>
            <td><a href="https://www.discogs.com/search/">Discogs</a></td>
            <td>Discogs is a user-built database of music. </td>
        </tr>
        <tr>
            <td><a href="https://www.metal-archives.com/">Encyclopaedia Metallum: The Metal Archives</a></td>
            <td>Database and forum for metal music</td>
        </tr>
    </tbody>
</table>

## Blogs

- [The Siren's Sound](http://www.thesirenssound.com/)
- [BIRP](http://www.birp.fm/)
- [The Obelisk](http://theobelisk.net/obelisk/)
- [Musigh](http://musigh.com/)
- [This Song is Sick](http://thissongissick.com/)

## Music Journalism and Reviews
- [Pitchfork](https://pitchfork.com/)
- [theneedledrop - Anthony Fantano](https://www.youtube.com/theneedledrop)
- [npr music](https://www.npr.org/sections/music-reviews/)
- [Dead End Hip Hop](https://www.youtube.com/user/deadendhiphop)
- [BBC](https://www.bbc.co.uk/music/reviews/)
- [Sputnikmusic](https://www.sputnikmusic.com/)
