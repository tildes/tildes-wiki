### Introductory

[How to Read the Bible: A Guide to Scripture, Then and Now](https://smile.amazon.com/How-Read-Bible-Guide-Scripture-ebook/dp/B007108SQK/) by James L. Kugel

Kugel contrasts the way modern scholars understand these events with the way Christians and Jews have traditionally understood them. Their interpretations became what the Bible meant for centuries and centuries-until modern scholarship came along. The question that this book ultimately asks is: What now? As one reviewer wrote, Kugel's answer provides "a contemporary model of how to read Sacred Scripture amidst the oppositional pulls of modern scholarship and tradition."

[Introduction to the Hebrew Bible](https://smile.amazon.com/Introduction-Hebrew-Bible-John-Collins/dp/1506445985/) by John J. Collins

Collins proceeds through the canon of the Old Testament and the Apocrypha, judiciously presenting the current state of historical, archaeological, and literary understanding of the biblical text, and engaging the student in questions of significance and interpretation for the contemporary world. In order to enhance classroom use, Collins's major text has now been divided into four volumes, one for each major part of the Hebrew Bible.

[Who Wrote the Bible?](https://smile.amazon.com/Wrote-Bible-Richard-Elliott-Friedman/dp/0060630353/) by Richard Elliott Friedman

Who Wrote the Bible? is enlightening, riveting, an important contribution to religious literature, and as the Los Angeles Times aptly observed in its rave review, “There is no other book like this one.” Friedman, a Harvard trained Biblical scholar, concisely walks us through the history of Old Testament scholarship while arguing for his own theories on who wrote specific portions, when, what their motivations were, and how and by whom the book was compiled.

[The Composition of the Pentateuch: Renewing the Documentary Hypothesis](https://smile.amazon.com/Composition-Pentateuch-Documentary-Hypothesis-Reference/dp/0300152639/) by Joel S. Baden

Interweaving historical and methodological chapters with detailed textual case studies, Baden provides a critical introduction to the history of Pentateuchal scholarship, discussions on the most pressing issues in the current debate, and a practical model for the study of the biblical text.
