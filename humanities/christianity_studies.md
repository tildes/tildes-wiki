### Introductory

[An Introduction to the New Testament](https://smile.amazon.com/Introduction-New-Testament-Abridged-Reference/dp/0300173121/) by Raymond E. Brown

Augmenting Brown’s commentary on the New Testament itself are topics such as the Gospels’ relationship to one another; the form and function of ancient letters; Paul’s thought and life, along with his motivation, legacy, and theology; a reflection on the historical Jesus; and a survey of relevant Jewish and Christian writings. The biblical writings themselves remain the focus, but there are also chapters dealing with the nature, origin, and interpretation of the New Testament texts, as well as chapters concerning the political, social, religious, and philosophical world of antiquity. Abridged by Marion Soards, who worked with Brown on the original text, this new, concise version maintains the essence and centrist interpretation of the original without tampering with Brown’s perspective, insights, or conclusions.

[Introducing the New Testament: A Historical, Literary, and Theological Survey](https://smile.amazon.com/Introducing-New-Testament-Historical-Theological/dp/0801099609/) by Mark Allan Powell

This lively, engaging introduction to the New Testament is critical yet faith-friendly, lavishly illustrated, and accompanied by a variety of pedagogical aids, including sidebars, maps, tables, charts, diagrams, and suggestions for further reading. It offers expanded coverage of the New Testament world in a new chapter on Jewish backgrounds, features dozens of new works of fine art from around the world, and provides extensive new online material for students and professors available through Baker Academic's Textbook eSources.

[The New Testament: A Historical Introduction to the Early Christian Writings](https://www.amazon.com/New-Testament-Historical-Introduction-Christian/dp/019020382X/) by Bart D. Ehrman

Featuring vibrant full color throughout, the sixth edition of Bart D. Ehrman's highly successful introduction approaches the New Testament from a consistently historical and comparative perspective, emphasizing the rich diversity of the earliest Christian literature. Distinctive to this study is its unique focus on the historical, literary, and religious milieux of the Greco-Roman world, including early Judaism. As part of its historical orientation, the book also discusses other Christian writings that were roughly contemporary with the New Testament, such as the Gospel of Thomas, the Apocalypse of Peter, and the letters of Ignatius.

[An Introduction to the New Testament](https://smile.amazon.com/Introduction-New-Testament-D-Carson/dp/0310238595/) by D. A. Carson & Douglas J. Moo

An Introduction to the New Testament focuses on "special introduction" that is historical questions dealing with authorship, date, sources, purpose, destination, and so forth. For each New Testament document, the authors also provide a substantial summary of that book’s content, discuss the book’s theological contribution to the overall canon, and give an account of current studies on that book, including recent literary and social-science approaches to interpretation. The chapter on Paul has been expanded to include an analysis of debates on the “new perspective.” The discussion of New Testament epistles has been expanded to form a new chapter.

### Pauline Studies

[Paul the Apostle of Jesus Christ: His Life and Works, His Epistles and Teachings](https://smile.amazon.com/Paul-Apostle-Jesus-Christ-Teachings/dp/0801045584/) by Ferdinand Christian Baur

Baur posed, in inescapably sharp form, a question which has haunted Christianity throughout its history: is Christianity simply a form of Judaism, development from Judaism, or was it, as Baur argued, from the beginning something quite distinct, a religious spirit or consciousness which could not be or become itself until it broke through the limits and restrictions of its historical origins? Baur's radical answer set the agenda for the rest of the nineteenth century, and though neglected for most of the twentieth century, the question has reemerged with renewed force in a post Holocaust world.

[Paul and His Interpreters: A Critical History](https://smile.amazon.com/Paul-His-Interpreters-Critical-History/dp/0649004388/) by Albert Schweitzer

In Paul and His Interpreters, Schweitzer reviews and critiques the history of scholarly analysis of Paul's theology. He is highly critical of previous scholarship, which he more-or-less politely accuses of sloppy thinking and poor use of sources. Schweitzer is particularly concerned with refuting the idea that Paul's theology was influenced by either classic Greek Hellenism or Greek mystery religions, and with establishing that it is at least compatible with late Jewish eschatology. This latter theme is taken up in Schweitzer's later work The Mysticism of Paul the Apostle, in which Schweitzer thoroughly explains his own analysis of Paul's theology.

[The Mysticism of Paul the Apostle](https://smile.amazon.com/Mysticism-Apostle-Albert-Schweitzer-Library/dp/0801860989/) by Albert Schweitzer

In The Mysticism of Paul the Apostle Albert Schweitzer goes against Luther and the Protestant tradition to look at what Paul actually writes in the Epistles to the Romans and Galatians: an emphasis upon the personal experience of the believer with the divine. Immediately after the Gospels, the New Testament takes up the history of the early Christian Church, describing the works of the twelve disciples, and introducing Paul, the man whose influence on the history of Christianity is beyond calculation. To students of the New Testament, this book opens up Paul by presenting him as offering an entirely new kind of mysticism, necessarily and exclusively Christian.

[Paul and Rabbinic Judaism: Some Rabbinic Elements in Pauline Theology](https://smile.amazon.com/Paul-Rabbinic-Judaism-Elements-Theology/dp/0800614380/) by W. D. Davies

W. D. Davies' Paul and Rabbinic Judaism was a watershed book fifty years ago, calling interpreters to situate Paul within a properly Jewish context. Interpreters of the New Testament today still have much to learn from Davies' deep and sympathetic engagement with Jewish sources. The publication of this new edition is both an occasion for celebration and an opportunity for New Testament scholars -- and everyone interested in Jewish-Christian dialogue -- to reflect freshly on the grounding of Paul's thought in Israel's Torah.

[Paul: The Theology of the Apostle in the Light of Jewish Religious History](https://smile.amazon.com/PAUL-theology-Apostle-religious-history/dp/B0000CKZB8/) by H. J. Schoeps

A major study of the apostle to the Gentiles, combining exceptional scholarship with an unusual approach that interprets Paul's theology in the light of his Jewish background, an influence that inevitably coloured and conditioned his Christological teaching.

[Paul and the Salvation of Mankind](https://smile.amazon.com/Paul-Salvation-Mankind-Johannes-Munck/dp/0334012295/) by Johannes Munck

Professor Munck may not always carry conviction, but his arguments deserve careful study and are a valuable corrective to theories which represent St Paul and his theology as independent of Christianity's historic roots in Jewish thought. The author argues with great skill the thesis that St Paul was never opposed to Jerusalem or to a Judaising party inside the Primitive Church, but that all his work among the Gentiles had a thoroughly Jewish inspiration and was founded on loyalty to the Jewish tradition.

[Perspectives on Paul](https://smile.amazon.com/Perspectives-Paul-Ernst-Ka%CC%88semann/dp/0800600304/) by Ernst Käsemann

Kasemann treats major themes in Paul's thought with his customary originality, clarity, and brilliance. The book's starting point is Paul's anthropology. Kasemann maintains that Paul stresses both man's individuality and his place within the cosmos. For Paul, justification must also be seen in cosmic and eschatological terms. Individual essays in this book focus on Paul's theology of the cross, on the motif of the body of Christ, on Christian worship, and on the terms spirit and letter.... This collection of essays is essential reading for anyone studying Paul or New Testament theology.

[Paul and Palestinian Judaism: A Comparison of Patterns of Religion](https://smile.amazon.com/Paul-Palestinian-Judaism-Anniversary-Christianity/dp/1506438148/) by E. P. Sanders

Sanders proposes a methodology for comparing similar but distinct religious patterns, demolishes a flawed view of rabbinic Judaism still prevalent in much New Testament scholarship, and argues for a distinct understanding of the apostle and of the consequences of his conversion.

[Christ and the Law in Paul](https://smile.amazon.com/Christ-Law-Paul-Brice-Martin/dp/1579107621/) by Brice L. Martin

Martin argues that Paul's differences with the Jewish view of the law stem from his starting point that righteousness and life (or salvation) can only come through Christ. Our dilemma is that we are fallen (or en sarki). We are obligated but unable to obey the law. For us this results in sin and death. By dying and rising with Christ we receive righteousness and life, and the Spirit. Empowered by the Spirit we obey the law, not to get saved, or to stay saved, but because we have been saved.

[Galatians: A New Translation with Introduction and Commentary](https://smile.amazon.com/Galatians-Anchor-Yale-Bible-Commentaries/dp/0300139853/) by J. Louis Martyn

Writing his letter to the Galatians in the midst of that struggle, Paul was concerned to find a way by which he could assert the radical newness of God’s act in Christ while still affirming the positive relation of that act to the solemn promise God had made centuries earlier to Abraham. We can therefore see why Galatians proved to be a momentous turning point in early Christianity: In this letter Paul preached the decisive and liberating newness of Christ while avoiding both the distortions of anti-Judaism and his opponents’ reduction of Christ to a mere episode in the epic of Israel’s history. Like the Galatians of Paul’s day, we can begin to hear what the apostle himself called “the truth of the gospel.” As its predecessors in the Anchor Bible series have done Galatians successfully makes available all the significant historical and linguistic knowledge which bears on the interpretation of this important New Testament book.

[Romans: A Commentary](https://smile.amazon.com/Romans-Commentary-Hermeneia-Critical-Historical/dp/0800660846/) by Robert Jewett

It includes fresh insights from the: archaeology of the city of Rome; social history of early Christianity; social-scientific work on early Christianity; and, interpretation and reception of Paul's letter through the ages. It focuses on Paul's missionary plans and how his letter reframes the system of honour and shame as it informed life in the Roman empire at the time.

[Echoes of Scripture in the Letters of Paul](https://smile.amazon.com/Echoes-Scripture-Letters-Paul-Richard/dp/0300054297/) by Richard B. Hays

Paul’s letters, the earliest writings in the New Testament, are filled with allusions, images, and quotations from the Old Testament, or, as Paul called it, Scripture. In this book, Richard B. Hays investigates Paul’s appropriation of Scripture from a perspective based on recent literary-critical studies of intertextuality. His uncovering of scriptural echoes in Paul’s language enriches our appreciation of the complex literary texture of Paul’s letters and offers new insights into his message.

[Theological Issues in the Letters of Paul](https://smile.amazon.com/Theological-Issues-Letters-Louis-Martyn/dp/0687056225/) by J. Louis Martyn

Louis Martyn believes that insufficient attention has been paid to the discovery of numerous and pervasive apocalyptic themes in Paul's letters. In Theological Issues in the Letters of Paul we find the results of a lifetime of study of Paul's letters by a well-known and widely respected New Testament scholar. We are now seeing the publication of a number of books from various points of view that are examining the life, thought, and influence of the apostle Paul.

[Perspectives Old and New on Paul: The "Lutheran" Paul and His Critics](https://smile.amazon.com/Perspectives-Old-New-Paul-Lutheran/dp/0802848095/) by Stephen Westerholm

Westerholm first offers a detailed portrait of the "Lutheran" Paul, including the way such theologians as Augustine, Luther, Calvin, and Wesley have traditionally interpreted "justification by faith" to mean that God declares sinners "righteous" by his grace apart from "works." Westerholm then explores how Paul has fared in the twentieth century, in which "New Perspective" readings of Paul see him teaching that Gentiles need not become Jews or observe Jewish law to be God's people.

[The New Perspective on Paul](https://smile.amazon.com/New-Perspective-Paul-James-Dunn/dp/0802845622/) by James D. G. Dunn

This collection of essays highlights a dimension of Paul's theology of justification that has been neglected -- that his teaching emerged as an integral part of his understanding of his commission to preach the gospel to non-Jews and that his dismissal of justification "by works of the law" was directed not so much against Jewish legalism but rather against his fellow Jews' assumption that the law remained a dividing wall separating Christian Jews from Christian Gentiles.

[The Deliverance of God: An Apocalyptic Rereading of Justification in Paul](https://smile.amazon.com/Deliverance-God-Apocalyptic-Rereading-Justification/dp/0802870732/) by Douglas Campbell

In The Deliverance of God Douglas Campbell holds that the intrusion of an alien, essentially modern, and theologically unhealthy theoretical construct into the interpretation of Paul has produced an individualistic and contractual construct that shares more with modern political traditions than with either orthodox theology or Paul’s first-century world. This book breaks a significant impasse in much Pauline interpretation today, pushing beyond both “Lutheran” and “New” perspectives on Paul to a non-contractual, “apocalyptic” reading of many of the apostle’s most famous -- and most troublesome -- texts.

[Paul: The Apostle's Life, Letters, and Thought](https://smile.amazon.com/Paul-Apostles-Life-Letters-Thought/dp/0800629566/) by E. P. Sanders

Always careful to distinguish what we can know historically from what we may only conjecture, and these from dogmatically driven misrepresentations, Sanders sketches a fresh picture of the apostle as an ardent defender of his own convictions, ever ready to craft the sorts of arguments that now fill his letters but as Sanders carefully argues were not the basis for his own beliefs and attitudes. Here are familiar themes from Sanders's earlier work the importance of works in Paul's thought, the relationship of "plight" and "solution" in a presentation that reveals a career's reflection, along with new thinking regarding development in Paul's thought. He also gives sustained attention to a historical sketch of Paul's context, particularly Second Temple Judaism, in order to set comparisons of Paul and that context on solid ground.

[Paul: A Critical Life](https://smile.amazon.com/Paul-Critical-Life-Jerome-Murphy-OConnor/dp/0192853422/) by Jerome Murphy-O'Connor

Reinforcing his critical analysis of Paul's letters with close attention to archaeology and contemporary texts, Murphy-O'Connor not only charts Paul's movements, but extracts a new understanding of his motives and the social and cultural aspects of his ministry. While continuing to give consideration to the Acts, Murphy-O'Connor reconstructs the apostle's life--from his childhood in Taursus and his years as a student in Jerusalem, to the successes and failures of his ministry--from his own writings.

[Paul and the Ancient Letter Form](https://smile.amazon.com/Paul-Ancient-Letter-Form-Hardback/dp/B01MXS4XZN/) by Stanley E. Porter and Sean Adams

Throughout the last century, there has been continuous study of Paul as a writer of letters. Although this fact was acknowledged by previous generations of scholars, it was during the twentieth century that the study of ancient letter-writing practices came to the fore and began to be applied to the study of the letters of the New Testament. This volume seeks to advance the discussion of Paul's relationship to Greek epistolary traditions by evaluating the nature of ancient letters as well as the individual letter components. These features are evaluated alongside Paul's letters to better understand Paul's use and adaptations of these traditions in order to meet his communicative needs.

[Framing Paul: An Epistolary Biography](https://smile.amazon.com/Framing-Paul-Epistolary-Douglas-Campbell/dp/0802871518/) by Douglas A. Campbell

In Framing Paul Douglas Campbell reappraises all these issues in rigorous fashion, appealing only to Paul’s own epistolary data in order to derive a basic “frame” for the letters on which all subsequent interpretation can be built. Though figuring out the authorship and order of Paul’s letters has been thought to be impossible, Campbell’s Framing Paul presents a cogent solution to the puzzle.

[The Theology of Paul the Apostle](https://smile.amazon.com/Theology-Paul-Apostle-James-Dunn/dp/0802844235/) by James D. G. Dunn

Dunn brings together more than two decades of vigorous and creative work on interpreting the letters of Paul into an integrated, full-scale study of Paul’s thought. Using Paul’s letter to the Romans as the foundation for constructing a fuller exposition of Paul’s whole theology, Dunn’s thematic treatment clearly describes Paul’s teaching on such topics as God, humankind, sin, christology, salvation, the church, and the Christian life.

[Paul and Pseudepigraphy](https://smile.amazon.com/Pseudepigraphy-Pauline-Studies-Stanley-Porter/dp/9004256687/) by Stanley E. Porter

In Paul and Pseudepigraphy, a group of scholars engage open questions in the study of the Apostle Paul and those documents often deemed pseudepigraphal, including canonical and non-canonical works.

[The Dead Sea Scrolls and Pauline Literature](https://smile.amazon.com/Scrolls-Pauline-Literature-Studies-Desert/dp/9004227032/) by Jean-Sebastien Rey

This book offers some syntheses of the results obtained in the last decades, and also opens up new perspectives, by highlighting similarities and indicating possible relationships between these various writings within Mediterranean Judaism. Now that all the Qumran scrolls have been published, it is possible to see more clearly the amplitude and impact of this corpus on first century Judaism. In addition, the authors wish to show how certain traditions spread, evolve and are reconfigured in ancient Judaism as they meet new religious, cultural and social challenges.

[Paul and the Faithfulness of God](https://smile.amazon.com/Paul-Faithfulness-God-Christian-Question-ebook/dp/B00GP5FO1Y/) by N. T. Wright

Wright carefully explores the whole context of Paul’s thought and activity—Jewish, Greek and Roman, cultural, philosophical, religious, and imperial—and shows how the apostle’s worldview and theology enabled him to engage with the many-sided complexities of first-century life that his churches were facing. Wright also provides close and illuminating readings of the letters and other primary sources, along with critical insights into the major twists and turns of exegetical and theological debate in the vast secondary literature. The mature summation of a lifetime’s study, this landmark book pays a rich tribute to the breadth and depth of the apostle’s vision, and offers an unparalleled wealth of detailed insights into his life, times, and enduring impact.

[Beginning from Jerusalem - Christianity in the Making Vol. 2](https://smile.amazon.com/Beginning-Jerusalem-Christianity-Making-vol-ebook/dp/B00EP7QZLC/) by James D. G. Dunn

Beginning from Jerusalem covers the early formation of the Christian faith from 30 to 70 C.E. After outlining the quest for the historical church (parallel to the quest for the historical Jesus) and reviewing the sources, James Dunn follows the course of the movement stemming from Jesus “beginning from Jerusalem.” He opens with a close analysis of what can be said of the earliest Jerusalem community, the Hellenists, the mission of Peter, and the emergence of Paul.

[The Sayings of Jesus in the Churches of Paul](https://smile.amazon.com/sayings-Jesus-churches-Paul-regulation/dp/0800600568/) by David L. Dungan

[Paul and the Gift](https://smile.amazon.com/Paul-Gift-John-M-Barclay/dp/0802875327/) by John M. G. Barclay

In this book esteemed Pauline scholar John Barclay presents a strikingly fresh reading of grace in Paul's theology, studying it in view of ancient notions of "gift" and shining new light on Paul's relationship to Second Temple Judaism. He offers a new appraisal of Paul's theology of the Christ-event as gift as it comes to expression in Galatians and Romans, and he presents a nuanced and detailed discussion of the history of reception of Paul.
