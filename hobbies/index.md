>Discussing hobbies - what are you into?

## Find your fellow brethren here.

 * [Alrighty show of hands, how many audiophiles do we have here?](https://tildes.net/~hobbies/4fz/alrighty_show_of_hands_how_many_audiophiles_do_we_have_here)
 * [Anybody “hobonichi” or bullet journal?](https://tildes.net/~hobbies/31j/anybody_hobonichi_or_bullet_journal)
 * [Indoor climbers, what gear are you rockin’?](https://tildes.net/~hobbies/3fp/indoor_climbers_what_gear_are_you_rockin)
 * [EDC Thread?](https://tildes.net/~hobbies/2in/edc_thread)
 * [Anyome here interested in flashlights?](https://tildes.net/~hobbies/1w1/anyone_here_interested_in_flashlights)
 * [Desktop Fabricaion-What are your favorite lasers, 3D printers and more?](https://tildes.net/~hobbies/5ag/desktop_fabrication_what_are_your_favorite_lasers_3d_printers_and_more)
 * [Anyone interested in 3D printing?](https://tildes.net/~hobbies/2oe/anyone_interested_in_3d_printing)
 * [Aquarium/Fish enthusiasts, what are you up to?](https://tildes.net/~hobbies/58v/aquarium_fish_enthusiasts_what_are_you_up_to)
 * [Fountain Pen corner](https://tildes.net/~hobbies/1ms/fountain_pen_corner)
 * [Garderners in da house?](https://tildes.net/~hobbies/4nm/gardeners_in_da_house)
 * [Geocaching!](https://tildes.net/~hobbies/2rx/geocaching)
 * [Does anyone have a homelab?](https://tildes.net/~hobbies/1w6/does_anyone_have_a_homelab)
 * [Mechanical Keyboards anyone?](https://tildes.net/~hobbies/2lo/mechanical_keyboards_anyone)
 * [Car/Motorcycle modifiers, restorers and racers... what do you drive and what have you done to it?](https://tildes.net/~hobbies/1j0/car_motorcycle_modifiers_restorers_and_racers_what_do_you_drive_and_what_have_you_done_to_it)
 * [Anyone into cars? What are you driving?](https://tildes.net/~hobbies/2dg/anyone_into_cars_what_are_you_driving)
 * [Musicians?](https://tildes.net/~hobbies/2fu/musicians)
 * [Where are all my fellow ukulele players at?](https://tildes.net/~hobbies/3d0/where_are_all_my_fellow_ukulele_players_at)
 * [Is anybody here into stargazing?](https://tildes.net/~hobbies/4ez/is_anyone_here_into_stargazing)
 * [Any quilters in the house or needlecrafters?](https://tildes.net/~hobbies/4pb/any_quilters_in_the_house_or_needlecrafters)
 * [Anyone into Whisky/ey?](https://tildes.net/~hobbies/4kk/anyone_into_whisky_ey)

## Endorse/Find a new hobby here.

 * [What’s an easy to get into hobby?](https://tildes.net/~hobbies/41c/whats_an_easy_to_get_in_to_hobby)
 * [What are some interesting hobbies you can start for free and without leaving your home?](https://tildes.net/~hobbies/2oy/what_are_some_interesting_hobbies_you_can_start_for_free_and_without_leaving_your_home)
 * [What is a hobby that none of your friends understand?](https://tildes.net/~hobbies/1fu/what_is_a_hobby_that_none_of_your_friends_understand)
 * [What’s everyone’s hobby?](https://tildes.net/~hobbies/1e7/whats_everyones_hobbies)

## Guides to hobbies

 * [[OC] An introduction to homebrewing.](https://tildes.net/~hobbies/3s8/oc_an_introduction_to_homebrewing)
 * [Dvorak, Colemak and other alternative keyboard layouts.](https://tildes.net/~hobbies/2pl/dvorak_colemak_and_other_alternative_keyboard_layouts)
