Quick reference for paywall types of sites commonly submitted to Tildes:

Soft = Metered access
Hard = Subscriber only

**Soft Paywall**
The Atlantic - atlantic.com
Bloomberg - bloomberg.com
Los Angeles Times - latimes.com
Medium - medium.com
The New Statesman - newstatesman.com
The New Yorker - newyorker.com
The New York Times - nytimes.com
New York Magazine - nymag.com
Rolling Stone - rollingstone.com
The Sydney Morning Herald - smh.com.au
TIME - time.com
WIRED - wired.com

**Hard Paywall**
The Boston Globe - bostonglobe.com (hybrid, boston.com is free but only has older articles)
Business Insider  - businessinsider.com (hybrid, they allow some free articles based on unspecified metrics)
The Economist - economist.com
Financial Times - ft.com
Forbes - forbes.com (hybrid, print articles are hard paywalled, forbes.com/sites/ ones are free)
Fortune - fortune.com
NewScientist - newscientist.com
Scientific American - scientificamerican.com
The Telegraph - telegraph.co.uk
USA Today - usatoday.com
The Wall Street Journal  - wsj.com
The Washington Post - washingtonpost.com (hybrid, soft for some regions, hard for others) <sup>needs confirmation</sup>

---

Note: Some hard paywalled sites occasionally allow certain articles to be freely viewed even though most are still hard paywalled.  A new "Hybrid" category could potentially be created for these, however since the majority of their content is still locked behind a hard paywall, and it's often difficult to know when an article will be free or not, it's probably best to still tag most of them as paywall.hard.

p.s. Please feel free to contribute to this wiki entry by adding more sites, correcting any mistakes, etc.
