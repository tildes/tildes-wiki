Tools used to interact with Tildes that add functionality and/or improve quality of life for users.

### User Created Scripts

| Name | Author | Description |
| ---- | ------ | ----------- |
| [Tildes Automated Posting Script (TAPS)](https://tildes.net/~tildes/epe/a_script_that_posts_topics_to_tildes_designed_to_be_run_on_a_schedule) | @hungariantoast  | A script that posts topics to Tildes, designed to be run on a schedule. |
| [Tildee](https://tildes.net/~tildes/f0v/tildee_a_python_library_for_interacting_with_tildes) | @deing  | A python library for interacting with Tildes |
| [Automatic Tag Editor](https://git.dingenskirchen.systems/Dingens/tildee.py/wiki/Automatic-Tag-Editor) | @deing | A script based on tildee that allows semi-automatic mass search-and-replace for tags |
