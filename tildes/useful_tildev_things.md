**Assorted snippets you might find useful when developing for Tildes. Feel free to add your own!**

All commands below are run inside the Vagrant box, to get inside use `vagrant ssh`.

* When the "interactive Python shell" is mentioned, run `pshell development.ini`. To exit use `exit`.
* When the "PostgreSQL interactive terminal" is mentioned, run `psql -U tildes tildes`. To exit use `\q`.

<details>
<summary>Creating new users</summary>

In the interactive Python shell:

```py
from tildes.models.user import User;
request.db_session.add(User('AnotherUser', 'correct horse battery staple'));
request.tm.commit();
```
</details>

<details>
<summary>Creating new groups</summary>

In the interactive Python shell:

```py
from tildes.models.group import Group;
request.db_session.add(Group('testing.more', 'A manually created group to use for testing even more'));
request.tm.commit();
```
</details>

<details>
<summary>Grant permissions</summary>

In the PostgreSQL interactive terminal:

```sql
insert into public.user_permissions(user_id, group_id, permission) values (user, group, 'permission')
```

Substitute `user` for the target user ID number, `group` for the target group ID number, and `permission` for the target value of the permission. Note that for each permission you would like a user to have, a separate row must be added to the table. Additionally, there is a `permission_type` column that defaults to `ALLOW`, but could also be set to `DENY`. In order to prevent a user from making use of a standard permission. 

<details>
<summary>Available permissions</summary>

Below is a table of some of the permissions as defined by the `userpermissions` enumeration:

| Name | Effect |
|:------|:--------|
| `comment.remove` | Remove posted comments. |
| `comment.view_labels` | See all labels applied to comments and the label appliers. |
| `topic.edit_by_generic_user` | Allows the user to edit automated and scheduled posts from the "Tildes" user. |
| `topic.edit_link` | Edit the link on a link topic. |
| `topic.edit_title` | Edit the title on a topic. |
| `topic.lock` | Lock comments for a topic. |
| `topic.move` | Move a topic from one group to another. |
| `topic.post` | Used to allow users to post in special groups, like ~tildes.official. |
| `topic.remove` | Remove posted topics. |
| `topic.tag` | Add or remove topic tags. |
| `user.ban` | Allows user to ban others. |
| `wiki.edit` | Edit and create wiki pages. |
</details>
</details>

<details>
<summary>Adding financial data</summary>

Adding all 3 types of financial data (goal, income and expense) will make the donation goal meter and financials page work properly. If the date ranges are no longer in the current month, you may have to adjust them.

In the PostgreSQL interactive terminal:

```sql
insert into financials (entry_type, description, amount, date_range) values
('GOAL'::financialentrytype, 'The Goal', '50', '[2020-02-01 14:30, 2020-03-01 15:30)'),
('INCOME'::financialentrytype, 'Income Amount', '25', '[2020-02-01 14:30, 2020-03-01 15:30)'),
('EXPENSE'::financialentrytype, 'Expense Amount', '15', '[2020-02-01 14:30, 2020-03-01 15:30)');
```

<small>If you know how to make the date ranges get generated based on the current date, feel free to edit this snippet.</small>
</details>
