# Tildes Wiki Sitemap

This page is a temporary placeholder to help wiki contributors navigate. Find this page easily by bookmarking it!

<details>
<summary>Generating this page</summary>

The sitemap can be automatically generated by [tildes-wiki-sitemap](https://git.bauke.xyz/Bauke/tildes-wiki-sitemap).

If this page is outdated and you can't update it yourself, feel free to [message @Bauke](https://tildes.net/user/Bauke/new_message?subject=Tildes%20Wiki%20Sitemap&message=Update%20the%20sitemap%20you%20doofus!).
</details>

## ~anime

> **Posts about anime and the related media/genres including TV shows, manga, movies, and games.**

* [index](https://tildes.net/~anime/wiki/index)
* [Useful Links](https://tildes.net/~anime/wiki/useful_links)

## ~arts

> **Articles and discussions about the arts - theatre, drawing, painting, photography, poetry, etc.**

There are no wiki pages for ~arts yet, [click here](https://tildes.net/~arts/wiki/new_page) if you want to create one.

## ~books

> **Discussion, news and articles related to books (fiction or non-fiction).**

* [index](https://tildes.net/~books/wiki/index)
* [Reading list](https://tildes.net/~books/wiki/reading_list)

## ~comics

There are no wiki pages for ~comics yet, [click here](https://tildes.net/~comics/wiki/new_page) if you want to create one.

## ~comp

> **Topics focused on the more technical side of computers, things of interest to programmers, sysadmins, etc.**

* [index](https://tildes.net/~comp/wiki/index)
* [Coding Resources](https://tildes.net/~comp/wiki/coding_resources)

## ~creative

> **Arts, crafts, and other DIY-ish things.**

There are no wiki pages for ~creative yet, [click here](https://tildes.net/~creative/wiki/new_page) if you want to create one.

## ~design

> **Topics related to design, both physical (e.g. fashion, architecture, products) and non-physical (e.g. logos, web design, advertising)**

There are no wiki pages for ~design yet, [click here](https://tildes.net/~design/wiki/new_page) if you want to create one.

## ~engineering

There are no wiki pages for ~engineering yet, [click here](https://tildes.net/~engineering/wiki/new_page) if you want to create one.

## ~enviro

> **Topics related to the environment - conservation, recycling, alternate energy, etc.**

* [index](https://tildes.net/~enviro/wiki/index)

## ~finance

> **Topics related to business, economics, personal finance, investing, the stock market, and so on.**

There are no wiki pages for ~finance yet, [click here](https://tildes.net/~finance/wiki/new_page) if you want to create one.

## ~food

> **Topics related to food (and drinks!).**

There are no wiki pages for ~food yet, [click here](https://tildes.net/~food/wiki/new_page) if you want to create one.

## ~games

> **News and discussion about games of all types - video games, tabletop games, board games, etc.**

* [index](https://tildes.net/~games/wiki/index)
* [E3 2019 trailers](https://tildes.net/~games/wiki/e3_2019_trailers)

## ~games.tabletop

> **Topics about tabletop games: board games (both modern and classical), role-playing games, card games, etc.**

There are no wiki pages for ~games.tabletop yet, [click here](https://tildes.net/~games.tabletop/wiki/new_page) if you want to create one.

## ~health

> **A place for discussions concerning health - diets, fitness, etc.**

There are no wiki pages for ~health yet, [click here](https://tildes.net/~health/wiki/new_page) if you want to create one.

## ~health.mental

There are no wiki pages for ~health.mental yet, [click here](https://tildes.net/~health.mental/wiki/new_page) if you want to create one.

## ~hobbies

> **Discussing hobbies - what are you into?**

* [index](https://tildes.net/~hobbies/wiki/index)

## ~humanities

> **Topics and discussion related to the humanities - history, philosophy, language, and so on.**

* [index](https://tildes.net/~humanities/wiki/index)
* [Christianity studies](https://tildes.net/~humanities/wiki/christianity_studies)
* [Judaic studies](https://tildes.net/~humanities/wiki/judaic_studies)

## ~humanities.history

There are no wiki pages for ~humanities.history yet, [click here](https://tildes.net/~humanities.history/wiki/new_page) if you want to create one.

## ~humanities.languages

There are no wiki pages for ~humanities.languages yet, [click here](https://tildes.net/~humanities.languages/wiki/new_page) if you want to create one.

## ~lgbt

> **This group includes discussions and news regarding LGBT-related topics. The umbrella term "LGBT" includes all minority sexualities and gender identities. Everybody is welcome to participate.**

* [LGBT Resources](https://tildes.net/~lgbt/wiki/lgbt_resources)

## ~life

> **Topics related to our personal (and professional) lives - work, school, families, relationships, parenting, etc.**

There are no wiki pages for ~life yet, [click here](https://tildes.net/~life/wiki/new_page) if you want to create one.

## ~life.home_improvement

There are no wiki pages for ~life.home_improvement yet, [click here](https://tildes.net/~life.home_improvement/wiki/new_page) if you want to create one.

## ~life.men

There are no wiki pages for ~life.men yet, [click here](https://tildes.net/~life.men/wiki/new_page) if you want to create one.

## ~life.pets

There are no wiki pages for ~life.pets yet, [click here](https://tildes.net/~life.pets/wiki/new_page) if you want to create one.

## ~life.style

There are no wiki pages for ~life.style yet, [click here](https://tildes.net/~life.style/wiki/new_page) if you want to create one.

## ~life.women

There are no wiki pages for ~life.women yet, [click here](https://tildes.net/~life.women/wiki/new_page) if you want to create one.

## ~misc

> **Topics that don't fit anywhere else.**

There are no wiki pages for ~misc yet, [click here](https://tildes.net/~misc/wiki/new_page) if you want to create one.

## ~movies

> **Movie-related news, trailers, discussion, etc.**

There are no wiki pages for ~movies yet, [click here](https://tildes.net/~movies/wiki/new_page) if you want to create one.

## ~music

> **Music and related topics. Share songs and music videos, ask for recommendations, post articles, reviews, and more.**

* [index](https://tildes.net/~music/wiki/index)
* [Discover New Music](https://tildes.net/~music/wiki/discover_new_music)
* [Genre Hierarchy](https://tildes.net/~music/wiki/genre_hierarchy)
* [Listening Club](https://tildes.net/~music/wiki/listening_club)
* [RYM Genre Hierarchy](https://tildes.net/~music/wiki/rym_genre_hierarchy)
* [Topic Tags (2019-07-05)](https://tildes.net/~music/wiki/topic_tags_2019_07_05)
* [YouTube Playlister](https://tildes.net/~music/wiki/youtube_playlister)

## ~news

> **All the world's current events, including politics as well as analysis or opinion pieces. Please tag topics with the name of the relevant country or countries.**

* [index](https://tildes.net/~news/wiki/index)
* [Defining political groups](https://tildes.net/~news/wiki/defining_political_groups)

## ~science

> **Everything science-related: discoveries, studies, articles, discussions, and questions. Topics related to any scientific discipline are welcome.**

* [index](https://tildes.net/~science/wiki/index)
* [A Layperson's Introduction to...](https://tildes.net/~science/wiki/laypersons_intro)

## ~space

> **Topics about the universe, space exploration, and related subjects such as rocketry and satellites.**

There are no wiki pages for ~space yet, [click here](https://tildes.net/~space/wiki/new_page) if you want to create one.

## ~sports

> **Sports news, discussions, etc.**

There are no wiki pages for ~sports yet, [click here](https://tildes.net/~sports/wiki/new_page) if you want to create one.

## ~sports.american_football

There are no wiki pages for ~sports.american_football yet, [click here](https://tildes.net/~sports.american_football/wiki/new_page) if you want to create one.

## ~sports.baseball

There are no wiki pages for ~sports.baseball yet, [click here](https://tildes.net/~sports.baseball/wiki/new_page) if you want to create one.

## ~sports.basketball

There are no wiki pages for ~sports.basketball yet, [click here](https://tildes.net/~sports.basketball/wiki/new_page) if you want to create one.

## ~sports.combat

There are no wiki pages for ~sports.combat yet, [click here](https://tildes.net/~sports.combat/wiki/new_page) if you want to create one.

## ~sports.football

There are no wiki pages for ~sports.football yet, [click here](https://tildes.net/~sports.football/wiki/new_page) if you want to create one.

## ~sports.hockey

There are no wiki pages for ~sports.hockey yet, [click here](https://tildes.net/~sports.hockey/wiki/new_page) if you want to create one.

## ~sports.motorsports

There are no wiki pages for ~sports.motorsports yet, [click here](https://tildes.net/~sports.motorsports/wiki/new_page) if you want to create one.

## ~talk

> **Open-ended discussions with fellow Tildes users, casual or serious.**

* [hungariantoast's folksonomy changelog](https://tildes.net/~talk/wiki/hungariantoasts_folksonomy_changelog)

## ~tech

> **Technology news and discussions, intended for a general audience. Posts requiring deep technical knowledge should go in more-specific groups.**

* [index](https://tildes.net/~tech/wiki/index)
* [Customizing Tildes](https://tildes.net/~tech/wiki/customizing_tildes)
* [FOSS Alternatives](https://tildes.net/~tech/wiki/foss_alternatives)
* [Recommended Extensions](https://tildes.net/~tech/wiki/recommended_extensions)

## ~test

> **For testing features, markdown, trying to break the site, etc. DON'T POST ANYTHING IMPORTANT IN HERE, IT MAY BE WIPED AT ANY TIME.**

* [index](https://tildes.net/~test/wiki/index)
* [123](https://tildes.net/~test/wiki/123)
* [_1test](https://tildes.net/~test/wiki/1test)
* [aaaaaaaaaaa](https://tildes.net/~test/wiki/aaaaaaaaaaa)
* [~test](https://tildes.net/~test/wiki/test)

## ~tildes

> **Meta discussion about Tildes itself, including questions, suggestions, and bug reports.**

* [index](https://tildes.net/~tildes/wiki/index)
* [Customizing Tildes](https://tildes.net/~tildes/wiki/customizing_tildes)
* [Hard vs Soft Paywalls](https://tildes.net/~tildes/wiki/hard_vs_soft_paywalls)
* [Interacting with Tildes](https://tildes.net/~tildes/wiki/interacting_with_tildes)
* [Introduction](https://tildes.net/~tildes/wiki/introduction)
* [sitemap](https://tildes.net/~tildes/wiki/sitemap)
* [Unofficial Tildes Chats](https://tildes.net/~tildes/wiki/unofficial_tildes_chats)
* [Useful Tildev Things](https://tildes.net/~tildes/wiki/useful_tildev_things)

## ~tildes.official

> **Official Tildes announcements, updates, and daily discussions (admin posts only).**

* [Contact](https://tildes.net/~tildes.official/wiki/contact)
* [General development](https://tildes.net/~tildes.official/wiki/development/general_development)
* [index](https://tildes.net/~tildes.official/wiki/development/index)
* [Initial setup](https://tildes.net/~tildes.official/wiki/development/initial_setup)
* [Docs landing page](https://tildes.net/~tildes.official/wiki/docs_landing_page)
* [Donate](https://tildes.net/~tildes.official/wiki/donate)
* [Future Plans](https://tildes.net/~tildes.official/wiki/future_plans)
* [Commenting on Tildes](https://tildes.net/~tildes.official/wiki/instructions/commenting_on_tildes)
* [Hierarchical Tags](https://tildes.net/~tildes.official/wiki/instructions/hierarchical_tags)
* [Hierarchical Tag Short Rules](https://tildes.net/~tildes.official/wiki/instructions/hierarchical_tag_short_rules)
* [Instructions](https://tildes.net/~tildes.official/wiki/instructions/index)
* [Mobile devices](https://tildes.net/~tildes.official/wiki/instructions/mobile_devices)
* [Navigating Tildes](https://tildes.net/~tildes.official/wiki/instructions/navigating_tildes)
* [Posting on Tildes](https://tildes.net/~tildes.official/wiki/instructions/posting_on_tildes)
* [Replying and messaging](https://tildes.net/~tildes.official/wiki/instructions/replying_and_messaging)
* [Searching Tildes](https://tildes.net/~tildes.official/wiki/instructions/searching_tildes)
* [Text Formatting](https://tildes.net/~tildes.official/wiki/instructions/text_formatting)
* [The Tildes wiki](https://tildes.net/~tildes.official/wiki/instructions/the_tildes_wiki)
* [Tildes front page](https://tildes.net/~tildes.official/wiki/instructions/tildes_front_page)
* [Tildes groups](https://tildes.net/~tildes.official/wiki/instructions/tildes_groups)
* [User settings](https://tildes.net/~tildes.official/wiki/instructions/user_settings)
* [Philosophy: Content](https://tildes.net/~tildes.official/wiki/philosophy/content)
* [Philosophy](https://tildes.net/~tildes.official/wiki/philosophy/index)
* [Philosophy: People](https://tildes.net/~tildes.official/wiki/philosophy/people)
* [Philosophy: Privacy](https://tildes.net/~tildes.official/wiki/philosophy/privacy)
* [Philosophy: Site Design](https://tildes.net/~tildes.official/wiki/philosophy/site_design)
* [Philosophy: Site Implementation](https://tildes.net/~tildes.official/wiki/philosophy/site_implementation)

## ~transport

There are no wiki pages for ~transport yet, [click here](https://tildes.net/~transport/wiki/new_page) if you want to create one.

## ~travel

There are no wiki pages for ~travel yet, [click here](https://tildes.net/~travel/wiki/new_page) if you want to create one.

## ~tv

> **Television news, trailers, discussion, etc.**

There are no wiki pages for ~tv yet, [click here](https://tildes.net/~tv/wiki/new_page) if you want to create one.
