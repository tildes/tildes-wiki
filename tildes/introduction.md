Lots of new folks seem to be coming in these past days, so I wanted to make a post that compiles some useful things to know, commonly asked questions, and a general idea of tildes history (short though it may be). Please keep in mind that [tildes is still in Alpha](https://tildes.net/~tildes/522/how_will_tildes_work_with_bans#comment-1g87), and many features that are usually present such as repost detection haven't been implemented yet.
 
#### Settings
 
First of all, check out the [settings page](https://tildes.net/settings) if you haven't yet. It's located in your user profile, on the right sidebar. There are different themes available, the account default is the 'white' theme, which you can change. I recommend setting up [account recovery](https://tildes.net/settings/account_recovery) in case you forget your password. There are more features available but you should go look in the settings yourself.
 
#### Posting
 
You can post a topic by navigating to a group and clicking on the button in the right sidebar. Tildes uses markdown, if you are not familiar with it check the [text formatting doc page](https://docs.tildes.net/text-formatting). Please tag your post so it is easier for other people to find, and check out [the topic tagging guidelines](https://docs.tildes.net/topic-tagging). Some posts have a topic log in the sidebar that shows what changes were done to the post since it was posted. You can see an example [here](https://tildes.net/~humanities/8ru/personal_vs_private_property_in_marxism). Some people have the ability to add tags to posts, edit titles, and move posts to different groups. They were given the ability by Deimos, see [this post](https://tildes.net/~tildes.official/53r/users_can_now_be_manually_granted_permissions_to_re_tag_topics_move_them_between_groups_and_edit).
 
#### Topic Tags
 
You can find all posts with the same tag by clicking on a tag on a post, which will take you to an url like `https://tildes.net/?tag=ask`, where ask is the tag you clicked on. Replace ask with whatever tag you want to search for. You can also filter tags within a group like this: `https://tildes.net/~tildes?tag=discussion`, and it will only show you posts within that group. Clicking on a tag while you are in a group achieves the same effect.
 
You can also filter out posts with specific tags by going to your settings and [defining topic tag filters](https://tildes.net/settings/filters).
 
#### Comment Tags
 
Comment tags are a feature that was present in the early days of tildes, but was removed because of abuse. There were five tags you can tag on someone else's comment: joke, noise, offtopic, troll, flame. The tags have no effect on sorting or other systematic features; they were only used to inform the user on the nature of a comment. The tags would show up along with the number of people who applied them, like this: `[Troll] x3, [Noise] x5`
 
People used these tags as a downvote against comments they disliked, and because the tags appeared at the top of a comment in bright colors, they often would bias the user before they read the comment. The abuse culminated in the [first person banned on the website](https://tildes.net/~tildes.official/wv/daily_tildes_discussion_our_first_ban), and the comment tags were disabled for tweaking.
 
As of September 07, 2018, the comment tags have been [re-enabled](https://tildes.net/~tildes.official/63s/comment_tags_have_been_re_enabled_to_experiment_with_input_wanted_on_plans) and are [experimented](https://tildes.net/~tildes.official/6hn/comment_tags_now_affect_sorting_more_changes_coming) with. Any account over a week old will have access to this ability. The tagging button is located on the centre bottom of a comment. You cannot tag your own comment. [Here are the comment tagging guidelines from the docs](https://docs.tildes.net/mechanics#comment-tags).
 
Currently, the tags are: exemplary, joke, offtopic, noise, malice. The exemplary tag can only be applied once every 8 hours, and requires you to write an anonymous message to the author thanking them for their comment. Similarly, applying the malice tag requires a message explaining why the comment is malicious. The tags have different effects on the comments, which you can read about [here](https://tildes.net/~tildes.official/6ue/many_updates_to_the_feature_formerly_known_as_comment_tagging), and [here](https://tildes.net/~tildes.official/6hn/comment_tags_now_affect_sorting_more_changes_coming).
 
#### Search
 
The search function is fairly primitive right now. It only includes the [title and text of posts](https://tildes.net/~tildes.official/5at/extremely_basic_search_added) and [their topic tags](https://tildes.net/~tildes.official/7j9/minor_search_update_topic_tags_are_now_included_in_search).
 
#### Default sorting
 
The current default sorting is *activity, last 3 days* in the main page, *activity, all time* in individual groups. Activity sort bumps a post up whenever someone replies to it. 'Last 3 days' mean that only posts posted in the past 3 days will be shown. You can change your default sort by choosing a different sort method and/or time period, and clicking the 'set as default' button that will appear on the right.
 
#### Bookmarks
 
[You can bookmark posts and comments.](https://tildes.net/~tildes.official/83l/topics_and_comments_can_now_be_bookmarked_aka_saved) The "bookmark" button is on the bottom of posts and comments. Your bookmarked posts can be viewed through the bookmark page in your user profile sidebar. Note: to unbookmark a post, you have to refresh first.
 
#### Extensions
 
@Emerald_Knight has compiled a list of user created extensions and CSS themes here: https://gitlab.com/Emerald_Knight/awesome-tildes
 
In particular, I found the browser extension [Tildes Extended](https://github.com/theCrius/tildes-extended) by @crius and @Bauke very useful. It has nifty features like jumping to new comments, markdown preview and user tagging.
 
#### Tildes Development
 
Tildes is open source and if you want to contribute to tildes development, this is what you should read: https://gitlab.com/tildes/tildes/blob/master/CONTRIBUTING.md
 
For those who can't code, you might still be interested in the [issue boards](https://gitlab.com/tildes/tildes/boards?=) on Gitlab. It contains known issues, features being worked on, and plans for the future. If you have a feature in mind that you want to suggest, try looking there first to see if others have thought of it already, or are working on it.
 
***
 
#### Tildes' Design and Mechanics
 
In other words, how is it going to be different from reddit? Below are some summaries of future mechanics and inspiration for tildes' design. Note: most of the mechanics have not been implemented and are subject to change and debate.
 
1. Tildes will not have conventional moderators. Instead, the moderation duties will be spread to thousands of users by the trust system. [Trust people, but punish abusers]. More info on how it works and why it is designed that way:
     - [Docs on future mechanics](https://docs.tildes.net/mechanics-future)
     - [Possible levels of trust that users can acquire](https://www.reddit.com/r/RedditAlternatives/comments/8kez6p/tildes_by_former_reddit_dev_invite_only/dz8yp7y/)
     - [A long rant](https://www.reddit.com/r/listentothis/wiki/siteideas)
     - Bubble-up mechanics: [1](https://tildes.net/~tildes/vs/what_is_tildes_plan_for_communities_that_get_too_large#comment-4zs) , [2](https://old.reddit.com/r/TrueReddit/comments/8nj5kj/an_exreddit_administrator_is_aiming_to_create_the/dzwh0w0/)
     - [A group is it's worst own enemy](https://tildes.net/~tildes.official/16g/daily_tildes_discussion_a_group_is_its_own_worst_enemy)
     - [If your website's full of assholes, it's your fault](https://anildash.com/2011/07/20/if_your_websites_full_of_assholes_its_your_fault-2/)
     - [On a technicality](https://eev.ee/blog/2016/07/22/on-a-technicality/)
     - [This neat game about trust and game theory](https://ncase.me/trust//)
 
2. Instead of subreddits, there are [groups](https://docs.tildes.net/mechanics#groups), a homage to [Usenet](https://en.wikipedia.org/wiki/Usenet). Groups will be organized hierarchically, the first and only subgroup right now is ~tildes.official. Groups will never be created by a single user, instead, they will be created based on group interest <sup>[citation needed]</sup>. For example, if a major portion of ~games consists of [DnD](https://en.wikipedia.org/wiki/Dungeons_%26_Dragons) posts and they are drowning out all the other topics, a ~games.dnd subgroup would be created - either by petition, algorithm, or both<sup>[citation needed]</sup> - to contain the posts, and those who don't like DnD can unsubscribe from ~games.dnd. There is currently no way to filter out a subgroup from the main group.
 
3. Tildes is very [privacy oriented](https://docs.tildes.net/privacy-policy). See: [Haunted by data](https://tildes.net/~tildes.official/2d4/daily_tildes_discussion_haunted_by_data)
 
***
 
#### Tildes History/Commonly answered questions
 
I recommend you check out [this past introduction post](https://tildes.net/~tildes/r5/were_starting_to_see_a_lot_of_repeat_questions_so_let_me_make_an_introduction_to_tildes_post_for) by @Amarok before anything else, it's a bit outdated but contains many interesting discussions and notable events that have happened on tildes. @Bauke also tracks noteworthy events each month on his website [https://til.bauke.xyz/](https://til.bauke.xyz/). Also see [the FAQ](https://docs.tildes.net/faq) in the docs. Other than that, the best way for you to get an idea of how tildes changed over time is to go to ~tildes.official and look at all the past daily discussions.
 
Below are some scattered links that I found interesting, informative, or important:
 
- There are 2 announced bans, you can read about them [here](https://tildes.net/~tildes.official/wv/daily_tildes_discussion_our_first_ban), and [here](https://tildes.net/~tildes.official/1wa/daily_tildes_discussion_banning_for_bad_faith_trolling_behavior). There have been more bans since, just unannounced. You can tell if a user is banned from their user page: there will be no private message button. [An example](https://tildes.net/user/Hypnotoad). This differs from deleted accounts, which have all content removed and say "This user has deleted their account". [An example.](https://tildes.net/user/kat) Note that when an account is deleted, whether all posts are deleted or not seems to depend on the user's choice. There is no official commentary, but there has been examples of deleted user comments left intact. Example: [deleted user](https://tildes.net/user/wise), [intact post](https://tildes.net/~music/5qf/what_are_the_sounds_of_your_people).
 
- [The first group proposal](https://tildes.net/~tildes.official/342/daily_tildes_discussion_proposals_for_trial_groups_round_1), and the resulting [group additions](https://tildes.net/~tildes.official/3qv/four_new_groups_added_and_everyone_subscribed_anime_enviro_humanities_and_life).
 
- You used to be able to [see who invited a user](https://tildes.net/~tildes.official/1zz/daily_tildes_discussion_should_inviter_invitee_info_be_public) on their profile, but [now you can't](https://tildes.net/~tildes.official/26m/invited_by_information_for_users_is_no_longer_displayed
).
 
- Past introduction posts: [1](https://tildes.net/~talk/dk/introductions), [2](
https://tildes.net/~talk/sy/introductions_round_2), [3](https://tildes.net/~talk/10l/i_think_its_time_for_introductions_episode_3), [4](https://tildes.net/~talk/1cl/noticed_we_have_a_lot_of_new_people_so_lets_do_a_new_introduce_yourself_post), [5](https://tildes.net/~talk/4by/who_are_you)
 
- The very first post on tildes: https://tildes.net/~tildes.official/2/welcome_to_tildes
 
- The first page of ~tildes.official, if you want to start reading from the beginning (there really should be a chronological sorting option): https://tildes.net/~tildes.official?after=5k
 
- [What ](https://tildes.net/~tildes/k9/what_will_tildes_users_be_called) should [the](https://tildes.net/~talk/1bt/so_what_should_our_demonym_be) names [of](https://tildes.net/~talk/1bt/so_what_should_our_demonym_be) Tildes [users](https://tildes.net/~tildes/27m/what_is_the_collective_term_for_tildes_users) be?
 
- [There used to be comment tags](https://tildes.net/~tildes.official/70/daily_tildes_discussion_comment_tags_and_how_they_feel_to_use), but they were [removed because of abuse](https://tildes.net/%7Etildes.official/11n/daily_tildes_discussion_what_do_we_need_to_change_to_make_comment_tags_reasonable_to_re_enable), and [now they are re-enabled again](https://tildes.net/~tildes.official/63s/comment_tags_have_been_re_enabled_to_experiment_with_input_wanted_on_plans).
 
- Some tradition that has started to develop:
 
    - [programming challenges](https://tildes.net/~comp?tag=challenge)
 
    - [music weekly threads]( https://tildes.net/~music?tag=weekly)
 
    - [black mirror watch thread]( https://tildes.net/~tv/4lz/black_mirror_rewach_announcement_and_schedule)
 
    - [what are you reading these days?](https://tildes.net/~books?tag=recurring)
 
    - [what have you been watching/reading this week? (Anime/Manga)](https://tildes.net/~anime?tag=recurring)
 
    - [the "layperson's introduction" series](https://tildes.net/~tildes/8al/meta_post_for_the_a_laypersons_introduction_to_series)
 
- [How do we fund tildes?](https://tildes.net/~tildes/je/lets_talk_about_that_annoying_thing_we_all_dont_want_to_think_about_funding)
 
- [How to handle account deletion](https://tildes.net/~tildes.official/2ta/daily_tildes_discussion_how_to_handle_account_deletion)
 
- [Anonymous posting](https://tildes.net/~tildes.official/2x3/daily_tildes_discussion_allowing_users_to_post_anonymously)
 
- Demographics survey: year [0](https://tildes.net/~tildes/1eq/unofficial_tildes_users_demographics_survey_year_0) and [results](https://tildes.net/~tildes/212/demographics_survey_results_year_zero), year [0.5](https://tildes.net/~tildes/7x5/unofficial_tildes_demographics_survey_year_0_5) and [results](https://tildes.net/~tildes/90t/demographics_survey_results_year_0_5)
 
- [How should we deal with low effort content?](https://tildes.net/%7Etildes.official/3t5/daily_tildes_discussion_just_try_to_relax_a_bit)
 
- [Why is the comment box at the bottom?](https://tildes.net/%7Etildes/ov/we_gotta_move_the_comment_box_from_the_bottom_of_the_comments_to_the_top)
 
- [How does Tildes feel about bots?](https://tildes.net/~tildes/16x/how_does_tildes_feel_about_bots)
 
- The Tildes unofficial Minecraft servers: [1](https://tildes.net/~games/5ne/tildes_unofficial_minecraft_server) , [2](https://tildes.net/~games/96z/would_anyone_be_interested_in_a_tildes_exclusive_minecraft_server_again)
 
- [The Tildes unofficial wiki](https://tildes.net/~tildes/57h/i_made_a_tildes_wiki_to_better_help_communities_create_permanent_archives_of_their_development_and) (now closed due to inactivity and spambots)
 
- [How will Tildes work with bans?](https://tildes.net/~tildes/522/how_will_tildes_work_with_bans)
 
- [Should we have a mascot?](https://tildes.net/~tildes/57g/we_need_a_loveable_mascot)
 
- [Scraping Tildes/Statistics](https://tildes.net/~tildes/9qf/scraping_tildes_as_an_exercise_and_statistics)
 
***
 
If anyone thinks of a link that should be included here, post a comment with the link and I'll edit it in[.](https://pastebin.com/5uDhS4q1)
 
To the rest: have fun!
