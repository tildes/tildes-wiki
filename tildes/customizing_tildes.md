Successor of the awesome-tildes project by @Emerald_Knight listing userthemes, userscripts, and extensions to improve your browsing experience! 

## Changes using the Web Browser

### Extensions

Browser extensions (addons) for Firefox, Chrome, etc... They usually add a number of useful features, especially ones that require deeper integration with the browser.

| Name | Author | Description |
| ---- | ------ | ----------- |
| [Tildes Shepherd](https://gitlab.com/tildes-community/tildes-shepherd) | @Bauke | An extension which provides interactive guided tours of Tildes! [Announcement thread](https://tildes.net/~tildes/16jy/announcing_tildes_shepherd_a_webextension_with_interactive_guided_tours_for_tildes) |
| [Tildes ReExtended](https://gitlab.com/tildes-community/tildes-reextended) | @Bauke | An improved version of @crius's [original extension](https://github.com/theCrius/tildes-extended) with improved user labels, a button to jump to new comments, and many other features. It's bascally mini Reddit Enhancement Suite for Tildes! |

#### Packing webextensions for use on Chromium-based browsers

*This entire section is now outdated for ReExtended. See the new [Chrome Web Store](https://chrome.google.com/webstore/detail/olmhgeajhikmikkdbbdgnmeobfbpefhj) page to install the official version of ReExtended!*

@Bauke, the developer of the above referenced extensions, [did not publish on the Chrome Webstore due to past issues with automated checks/removals](https://tildes.net/~tildes/17kq/tildes_reextended_has_been_updated_and_is_also_back_on_the_chrome_web_store_again). Instructions are provided here for users who want to use these extensions on Chromium-based browsers.

Unpacked extensions don't work for regular profiles since Chromium forbids loading folders beginning with an underscore.

The work-around is as follows:  
1. Download the latest extension release (On Gitlab, Deploy > Releases) and unzip it
2. Open Chromium's "extensions" page
3. Enable developer mode
4. Click "Pack Extension" button and point to the extension directory
5. Chromium will automatically generate a signing key, zip the build directory, and generate the crx
6. Drag and drop the crx into the extensions page, or open the crx file with Chromium.

Don't lose the `.pem` key or else you will need to back up your settings and uninstall the old version of the extension before upgrading.

[Instructions](https://tildes.net/~tildes/pkw/#comment-5697) courtesy of @rfr

### Scripts

Userscripts require an extension like [Violentmonkey](https://violentmonkey.github.io/get-it/) to work. They are similar to browser extensions but are usually less powerful. Firefox users (especially those who have tweaked `about:config`) should be aware that [userscripts may not work on Tildes](https://tildes.net/~tildes/ea2/tildes_focus_a_greasemonkey_script_to_navigate_to_new_comments_on_tildes#comment-3eg2) due to [bugs](https://bugzilla.mozilla.org/show_bug.cgi?id=1267027).

| Name | Author | Description |
| ---- | ------ | ----------- |
| [Tildenhancer](https://greasyfork.org/en/scripts/369273-tildenhancer) | @thykka | Various UI/UX enhancements, including automatic night mode. | 
| [Tildes Focus](https://tild.es/ea2) | @Wes | Jump around new comments using J/K (vim keybinds) |
| [Youtube Thumbs](https://tildes.net/~tildes/ezb/tildes_user_script_youtube_thumbnails_below_topic_title) | @cadadr | Embeds thumbnails into the header on YouTube topics. |
| [Hide Vote Count](https://tildes.net/~tildes/ejk/userscript_that_hides_votes_like_in_the_recent_experiment) | @cadadr | Hides the vote count, similar to the old [Tildes experiment](https://tildes.net/~tildes.official/e7i/the_number_of_votes_on_comments_is_no_longer_visible_for_the_next_week) |
| [Username Drag'n'Drop](https://tildes.net/~tildes/g46/tildes_user_script_drag_and_drop_usernames_in_order_to_mention_them_in_your_comments) | @cadadr | Drag and drop usernames into reply forms to insert the name. |
| [Comment Link Fix](https://tildes.net/~tildes/1647/tildes_userscript_comment_link_fix) | @blank_dvth | Fixes comment links (anchors) not working as a result of Tildes' comment collapsing feature. |
| [Sticky NavBar](https://tildes.net/~tildes/162d/for_anyone_that_likes_sticky_navbars) | @ewpratten | Make the navbar stick to the top of the screen |
| [Lazy Userscript](https://tildes.net/~tildes/16h8/lazy_userscript) | @TemulentTeatotaler | Adds hotkeys to navigate and bookmark/ignore/vote posts. |
| [Tildezy](https://github.com/TeJayH/Tildezy) | @Tejay | Adds some features like comment collapse, group favorites, a scroll-to-top button, and user colors. [Tildes Thread](https://tildes.net/~tildes/16q5/tildes_userscript_tildezy) |

### CSS Themes

Userstyles require an extension like [Stylus](https://github.com/openstyles/stylus/#releases) in order to work. They allow a user to completely change the look and styling of a website to their liking.

| Name | Author | Description |
| ---- | ------ | ----------- |
| [Dark & Flat](https://gitlab.com/jfecher/Tildes-DarkAndFlat) | @rndmprsn | A dark and flat theme + alt. version combined with @pfg's [Collapse Comment Bar](https://gist.github.com/pfgithub/1da7d2024a9748085fccfceaf2ce126d) |
| [Aqua Dark](https://openusercss.org/theme/5ba4643ffd8bc10c006d8c6c) | [Exalt (openusercss)](https://openusercss.org/profile/5b9f139c1a64280c0030ee4d) | A green/blue dark theme |
| [Tildes Darkish](https://userstyles.org/styles/161244/tildes-darkish) | [Minikrob (userstyles.org)](https://userstyles.org/users/663475) | Dark theme using lighter shades of grey. |
| [A Tildes Darkly](https://userstyles.org/styles/164183/a-tildes-darkly) | [Derv Merkler (userstyles.org)](https://userstyles.org/users/285330) | Has many popular variations on dark themes found in Linux ricing: Monokai, Obsidian, ArcDark, and Dracula |
| [Retroterminal](https://gitlab.com/Emerald_Knight/Tildes-Retroterminal-Theme) | @Emerald_Knight | Based on early computer displays, mostly black/green with scanlines. |
| [More like Reddit](https://pastebin.com/raw/xpJGNXUf) | @tesseractcat | If you want [Tildes to look like Reddit](https://tildes.net/~tildes/9py/could_someone_proficient_with_css_make_a_userstyle_that_moves_the_vote_buttons_on_posts_to_the_left#comment-2geb). |
| [Digg v4 for Tildes](https://userstyles.world/style/10167/digg-v4-for-tildes) | @s8n | If you want [Tildes to look like Digg](https://tildes.net/~tildes/15ol/digg_v4_for_tildes#comment-7xlm). |
| [Rounded Tildes](https://github.com/vignesh-seven/rounded-tildes-styles/) | @safely | The theme gives Tildes a rounded look. [Tildes Thread](https://tildes.net/~tildes/16cl/i_made_a_thing_to_make_tildes_look_better#comment-8f0v). |
| [Revontuli for Tildes](https://codeberg.org/akselmo/Revontuli/src/branch/main/Tildes) | @akselmo | A relatively high contrast theme. [Tildes Thread](https://tildes.net/~comp/16ds/revontuli_a_high_contrast_ish_colorscheme_for_code_editors_kde_plasma_etc_also_for_tildes#comment-8g2s)  |
| [Tildes Mustard Dark](https://userstyles.world/style/10278/tildes-mustard-dark) | @asher | A dark theme with blue and yellow highlights. [Tildes comment](https://tildes.net/~tildes/16ks/which_theme_are_you_defaulting_to_im_loving_atom_one_dark#comment-8l4z)  |
| [Nord for Tildes](https://userstyles.world/style/10435/nord-for-tildes) | @asher | A dark theme primarily in shades of blue. [Tildes comment](https://tildes.net/~tildes/16ks/which_theme_are_you_defaulting_to_im_loving_atom_one_dark#comment-8lkl)  |
| [Wide Tildes](https://userstyles.world/style/10852/wide-tildes) | @gadling | An userstyle which makes wide aspect ratio viewports better. [Tildes thread](https://tildes.net/~tildes/17wp/wide_tildes_a_user_style_for_better_utilization_of_large_monitors)  |

If you are developing your own userstyle, you can get it to work on any Tildes site built-in theme by using the following selector:
```css
	body[class|=theme] {
		--alert-color: #e66b00;
		...
	}
```

#### Custom Userstyles

Userstyles can also be used to make smaller and more targeted adjustments. Use the browser developer tools, a browser extension such as [Stylus](https://github.com/openstyles/stylus/#releases) and a resource like the [Mozilla Developer Network web docs](https://developer.mozilla.org/en-US/docs/Web/CSS) if you want to try on your own!

For example, the style below will make comment/topic text areas larger:  
```css
@-moz-document domain("tildes.net") {
  .form-markdown textarea.form-input {
    height:700px;
  }
}
```  
[Credits to @hungariantoast and @Deimos](https://tildes.net/~tildes.official/cwh/markdown_preview_is_now_available_when_writing_topics_comments_etc#comment-34xm)

### Custom Userstyle Examples

| Name | Author | Description |
| ---- | ------ | ----------- |
| [Tildes Stylus Template](https://gitlab.com/tomflint/tildes-stylus-template/) | @tomf | Add this theme to Stylus and mess with the colors. Open the front page, a thread, etc, to get a feel for the impact of different colors. |
| [Hide Vote Count (alt.)](https://tildes.net/~tildes/erz/stylus_userstyle_that_hides_comment_vote_counts) | @Soptik | Hides vote count, similar to the old [Tildes experiment](https://tildes.net/~tildes.official/e7i/the_number_of_votes_on_comments_is_no_longer_visible_for_the_next_week) |
| [Tildes Tweaks](https://gitlab.com/refragable/userstyles/-/raw/master/tildes/tildes-tweaks.user.styl) | @rfr | Userstyle with configurable options. Mostly used to hide elements and change the appearance of the comment entry form |
| [Tilweaks](https://tildes.net/~tildes/q0z/tilweaks_a_user_style_i_made_to_clean_up_tildes_interface#comments) | @admicos | Userstyle with configurable options. "Cleans up the interface" by lowering the font size, flattens all default theme colors, and increasing padding. |

### Bookmarklets

Bookmarklets are script snippets that you can save as bookmarks. When you use the bookmarklet on a page, it triggers an effect.

#### Change your current theme

_Please note that these theme bookmarklets are now obsolete. You can change the themes simply by using the dropdown menu at the bottom of the screen in the footer of any page on Tildes or by using the [site settings](https://tildes.net/settings)._

Instructions: Copy the code for the theme you want and save it as a bookmark.  

 - In Firefox press `Ctrl+B` to open your bookmarks sidebar and right click on a bookmark to select `Add a New Bookmark`.
   - Paste the javascript code in the `Location` field and fill out the other fields as you wish. Then place the bookmarklet in your bookmarks toolbar for easy access!
 - In Chrome/Chromium press `Ctrl+Shift+O` to open the bookmark manager.
   - Right click anywhere in the manager and select `Add A New Bookmark`.
   - Paste the javascript code in the `URL` field. Once done you can place the bookmarklet in your bookmarks bar for easy access!

  - White:  
`javascript:(function() { document.cookie = "theme=white;path=%2F;domain=tildes.net;secure";document.location='https://tildes.net';})()`
  - Solarized Light:  
`javascript:(function() { document.cookie = "theme=light;path=%2F;domain=tildes.net;secure";document.location='https://tildes.net';})()`
  - Solarized Dark:  
`javascript:(function() { document.cookie = "theme=dark;path=%2F;domain=tildes.net;secure";document.location='https://tildes.net';})()`
  - Dracula:  
`javascript:(function() { document.cookie = "theme=dracula;path=%2F;domain=tildes.net;secure";document.location='https://tildes.net';})()`
  - Atom One Dark:  
`javascript:(function() { document.cookie = "theme=atom-one-dark;path=%2F;domain=tildes.net;secure";document.location='https://tildes.net';})()`
  - Black:  
`javascript:(function() { document.cookie = "theme=black;path=%2F;domain=tildes.net;secure";document.location='https://tildes.net';})()`

Thanks to @balooga who originally posted the bookmarklets [here](https://tildes.net/~tildes/b1n/i_feel_it_would_be_convenient_to_make_theme_changing_faster#comment-2rkp)!

## Specific Changes with Browser Extensions

### uBlock Origin

>Raymond Gorhill (creator of uBlock Origin):  
>[uBlock Origin](https://github.com/gorhill/uBlock) is __NOT__ an "ad blocker": [it is a wide-spectrum blocker](https://github.com/gorhill/uBlock/wiki/Blocking-mode) -- which happens to be able to function as a mere "ad blocker". The default behavior of uBlock Origin when newly installed is to block ads, trackers and malware sites -- through EasyList, EasyPrivacy, Peter Lowe’s ad/tracking/malware servers, various lists of malware sites, and uBlock Origin's own filter lists.

We can leverage uBlock Origin's advanced capabilities to do things besides ad blocking, like __filtering topics and comments from a specific user__.

#### Custom Filters

```
! Filter topics from a specified user (case sensitive)
tildes.net##article.topic[data-topic-posted-by="Example_Username"]

! Filter comments from a specified user (case sensitive)
tildes.net##.comment-itself:has(> header > .link-user:has-text(/^Example_Username$/))

! Filter specific Tildes threads by ID (check the URL)
tildes.net##article[id="topic-7cv"]

! Filter any topic with a specific term in the title (not case sensitive)
tildes.net##article:has(> header >.topic-title:has-text(/example/i))

! Filter any topic with a specific tag (not case sensitive) (alternative to the Tildes built-in filtering)
tildes.net##article:has(> .topic-metadata > .topic-tags > .label-topic-tag:has-text(/example/i))

! Filter all YouTube submissions
tildes.net##article:has(.topic-title:has([href^="https://www.youtube.com/watch"]))
tildes.net##article:has(.topic-title:has([href^="https://youtu.be/"]))

! Hides ALL comment authors on Tildes (pseudo-anonymous mode)
tildes.net##.comment-itself > header > .link-user

! Hide comment vote button/count (YOU WILL NOT BE ABLE TO VOTE ON COMMENTS WITH UBLOCK ON)
tildes.net##button.btn-post-action[data-ic-verb="PUT"]:has-text(/^Vote/)

! Hide topic vote button/count (YOU WILL NOT BE ABLE TO VOTE ON TOPICS WITH UBLOCK ON)
tildes.net##button.topic-voting

! Hide a specified user's vote count
tildes.net##article.topic[data-topic-posted-by="rfr"] > div.topic-voting
tildes.net##.comment-itself:has(> header > .link-user:has-text(/^rfr$/)) > div.comment-votes

! Hide vote counts on the logged in account's posts/topics
tildes.net##article.is-topic-mine > div.topic-voting
tildes.net##article.is-comment-mine > div.comment-itself > div.comment-votes
```

#### Instructions for use

  1. Open uBlock Origin Settings.
     * Click the uBlock icon in your browser.
     * There is a big "power button". Below it are four icons, you want to click the far right one. Hovering will show "Open the dashboard".
  2. Go to the "My Filters" tab.
  3. Select the filters you wish to use (check the comment denoted by the line starting with the exclaimation mark for the effect). Copy and paste the chosen filters to the text area.
  4. Edit the filters (if necessary)
     * e.g. replace `Example_Username` with `rfr`.
     * The `^` and `$` in the comment filter are important. Don't remove them when changing the username.
     * Check the "short link" in the side bar for the topic id if you wish to filter specific threads. (This is defunct since logged in accounts can ignore specific threads). 
  5. Copy the filters and edit the necessary usernames/IDs again as many times as you wish. (e.g. to filter additional users/topics)
     * Additionally, if you only want the filter(s) to apply in a certain group, add the group name to the domain like this: `tildes.net/~tildes.official##<filter>`
  6. Remember to click the "Apply Changes" button to save your filters.
  7. Refresh any previously open pages on Tildes. Your filters should now be working!

Credits to @rfr for making the [instructions](https://tildes.net/~tildes/9tp/filtering_specific_users#comment-2h2k)!

#### Creating your own filters

Please refer to the uBlock Origin wiki article on [static filter syntax](https://github.com/gorhill/uBlock/wiki/Static-filter-syntax).

[Why you should learn how to create your own filters?](https://blog.wesleyac.com/posts/adblock)  
> Adblock filters are an excellent tool for making the web work the way you want them to, but they can be intimidating to learn! For simple stuff, you can open the "Element Picker" interface, but for more complicated rules I usually open up the inspector and sometimes look at the uBlock wiki. I've found a lot of value in realizing that I can customize my web experience and investing a bit of time in learning how to write adblock rules. I hope you consider writing some rules for yourself!
