  * Discord Unofficial Tildes Chat: <https://discord.gg/PcP8qHS> (some activity)
  * Keybase Team (E2E encryption): <https://keybase.io/team/tildeschat> (some activity)
  * IRC: <https://webchat.freenode.net/?channels=%23tildes&uio=Mj10cnVlJjk9dHJ1ZSYxMT0yMzYa9> (no activity)
  * Matrix Room: <https://matrix.to/#/!eZqTEJKtZqyQmZcbRK:matrix.org?via=matrix.org> (dormant)

**Please remember that these are not endorsed by Tildes, and no one involved with the site has any control or responsibility over what happens within. Do not bring up the unofficial chats to Deimos.**
