### Communicate openly and honestly

Running a community or platform that people enjoy being a part of is largely about trust. Without trust, every action or change is viewed with suspicion, as people try to figure out "the real reason" that it's being done.

Trust is often lost due to a lack of communication, or a history (or perception) of being deceptive. It's not especially difficult to prevent this from happening, but it requires a willingness to communicate regularly, explain the true reasons behind what's being done, and solicit (and actually be willing to listen to) feedback.

Part of this is avoiding "PR-speak", where companies utilize deliberately abstruse terminology as a mechanism for disseminating information to segments of their stakeholders. I'll always try to communicate in plain language: if I need to shut something down I'll tell you it's being shut down, not ["sunset"](https://ig.ft.com/sites/guffipedia/sunset/). The [Privacy Policy](https://docs.tildes.net/policies/privacy-policy) and [Terms of Service](https://docs.tildes.net/policies/terms-of-use) for Tildes were written in this way as well, with as little legalese as possible.

### Trust people, but punish abusers

The large majority of users on a site, generally, behave in good faith and are only interested in legitimately participating and contributing. However, there is always a group of users actively trying to undermine others, and even though they are usually a tiny minority, sites often have to build in such a way to prevent these bad-faith users from being able to do much damage.

This tends to mean that many, potentially, powerful tools cannot be added to the site, since malicious use of them would be too dangerous. Instead of restricting capabilities by needing to design around the worst way any tool could be used, Tildes will default to trusting users to behave in good faith, and punish people that take advantage of that trust. Punishments may involve losing access to certain tools or capabilities, being banned from communities or the site as a whole.

### Recognize that users are people, not just metrics

In his talk, ["Is Anything Worth Maximizing?"](http://nxhx.org/maximizing/), Joe Edelman discusses the difference between making decisions based on metrics compared to basing them on the users' reasons for visiting the site. Too often, sites focus on increasing their raw numbers (pageviews, time on site, etc.) instead of thinking about *why* the users are there and trying to improve that experience. This is, generally, because the site's own goals don't align with the users'; for example, relying on advertising for revenue means that the site wants to show users as many ads as possible, while users would prefer to see none at all.

Because Tildes has been organized specifically to cater to its users' interests, this type of conflict isn't present, and we can focus solely on improving the user experience instead of obsessing over metrics that don't necessarily reflect how well the site serves its users.

As a specific example, many sites are constantly performing thousands of experiments ("A/B tests") on random sets of users to see how changes to the interface or behavior might affect their metrics. This can be a frustrating experience as a user, since elements move around, behave differently, or even disappear entirely from one day to the next. This is exactly what I want to avoid: regularly annoying users and degrading their experience solely because of an obsession with metrics.
