These pages cover the main philosophies behind Tildes: why it exists, how it will operate, and what drives the decisions behind its implementation.

The ["Announcing Tildes" blog post](https://blog.tildes.net/announcing-tildes) also covers some of this material.

* [Philosophy: People](https://tildes.net/~tildes.official/wiki/philosophy/people)

* [Philosophy: Content](https://tildes.net/~tildes.official/wiki/philosophy/content)

* [Philosophy: Privacy](https://tildes.net/~tildes.official/wiki/philosophy/privacy)

* [Philosophy: Site design](https://tildes.net/~tildes.official/wiki/philosophy/site_design)

* [Philosophy: Site implementation](https://tildes.net/~tildes.official/wiki/philosophy/site_implementation)
