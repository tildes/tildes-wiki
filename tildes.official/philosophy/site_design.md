### The "Tildes" name

On Tildes, the tilde symbol (`~`) is used to mark sections of the site: ~music, ~games, ~tv, and so on.

The path that led to "Tildes" was a bit strange. Originally, I wanted to have a name related to the word "spectrum". I think that's a great term for describing an online community platform: a wide range of variance inside a whole. That's why the non-profit behind the site is named "Spectria".

As part of thinking about other topics related to a spectrum, I ended up on waves and waveforms, which led to realizing that the tilde symbol (~) looks like a tiny wave. For multiple reasons, I started really liking the idea of using a tilde as the "marker" for a community on the site (for example, the music community would be "~music").

First, tilde is one of the only "unreserved" characters that can be used in web addresses (URIs). From [the RFC related to URIs](http://www.ietf.org/rfc/rfc3986.txt):

> Characters that are allowed in a URI but do not have a reserved purpose are called unreserved.  These include uppercase and lowercase letters, decimal digits, hyphen, period, underscore, and tilde.

That means that a tilde can always be used in a web address without needing to be escaped or converted. This isn't true for many other symbols; for example, some sites try to put an `@` character in their addresses (usually related to usernames), but since that's not an unreserved character, it will often get converted to `%40`, which looks much uglier. A tilde should always be kept as a tilde.

In addition, the `~` symbol also has an association of "home" to many technical people. If you're using [the Bash shell](https://en.wikipedia.org/wiki/Bash_\(Unix_shell\)) (or various others), a tilde can often be used to refer to the user's home location. For example, the command `cd ~` changes the directory to your home directory. A command like `cd ~deimos` will go to the home directory of the user `deimos`, and so on. I like the idea of each community being thought of as "the home for <topic>".

It's also a bit of a throwback to common addresses on the early web, where users would host their website on a shared system under their username. For example, when I was in university, the address of my website hosted on the Computer Science department's server was something like http://pages.cpsc.ucalgary.ca/~cbirch. [Paul Ford caused a fun resurgence of this a few years ago when he started Tilde.Club](https://medium.com/message/tilde-club-i-had-a-couple-drinks-and-woke-up-with-1-000-nerds-a8904f0a2ebf).

So in the end, a bunch of technical, historical, and associational reasons convinced me that I definitely wanted to use the tilde symbol. From there, it didn't take much until the symbol of the site turned into the actual name.


#### A name for Tildes users

There's no official demonym, and the majority apparently doesn't want one.

> Despite @Kat’s [insidious attempt to influence the data](https://tildes.net/~tildes/7x5/unofficial_tildes_demographics_survey_year_0_5#comment-24r2), “waves” as a demonym only received 5.5% of the vote. The leader for that, overwhelmingly, is “no demonym at all”, with a combined 49% of the votes and 18.5% of respondents strongly preferring the site not to have a demonym. Second place, the generic “users”, only has 15.8% in comparison. The first Tildes-specific demonym present is Tilders/~​rs, with 13.4%.
>
> — [The (unofficial) year 0.5 demographics survey](https://tildes.net/~tildes/90t/demographics_survey_results_year_0_5)


### The Golden Rule

There are many variants of the ["golden rule"](https://en.wikipedia.org/wiki/Golden_Rule), but the base idea is that you should act towards others as you'd like them to act towards you. That philosophy applies to various aspects of how I'm approaching building Tildes: in the end, I'm trying to build the community site that I wish existed, one that treats its users the way they want to be treated.

For example, [having low tolerance for people that consistently make others' experience worse](https://blog.tildes.net/announcing-tildes#limited-tolerance-especially-for-assholes). Nobody (except trolls) hopes to get abuse in response to their posts, so there's no reason to allow that kind of behavior. If people treat each other in good faith and [apply charitable interpretations](https://en.wikipedia.org/wiki/Principle_of_charity), everyone's experience improves.

This sort of approach can also apply to decisions related to site mechanics and features. For example, when a feature has a privacy implication, we should consider how we would want our own data to be treated. If the idea of another site collecting similar data would make us nervous, we should try to figure out a way to adjust the feature to reduce or remove that anxiety.

### Use words, not icons

It's become widespread in web and app design lately to use icons extensively, often with no accompanying text label. This can make understanding an interface quite difficult: a lot of icons are unintuitive or opaque, and it's not uncommon for different sites to use the same icon for different functions (or different icons for the same functions).

On a PC, the user might need to mouse over a bunch of different icons to figure out what they're supposed to mean. On mobile, this usually isn't even possible and the user just has to try pushing buttons to see what happens. The result is that a lot of users won't even realize that some functionality exists, because they weren't able to puzzle out the meaning of an icon or didn't want to click random buttons to see the result.

On Tildes I'm leaning towards using words to label information and functions. That makes it far easier to look at a page and figure out what it's possible to do.

More info:

* [Matt Wilcox - The ineffectiveness of lonely icons](https://mattwilcox.net/musing/the-ineffectiveness-of-icons)

### Tildes and non-English languages

For now, Tildes supports only one language: English. Many of the site's goals will be difficult or impossible to work towards without being able to understand what's going on in a community, so for now everything needs to be primarily in English. This may change someday in the future, and if it does, the hierarchical groups could work very well for giving other languages their own set of groups.


### The comment box

The comment box is placed at the bottom of the page to promote reading all — even very new — comments before posting a new one.

> As mentioned, we've talked about this multiple times, and it's probably the most common request for change I've heard so far—I think most people think it's a mistake that it's at the bottom, not something I did deliberately. People have already linked you to some of the other discussions about it, so let me cover something new this time that I've come to realize about why I like having it at the bottom.
>
> Of course, I think it's a good thing that it encourages people to read the existing comments before posting their own, but there's also a more subtle side effect to this: it encourages responding to other people over posting your own top-level comment. While you're reading the comments on your way down, it's more likely that you'll consider replying to someone else. Maybe you'll end up saying basically the same thing that your top-level comment would have, but now you're doing it by discussing with someone else, instead of just throwing your thoughts out (and hoping that other people start discussing with you).
>
> Also, yes, this gets more and more inconvenient as the thread gets larger, but I think that's also a good thing in some ways. I think most of us that have a lot of reddit experience have learned that once a thread gets to a certain size, a new top-level comment has very little chance of being seen by more than a tiny group of users. If we're coming into a large thread with hundreds of comments, we know that if we want any chance of our comment being seen at all, you pretty much have to reply to one of the chains that's already near the top. This situation isn't really great overall, but I think it's even worse because the comment box being at the top encourages people to post new top-level comments, which will probably feel discouraging when they end up getting no responses or even votes.
>
> One thing that I think is good to keep in mind too is that all threads don't have to behave the same, and maybe we can eventually have some different thread "types". For example, the introductions thread is one where people definitely should be encouraged to post new top-level comments (and the thread should probably also be sorted so the newest comments come first by default). So maybe in the future we could have a separate thread type that sorts by new by default and also moves the comment box to the top, since it's more appropriate in that type of thread.
>
> — [Deimos](https://tildes.net/~tildes/ov/we_gotta_move_the_comment_box_from_the_bottom_of_the_comments_to_the_top#comment-335)



### Lack of downvoting

Tildes does not have negative votes for either topics or comments. The reason for this is that I believe we can implement different mechanics that replace the "proper" use of downvotes without also enabling all the misuses of them.

The ideal usage of a downvote is a generic way to express "this doesn't contribute", but in practice they tend to be used more as "I disagree" or "I don't like this". High-quality posts will often get downvoted because other users disagree with the opinion, and in taste-based communities (such as ones related to music), entire categories of valid posts might not be viable because they'll just be downvoted by users with different tastes.

On Tildes, I want to find ways to accomplish those valuable uses through other mechanics. For example, the comment labels can be used to communicate *why* you don't think a comment contributes. Topic tags will allow users to simply filter out certain types of posts that they're not interested in, instead of downvoting them and hurting them for other users that *do* want to see them.



### No warrant canary on Tildes


> It's an interesting question, and I honestly haven't researched warrant canaries very heavily, but my general feeling towards them is a bit negative because I don't think they're very useful.
> A few concerns/problems I have with warrant canaries in general:
>
> - The legal status of them is pretty questionable. A lot of the justification for them in the US seems to be based on the First Amendment, and since Canada's laws are a little different, it might be even more questionable here. I just found this article written by Jon Penney (a Canadian lawyer and a great guy that I've met and talked to a few times), and he doesn't seem very confident about the legal basis for them in Canada. I could probably contact him and talk to him about it more to see if his opinion's changed at all in the last few years.
>
> - They're kind of only usable once. Once you remove it, you can't really put it back. Even the people that support warrant canaries seem to think they get even more iffy if you start trying to increase the specificity of them (such as by re-adding a new one or narrowing down the date range).
>
> - Even in the cases where they've been used and removed, nobody seems to care and nothing happens. Reddit had one; it was removed a couple years ago. Was it removed because something actually happened, or did they just not want to have it any more? What did removing it actually mean? Regardless of the answers to those questions, as far as I can tell, nothing significant happened in response to it being removed. So was there even a point?
>
> — [Deimos](https://tildes.net/~tildes/7r/warrant_canary#comment-lm)
