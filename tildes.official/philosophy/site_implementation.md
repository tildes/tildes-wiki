## General

### Open-source as much as possible

Similar to the "privacy by default" principle that comes from [Privacy by Design](https://tildes.net/~tildes.official/wiki/philosophy/privacy#privacy_by_design), my goal is "open-source by default". That is, the default state of all code for Tildes should be open-source. There should need to be a strong justification to *not* open-source something.

#### License

Tildes is [licensed under AGPLv3](https://gitlab.com/tildes/tildes/blob/master/LICENSE). My reasoning for this is that it seems to be the only established license that will ensure the code (and anything built on top of it) will always remain open-source.

This isn't because I believe that Tildes's code is valuable or to prevent it from being "stolen". It's intended as a *commitment*, similar in many ways to organizing as a non-profit. It means that anyone that contributes code to Tildes can do so knowing that it can *never* be closed off, for profit, or any other reason.

I recognize that there are many companies that consider AGPL-licensed code too risky to use, but I don't think that's much of a concern for a project like Tildes. I think that would be an important consideration when choosing a license for something like a library, but it's not very relevant for a service that will probably not be widely re-used.

### Use modern versions of simple, reliable, "boring" technology

There should need to be an extremely compelling reason to use a new or less-known technology to implement something if it could also be done with one of the reliable ones already being used.

This will, also, make it easier for people to understand and contribute to the open-source code when there are fewer pieces involved and the ones being used are generally well-known and well-documented.

Here are some of the main technologies being used on Tildes:

* [Python 3.8](https://docs.python.org/3.8/) with [the Pyramid web framework](http://docs.pylonsproject.org/projects/pyramid/en/latest/)
* [PostgreSQL 12](https://www.postgresql.org/docs/12/static/)
* [Redis 5](https://redis.io/)
* [intercooler.js](http://intercoolerjs.org/) and [jQuery](http://api.jquery.com/).

More info:

* [Rich Hickey - Simplicity Matters](https://www.youtube.com/watch?v=rI8tNMsozo0)

### Keep the site lightweight

The average file size and "weight" of web sites has become a bit ridiculous. In April 2016, [it was pointed out that the average page was now larger than the game DOOM](https://mobiforge.com/research-analysis/the-web-is-doom). About a year later, [Dan Luu did some testing and went into this in more depth](https://danluu.com/web-bloat/) after noticing that the majority of popular sites on the web wouldn't even load on a slow/unreliable connection. Since he wrote that article, [page weight has grown by another 20%](https://www.httparchive.org/reports/page-weight?start=2017_02_15&end=latest).

More info:

* [Maciej Ceglowski - The Website Obesity Crisis](http://idlewords.com/talks/website_obesity.htm)

### Code quality is a priority

Especially as an open-source project that wants contributions, it's important to ensure that Tildes's code is generally high-quality, easy to understand and modify. Similar to deliberately choosing simple technology, code should also be written simply whenever possible.

The quality of Tildes's code is kept up through code review and enforcing strict code style (by using [Black](https://black.readthedocs.io/)) and commenting standards, as well as additional tools like [mypy](http://mypy-lang.org/) to require that all functions use Python's new type-annotation system.

More info:

* [Kate Gregory - Simplicity: Not Just for Beginners](https://www.youtube.com/watch?v=n0Ak6xtVXno)

### The site is the main mobile interface, not an app

Tildes is a website. Your phone already has an app for using it&mdash;it's your browser.

Tildes will have a full-featured API, so I definitely don't want to discourage mobile apps overall, but the primary interface for using the site on mobile should remain as the website. That means that mobile users will get access to updates at exactly the same time as desktop ones, and full functionality should always be available on both.

### No limits to logged-out browsing

Tildes won't have limits or roadblocks to browsing the site without logging in. Sites that do that are usually "growth hacking"&mdash;trying to boost their user numbers by using tricks to force people to register even if they don't really want to. Since Tildes does not care about growth, there's no reason to do this.

In fact, from a technical perspective it's *far easier* if more users are logged-out. It's often possible to serve all logged-out users the same static copy of a page, which is very lightweight to do.

### Minimal javascript - don't "flip the web pyramid"

There's a "pyramid" concept that's been used for many years when people talk about building for the web. It usually shows a solid base of HTML, a layer of CSS, and then the tip of the pyramid is javascript. I saw one most recently in Charlie Owen's great talk [Dear Developer, The Web Isn't About You](https://sonniesedge.co.uk/talks/dear-developer), where she refers to it as *The Pyramid of Robustness*.

The idea is that the essential part of a site should also be the most reliable&mdash;the HTML. The "higher" layers are still important for adding design and interactivity, but the site shouldn't completely fall over without them, because they might fail or not be applied for various reasons.

However, modern web development has basically flipped this pyramid upside-down. It's become standard for client-side javascript to deliver&mdash;and even generate&mdash;the HTML and CSS. Many basic, text-based sites (like blogs) no longer display anything without javascript.

I believe that relying on javascript to this level is fundamentally the wrong approach to the web, so Tildes is built in the "traditional" manner. Javascript is used as minimally as possible, and ideally only when it's the only option for accomplishing something. And because of that...

### Completely functional for browsing without javascript

Tildes will always be functional to *browse* without javascript enabled. Someone with javascript disabled should be able to look through all the listings on the site, read all types of posts, and so on.

It won't be a priority to make *interaction* work without javascript. Some features may end up naturally working due to how they're implemented, but I'm not going to worry about making things, like voting, functional when javascript is disabled.

## Privacy

### Zero third-party scripts/assets during normal use

Currently, the only page that includes any third-party assets at all is the one that redirects to a Stripe Checkout page when making a donation. This may be required again for some other donation methods or other very specialized uses, but in general there shouldn't be any third-party scripts or assets used on the site.

This means that when people are using Tildes, their device is communicating *only* with Tildes servers, not other companies that are tracking and collecting their data.

## Security

### Full HTTPS/SSL

Tildes has been built to be used 100% over HTTPS. [It has an A+ from the SSL Labs test](https://www.ssllabs.com/ssltest/analyze.html?d=tildes.net), and uses [HTTP Strict Transport Security (HSTS)](https://en.wikipedia.org/wiki/HTTP_Strict_Transport_Security) to ensure it's always accessed securely, including being on the [HSTS preload list](https://hstspreload.org/).

### Restrictive Content Security Policy (CSP)

Tildes utilizes an extremely restrictive [Content Security Policy](https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP) to eliminate the possibility of [cross-site scripting](https://developer.mozilla.org/en-US/docs/Glossary/Cross-site_scripting) (XSS) and various other web exploits.

XSS attacks, generally, happen if a malicious user discovers a way to get a &lt;script&gt; tag onto the site (usually by finding a loophole in how user-submitted text is processed). With the restrictive CSP used on Tildes, even if someone does discover a hole and manages to inject a script, the CSP will prevent it from having any effect.

The CSP on Tildes is quite close to the most restrictive one possible (while still allowing the site to use self-hosted scripts, images, etc.), and gets a perfect score on [Mozilla's Observatory test](https://observatory.mozilla.org/analyze/tildes.net).
