### Privacy by Design

While building Tildes, I have tried to ensure that I'm following ["Privacy by Design" (PDF)](https://www.ipc.on.ca/wp-content/uploads/2013/09/pbd-primer.pdf), a framework that encourages following certain principles to maximize users' privacy. Below are these principles and how I'm trying to apply them to Tildes:

1. **Proactive not reactive; preventative not remedial.** When creating new features, think about what data will need to be stored, and consider how harmful it might be if that data was to be leaked in the future. Is it possible to reduce the amount of data being stored to lower the potential harm? Can the data eventually be aggregated or anonymized so that we're only storing recent data instead of a full history?
2. **Privacy as the default setting.** If a feature has a significant privacy impact, it should always be opt-in. A brand new account shouldn't need to comb through their settings to improve their privacy; ideally a new account already has the best privacy possible.
3. **Privacy embedded into design.** Privacy is not just an afterthought for Tildes. It's a consequence of the fundamental aspects of the company: a non-profit site with no advertising can promote privacy in ways that would be practically impossible for profit-based companies.
4. **Full functionality: positive-sum, not zero-sum.** Privacy doesn't have to be a trade-off. Features can be designed in such a way that they still offer improvements without impacting privacy. Sometimes a minor change to implementation can make a massive difference in privacy without hurting the feature much or at all.
5. **End-to-end security; full lifecycle protection.** Always consider what happens with data after it's originally stored. Do we really need to keep it forever? Could it be aggregated or anonymized after a while, and the original data deleted? There are many pieces of data where, after a certain amount of time has passed, it's no longer important to know which specific users created that data.
6. **Visibility and transparency: keep it open.** Tildes is "open-source by default". It should be possible for people to examine the Tildes code and see exactly what data is being stored. It should also be possible for people to request to see the actual data stored for their account, and verify it matches up with their understanding of what's being stored.
7. **Respect for user privacy: keep it user-centric.** Always consider, "How would I want my own data treated?" Would you want some other company to be storing the same data for you, and how comfortable would you be if someone looked through it? If the idea makes you uncomfortable, try to figure out some way to reduce or remove the data to eliminate those concerns.

### Privacy Policy

Please see also Tildes' [Privacy Policy](https://docs.tildes.net/policies/privacy-policy).
