Posts on Tildes are formatted using a variant of a special formatting language called [CommonMark](http://commonmark.org/) (which is itself a variant of [Markdown](https://daringfireball.net/projects/markdown/syntax)).

If you're already familiar with Markdown (many other sites use it too), you should feel right at home. If not, this page will explain some of the basics.

## Other references

* [Official CommonMark cheatsheet](http://commonmark.org/help/)
* [10 minute interactive CommonMark tutorial](http://commonmark.org/help/tutorial/)

## Tildes-specific syntax

First, let's talk about some of the special ways to link to other things on Tildes:

### Linking to groups

Groups will be linked automatically by just typing their name, including the `~` in front. For example, if you type `~music` it will convert it to [~music](https://tildes.net/~music).

### Linking to other users

If you'd like to link to another user's profile page, you can use the `@` prefix. For example, typing `@flaque` will convert it to [@flaque](https://tildes.net/user/flaque).

Note that linking to another user this way, in a comment, will also send that user a notification informing them that their username was mentioned.

## Traditional Formatting

### Headers

You can create large header text by starting a line with one or more `#`:

```
# h1 - Largest header
## h2 - Second largest header
### h3 - Third largest header
#### h4 - Fourth largest header
##### h5 - Fifth largest header
###### h6 - Sixth largest header
```

### Bold and italics

With one `*asterisk*` you can create _italicized_ text.

You can use `**two asterisks**` to create **bold** text.

You can also use `__underscores__` in place of asterisks if you prefer.

### Lists

You can use `*`, `-` or `+` at the start of lines to create an unordered (bulleted) list.

For example:

```
* dogs
* cats
* bears
    * Oh my!
```

will create:

* dogs
* cats
* bears
    * Oh my!

You can also use numbers to make an ordered list:

```
1. get the peanut butter
2. get the jelly
3. make lunch
```

Becomes:

1. get the peanut butter
2. get the jelly
3. make lunch

### Links

You can make links with the format: `[text here](https://example.com)`. This will create a link such as [text here](https://example.com).

You can also just include the URL itself and it will be automatically converted to a link.

### Preformatted text and code

If you'd like to put code in-line with regular text, you can wrap it with backticks:

```
Check out my code: `console.log("hello world")`. Isn't it cool?
```

Which will look like: Check out my code: `console.log("hello world")`. Isn't it cool?

If you want to use the backtick character `` ` `` within an in-line code-section, you need to start and end it with 2 (or more) backticks instead and have a non-backtick character at the start and end:
```
``Code with a backtick (`) in it.``
```

You can also use three backticks: ``` to surround a separate block of code:

    ```
    console.log("hello");
    console.log("goodbye");
    ```

Syntax-highlighting is also supported if you specify the language of the code after the initial triple-backtick. For example, to add highlighting to the above example, it would be:

    ```javascript
    console.log("hello");
    console.log("goodbye");
    ```

Any language supported by the Pygments library will work by specifying the "short name" from the list available here: [http://pygments.org/docs/lexers/](http://pygments.org/docs/lexers/)

You can also create a code block by indenting the entire block of code by 4 spaces instead of using the backtick "fencing", but there is no way to add syntax-highlighting with that method.

### Quotes

You can quote someone with a `>` at the start of the line.

```
> I can quote myself like this
```

Which renders:

> I can quote myself like this

Subsequent quoted paragraphs will be merged into a single blockquote, even if there is a blank line between them. To prevent this and have each quote in its own separate block, include at least *two* blank lines between quoted paragraphs, or add something else between them such as text or a horizontal rule.

### Strikethrough

You can strike through some text by putting two tildes before *and* after it.

```
I ~~can't find it~~ found it.
```

Produces:

I <del>can't find it</del> found it.

### Tables

Tables are created by separating cells with pipe (`|`) characters, and require a header row, "delimiter" row, and then any number of rows for the actual table data. Cells can be left/right/center aligned by using colons in the relevant cell in the delimiter row (at the start/end/both respectively).

For example (spacing the columns "properly" is optional):

```
|Mountain     |Height of Summit|
|:------------|---------------:|
|Mount Everest|8,848m          |
|K2           |8,611m          |
```

Produces:

|Mountain     |Height of Summit|
|:------------|---------------:|
|Mount Everest|          8,848m|
|K2           |          8,611m|

If you're creating a large/complex table, it may be simplest to use one of the many HTML table generator tools available and copy the HTML output into your post.

### Horizontal rules

You can add a horizontal rule line with `---`. If you want to split up some sections, then you can do:

```
Chapter 1 - Once upon a time

---

Chapter 2 - The end
```

Will render:

Chapter 1 - Once upon a time

---

Chapter 2 - The end

## HTML support (limited)

Tildes also allows you to use HTML directly, if you prefer. Any HTML that could be generated by markdown can also be entered manually. For example, this will also work for creating a link:

```
<a href="https://tildes.net">Go to Tildes!</a>
```

Which renders as: [Go to Tildes!](https://tildes.net)

## HTML-exclusive formatting

There are several formatting features that can, currently, only be created by writing HTML (no markdown syntax is available):

### Small text

You can write text that will be displayed in a smaller font size (which is good for uses such as "side comments") by using the `<small>` tag.

### "Inserted" text

Strikethrough text can be created with markdown, but you can also (optionally) show the text you're replacing the strikethrough text with using the `<ins>` tag, such as: `I think this starts at ~~9 PM~~ <ins>10 PM</ins>`.

### Superscript and subscript

You can add superscript and subscript text using the `<sup>` and `<sub>` tags, respectively. For example: `E = mc<sup>2</sup>` or `H<sub>2</sub>O`.

### Expandable sections

You can create an expandable (and collapsible) section of text by placing all the content you want to be expandable inside a `<details>` tag. This will have a default label ("Details" in Firefox), but you can set your own label by including a `<summary>` tag inside, containing the label you want to use. For example:

```html
<details>
<summary>Click to view the hidden text</summary>

Here's all the hidden text.

It can have **markdown** in it too.
</details>
```

Which will produce:

<details>
<summary>Click to view the hidden text</summary>
<p>Here's all the hidden text.</p>
<p>It can have <strong>markdown</strong> in it too.</p>
</details>

(Note that if you want to use markdown inside the hidden text, you *must* leave a blank line after the &lt;summary&gt; line!)

The section will be collapsed by default, but if you want it to start out expanded, replace the initial `<details>` with `<details open>`.
