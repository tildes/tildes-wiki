## What are tags?
Tags are links that allow the content on Tildes to be easily navigated. They are optional when submitting content to the site, and can be easily added to any existing content by users with elevated privileges.

They can serve two purposes. The first, the ability to search for publications, authors and subjects. Clicking a tag will take you to all instances of its use on the site, within groups that you are subscribed to. The second use is the ability to use tags to filter out content you don't wish to see in your feed via the topic tag filter.

## Geography
Geographical tags aid in sorting content by region. Utilising hierarchical tags can allow the geography tags to be more granular, from a state or province, down to a city or town.

The United States, Canada and Australia all use 3-level hierarchical tags: `usa.ca.los angeles`, `canada.bc.vancouver` and `australia.nsw.sydney`. The state or province should use nationally recognised abbreviations for their respective country. All other countries use 2-level hierarchical tags: `united kingdom.london`.

Countries are tagged with their full name: `democratic republic of the congo.kinshasa`. With the sole exception of the USA. Country names and spellings should mirror those found on the Wikipedia encyclopedia.

## Content length
There are 5 tags for sorting content by length:

Text content uses `short read` for links less than 400 words in length. `long read` is used for any content stretching over 3000 words. These tags don't need to be applied to content with very minimal text length, indicated by the scraped link information below each link.

Video content uses `short watch` for videos under 5 minutes and `long watch` for videos over 30 minutes. This doesn't apply to `trailers` or `music videos`.

For podcasts the `long listen` tag can be applied for content with a run time over 30 minutes.

## Paywall
Content behind a paywall should be tagged `paywall`. Previous site philosophy was to recognise strengths of the relevant paywalls in the tag, but this is no longer required. Paywall features are constantly evolving and this was a fiddly – and inevitable – losing battle.

The exception is to differentiate gifted links that bypass paywalls for a limited time: `paywall.gifted`. The New York Times gifted links last 1 month. The Washington Post 1 week. Gifted tags will be deprecated on an ad hoc basis by users with editing privileges.

Paywalls are contentious among some users, so tagging as such is to be encouraged.

## Authors and publications
Authors of content are tagged: `author.jeff foust`. All authors should be listed, unless impractical, where you can simply tag no-one. If the author of a YouTube link is known, this can be tagged similarly. Only full names are used in tags. If one isn't known, such as with Reddit handles, then tags need not apply

Publications are tagged using the exact name shown in their favicon, spacing factored in: `source.the new york times`, `source.newatlas`. Substack content is tagged using the name of their substack, not the favicon: `substack.erin in the morning`

Youtube links do not utilse the source tag.

## Ask
Tildes is a forum where lots of questions are asked in the form of topics. There are several tags to differentiate these types of topics from one another.

`ask.survey` is used to ask simple questions such as "Where are you from?" and "Which is your favourite superhero villain?". `ask.discussion` is for deeper topics where back and forth debate should be expected. `ask.recommendations` for guidance on products or services. `ask.advice` on topics where the user is seeking advice on a problem. And finally `ask.experts` for niche topics where an area of expertise of specialisation might be required to answer.

## Politics
The bulk of `politics` content can now be found within the recently created group ~society. If politics doesn't interest you at all, you can unsubscribe from this specific group.

But political content in other groups, such as ~finance and ~enviro, should always be tagged `politics` too so that users who have the politics tag added to their filters can continue to ignore the subject matter wherever it is posted.

Politics content should try to include political parties such as `democratic party` and prominent politicians mentioned like `joe biden` to allow for more detailed search or filtering capabilities on this subject.

## Acronyms
There is no character limit with tags, so acronyms are to be avoided wherever possible. What might seem a commonly used one in your own country might be an enigma to someone from another part of the world.

This is particularly the case with the myriad of government organisations. The FDA is the `food and drug administration` and the EPA is the `environmental protection agency` here on Tildes. Of course there are exceptions for agencies famously known by their acronyms like `nasa` or with locations like `nyc`

When acronyms are used they are never plural, such as `jrpg` or `dvd`

## Names
Names of people mentioned in posted content should always be tagged with their full name: `recep tayyip erdogan`. Surnames only are ambiguous at best. This is especially important for users who are filtering politicians or controversial people from their feeds.

The spelling of non-Western names or those with special characters should follow the spelling framework set out in online encyclopedia Wikipedia. A large number of tags here on Tildes line up with articles on Wikipedia to facilitate easier personal study.

Hierarchical tags for names of franchises like `zelda.breath of the wild` or products like `playstation.5` allow for granular searching.

## Self promotion
Self promotion is not discouraged on Tildes, but common sense dictates not to solely post your own content. This type of content has two variations of tag that can be used:

`original content` typically applies to all text content. Whether that be an article published in the Economist or your own blogpost on Blogger. This tag doesn't apply to essays or long format writing published as text posts in ~talk.

For users who might post a YouTube video of their latest music video, or the link to a mobile game they have built, the `user created` tag is applied.

## What are hierarchical tags?

Hierarchical tags (occasionally known as __subtags__ and similar terms) can essentially be thought of as specifiers to a parent tag:

>Similar to groups, tags also support a hierarchy by separating "sections" with periods. For example, tagging something with rock.progressive will cause it to still be treated as though it has a rock tag (being found in searches and affected by filters), but is more specific and could allow more detailed filtering/searching.

This is a good starting point, but it doesn't expound very well on how you're intended to use them, nor does it really go into how the community has come to use them. Thus, the purpose of this document is firstly to explain some of the conventions of how these tags are used on Tildes; secondly to explain and note where they are used and not used currently; and thirdly, to put forward a "standard" of sorts by which to use them for the future

### Examples of hierarchical tags:

Here are some examples of hierarchical tags in action which demonstrate how they modify and narrow down their parent tag:

* `canada` shows that this topic is about something in the country of Canada.
* `canada.qc` shows that this topic is about something in the province of Quebec within the country of Canada.
* `canada.qc.montreal` shows that this topic is about something in the city of Montreal in the province of Quebec within the country of Canada.
* `ask` shows that this topic is asking a question.
* `ask.recommendations` shows that this topic is asking for recommendations.
* `ask.survey` shows that this topic is asking for people's opinions ("best", "worst", "favourite").

These are quite commonplace examples and fairly straightforward; however, hierarchical tags are used on Tildes in a number of other ways not reflected by either of these examples. This gets to the crux of this document: how else are hierarchical tags used on Tildes, where are places they might be better used or not used at all, and what sort of standard--if any--exists for hierarchical tagging and how can that standard be improved upon and made more consistent?

## Hierarchical tags in practice

### Mostly group-specific tags

Many sections have group-specific or mostly group-specific uses of hierarchical tags. This section covers most of these conventions, organized by the group or groups they are best associated with.

#### ~health

For `medicine.`, `health.` and similar tags, use is somewhat complicated. These tags should generally parallel, but currently and historically do not.

`medicine.` is more frequent than `health.`, generally speaking. The following existing constructions should mostly be used for `medicine.` tags:
* `medicine.veterinary` and other branches of medicine. Wikipedia compiles a [helpful list](https://en.wikipedia.org/wiki/Medicine#Branches) of these branches which you can draw from.
* `medicine.ethics` and similar constructions
* `medicine.technology` and similar constructions

Constructions like `alternative.medicine` should be avoided and converted to the above where they exist.

`health.` is similar. The following constructions exist and should be used and emulated accordingly:
* `health.public` (for public health)
* `health.environmental` (for environmental health)
* `health.brain` (for brain health)

...and so on, as needed.

The tag `public health` is extant, but should generally be deprecated in favor of `health.public` to follow with the trend above. _However_, the similar tag `mental health` is so well established that letting it stand is probably a better idea.

---

#### ~news

* consistent use of place standard for subtags. note that abbreviated and truncated forms generally win out with the subtag despite longform use on the tagging document: no `usa.california` or `canada.british columbia` for example; rather `usa.ca` and `canada.bc`

* note no use of `politics.[place]`; `politics` is standalone tag. ditto no `elections.`; `elections` is also standalone.

---

#### ~music

~music is, frankly, kind of a fucking disaster without consistent tags. Here is an effort to parse out every running tag that exists or which needs to probably become the standard, based on the hierarchical tag system.

##### under the `folk` heading
`folk.pop`

##### under the `rock` heading
`rock.alternative`; `rock.art`; `rock.carnatic`; `rock.experimental`; `rock.folk`; `rock.indie`; `rock.industrial`; `rock.opera`; `rock.progressive`; `rock.psychadelic`; `rock.pub`; `rock.punk`; `rock.rap`

##### under the `pop` heading
`pop.art`; `pop.indie`; `pop.jangle`; `pop.noise`; `pop.power`

##### under the `punk` heading
`punk.art`; `punk.post`

---

#### ~humanities

* For religions, use `[religion name].[denomination]` - such as `judaism.orthodox`, `christianity.catholic`, `islam.sunni`.
* For languages, use `[language].[dialect]`, such as `english.british`, `english.american`, `espanol.peninsular`, `espanol.mexicano`.

---

#### ~science

In the absence of a ~socialscience group, post social science-related topics in ~science with a `socialscience.[discipline]` tag - such as `socialscience.psychology`, `socialscience.sociology`,`socialscience.anthropology`, (examples [here](https://tildes.net/~science?tag=socialscience)).

---

#### ~sports

Sports has a large number of specific tags, some of which make use of hierarchical tags. However, this is one of the few cases where hierarchical tags are messier, more ambiguous, and just a general pain in the ass (particularly the occasional use of `football.`) and should probably be deprecated across the board for those reasons.

##### The specific mess of `football.`

The tag `football.` and similar conventions should probably be mothballed. This tag is, while technically correct, a giant mess because it tries to contain what could be as many as six different sports (and their countless variants) into a single tag, even though many of those sports only nominally resemble each other. It should probably be de-rolled in all cases and broken down as follows:

| Current tag(s)                                                  | Convert into                   |
|-----------------------------------------------------------------|--------------------------------|
| `football.soccer` `soccer.football` `soccer`                    |           `football`           |
| `football.american` `football` (referring to American football) |       `american football`      |
| `football.college`                                              |       `college football`       |
| `football.canadian`                                             |       `canadian football`      |
| `football.gridiron`                                             |       `gridiron football`      |
| `football.australian (rules)`                                   |   `australian rules football`  |
| `football.gaelic`                                               |        `gaelic football`       |
| `football.international rules`                                  | `international rules football` |
| `football.rugby`                                                |             `rugby`            |

##### Possible use cases

Beyond the `football.` tag, there is some on-and-off use of the `[sport].college` convention for college sports related topics. This is fine, but to keep things simpler, it may be best to the `college [sport]` convention, in line with how the `football.` tag should probably be used going forward.

### Mostly group non-specific tags

There are also a number of tags which are more general and occur or can occur in several or all groups on the website. Some of the more common conventions of hierarchical tags that are generally not group-specific are:

#### `recurring.`

`recurring.` is a relatively new set of hierarchical tags, but is used fairly simply. The interval of the thread is used as the subtag, i.e. `weekly`, `biweekly`, `monthly,` and so on. There are three established tags in this vein which cover all known use-cases as of now.

* `recurring.weekly`
* `recurring.biweekly`
* `recurring.monthly`

The tag `recurring.daily` can also be used (as can any other permutation of interval) but thus far has not.

---

#### `nsfw.`, `trigger.`, `tw.`, `cw.` and similar tags

`nsfw.`, `trigger.`, `tw`, and `cw.` are all universal tags that have been used in one form or another to separate out content which might be objectionable and which are still useful for these purposes. Although all four have been used, the community has largely settled on a standard of using `trigger.` over `tw.` and `cw.` with potentially triggering content primarily for reasons of clarity (the `trigger.` tag also been [put forward by Deimos](https://tildes.net/~tildes/50o/trigger_warning_tag_special_flair#comment-1fso) previously as a way of handing potentially triggering and objectionable content). `nsfw.` is also sometimes used, but this is less frequent and usually carries a different implication than `trigger.` does.

As mentioned above, if you are using intending to use a tag of this sort, the preferred option in almost all cases is `trigger.` over `tw.` or `cw.`. For all intents and purposes, `tw.` and `cw.` should be considered mothballed and previous uses of them should probably be converted into `trigger.` at some point (particularly the duplicates `tw.death`, `tw.suicide`, and `tw.selfharm`).

The main established tags under the `trigger.` banner are:
* `trigger.death`
* `trigger.selfharm`
* `trigger.suicide`
* `trigger.sexual violence`
* `trigger.rape`
* `trigger.assault`
* `trigger.child abuse`
* `trigger.transphobia`
* `trigger.homophobia` (not used yet, but presumably applicable due to `trigger.transphobia`'s existence)

These are self explanatory for the most part, and cover most bases; however, if you feel that a particular topic is likely to be triggering for some people, it would be courteous to tag it accordingly in line the above tags. (Do also note that all of these tags can be and often are applied as standalone tags instead of being grouped under `trigger.` due to the fact that `trigger.` has waxed and waned in popularity over Tildes's existence.)

If you are intending to post _graphic_ content, or content which has the potential of exposing people to graphic content (broadly construed), `nsfw.` is generally preferable over `trigger.`. `nsfw.` is quite rare, but one example of it in action is the `nsfw.racism` tag on [Ignore The Poway Synagogue Shooter’s Manifesto: Pay Attention To 8chan’s /pol/ Board](https://tildes.net/~news/csv/ignore_the_poway_synagogue_shooters_manifesto_pay_attention_to_8chans_pol_board) due to the exceptionally racist content screencapped as a part of the submitted article. `nsfw.sex` is also seen on [Do Police Know How To Handle Abuse Within Kinky Relationships?](https://tildes.net/~misc/d9y/do_police_know_how_to_handle_abuse_within_kinky_relationships) due to the explicitly sexual nature of the article's subject, but this is more of a courteous measure than a necessary one--a qualified `nsfw` tag is generally not necessary, and if one is a moderator will most likely add it after the fact.

---

#### `economics.` and similar tags

The `economics` tag can occur in several groups, most often ~science, ~news, and ~misc. While it can take hierarchical tags, standalone `economics` is usually fine. Nonetheless, with specific branches of economics like microeconomics and macroeconomics, hierarchical tags should probably be used (thus `economics.micro`, `economics.macro`, `economics.applied`, and so on). Examples of this in action (and further specification under this scheme) are:
* `economics.trade` (economics and trade)
* `economics.micro.urban` (urban microeconomics)
* `economics.policy.employment` (economic policy with respect to employment)

However, when placed in ~science, the standard is always `socialsciences.economics` over `economics.` to align with the standards of tagging in that group, thus `socialsciences.economics.trade` instead of `economics.trade`. Given that `economics.` in this case is itself a hierarchical tag, it may be pertinent to break off the last hierarchical tag into its own tag where it would lead to three consecutive hierarchical tags, like so:

* `socialsciences.economics.micro` and `urban areas`
* `socialsciences.economics.policy` and `employment`

---

#### `law.`

The `law` tag takes a very large number of modifiers and can be used in just about every group due to the fact that law generally transcends the current set of groups Tildes has. Historically, topics related to law have been tagged in the `[modifier] law` format (i.e. `medical law`, `copyright law`, `us law`, and so on); however, this has generally been phased out by the community in favor of using hierarchical tags for the modifiers. Therefore, with respect to pre-existing tags, constructions like `medical law` should be deprecated in favor of `law.medical`. In addition, the following tags which do exist should be converted accordingly:

* `medical law` (convert to `law.medical`)
* `international law` (convert to `law.international`)
* `labor law` (convert to `law.labor`)
* `employment law` (convert to `law.employment`)
* `antidiscrimination laws` (convert to `law.antidiscrimination`)
* `copyright law` (convert to `law.copyright`)
* `maritime law` (convert to `law.maritime`)
* `environmental law` (convert to `law.environmental`)
* `gun laws` (convert to `law.guns`)

All single modifier tags should follow a pattern like this. In other words, if you were going to tag something as "abortion law", you should do `law.abortion` instead of `abortion law`. Currently well established tags following this format are: `law.citizenship`, `law.international`, `law.labor`, `law.marriage`, and `law.juvenile`.

The following tags with location tags in them (and similar tags like them) should be converted slightly differently from the above tags. Instead of being rolled directly, the locator tag (or what would be the locator tag) should be broken out from the tag, and the tag that is left should have its modifier turned into a hierarchical tag if possible. Thus:

* `usa federal laws` is converted to `law.federal` and `usa`. (To elaborate in this case, the `usa` is separated, leaving `federal laws` which can be converted into `law.federal`)
* `us law` is similarly converted to `law` and `usa`
* `european law` is converted to `law` and `european union`

However, this should generally _not_ be done with tags which refer to specific laws. For example `religious neutrality law`, `blue laws` and `safe haven law` are tags which should not be converted to use hierarchical tags because it makes little sense to do so.

There are also two specific tags which should generally not be rolled, which are `martial law` and `law enforcement`. Martial law is mostly used to refer to a specific state of affairs rather than an actual subset of law, so it makes little sense for this to be grouped into the `law` tag, while law enforcement is not really law in the sense being tagged here and is also covered by other tags like `policing`; using `law.enforcement` for this purpose would also be ambiguous, since it more likely would refer to enforcement of legal doctrine.

The use of the `sharia law` tag is ambiguous. Since sharia is _de jure_ a form of law, it would make sense to roll it like the other examples so that the tag is `law.sharia`; however the two uses of it on Tildes are `sharia law` and there is currently no real consensus on whether or not to roll it in this manner.

---

#### `hurricanes.`, `cyclones.`, and `typhoons.`

Tropical cyclone news generally fits into several places, most often ~news, ~enviro, or ~science. Generally, the standard for tagging tropical cyclones, whether they are hurricanes, cyclones, typhoons, or other similar storms is to use the applicable term for the storm in question, and then use a hierarchical tag for the storm's name. Actual examples of this are:

* [`typhoons.yutu`](https://tildes.net/?tag=typhoons.yutu) for the Pacific typhoon basin's Typhoon Yutu
* [`hurricanes.michael`](https://tildes.net/?tag=hurricanes.michael) for the Atlantic basin's Hurricane Michael
* [`cyclones.idai`](https://tildes.net/?tag=cyclones.idai) for the South-West Indian basin's Cyclone Idai

This is relatively straightforward, and covers the nomenclature of all existing basins. However, some basins have not been represented on Tildes thus far, so here are the two cases where standards overlap for reference:

* the Pacific hurricane basin and the South Atlantic basin would both be represented by the same standard as the Atlantic basin (thus, `hurricanes.patricia` for the Pacific [Hurricane Patricia](https://en.wikipedia.org/wiki/Hurricane_Patricia) and `hurricanes.catarina` for the South Atlantic [Hurricane Catarina](https://en.wikipedia.org/wiki/Hurricane_Catarina))
* the Australian, North Indian, and South Pacific basins would be represented by the South-West Indian basin's standard (thus, `cyclones.tracy` for Australian [Cyclone Tracy](https://en.wikipedia.org/wiki/Cyclone_Tracy), `cyclones.fani` for North Indian [Cyclone Fani](https://en.wikipedia.org/wiki/Cyclone_Fani), and `cyclones.gita` for South Pacific [Cyclone Gita](https://en.wikipedia.org/wiki/Cyclone_Gita)).

For convenience purposes, storms which are named but have not hit hurricane status should probably still be referred to with the corresponding cyclonic storm tag for their basin, even though they have not formally reached hurricane, cyclone, or typhoon status.

If there is no name to refer to (i.e. a name has not been designated for the storm), a hierarchical tag should probably not be applied at all, since that would get messy and likely necessitate updates. With storms that have only nicknames or lack a name under the nomenclature since they predate cyclone naming (for example, the [1938 New England Hurricane](https://en.wikipedia.org/wiki/1938_New_England_hurricane)) there's really no best way to do things, however, using a truncation of the nickname may be the most preferable option (for example: `hurricanes.1938 new england`).
