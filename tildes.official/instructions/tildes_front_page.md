When you view Tildes, you'll see a front page that lists topics that have been posted in various groups.

### Sorting the topics

You can change the order in which the topics are displayed.

There are five sorting options:

* **Activity** orders the topics so that the topics with recent comments posted in them appear at the top. Some comments are excluded and will not cause the topic to "bump", such as ones made in threads labeled as Offtopic and Noise.

 * **Votes** orders the topics so that the topics with the highest number of votes appear at the top.

 * **Comments** orders the topics so that the topics with the highest number of comments appear at the top.

 * **New** orders the topics so that the topics posted most recently appear at the top.

 * **All activity** orders the topics so that the topics which have most recently had a comment posted under them appear at the top. This is similar to the "Activity" sort, but without any exclusions (so *any* comment will always cause a topic to come back to the top).

There is also a time-based filter. It shows as "from", with a selection box. This allows you to limit the topics by when they were posted:

* **last 1 hour** will limit the list to only those topics which were posted in the last 1 hour.
* **last 12 hours** will limit the list to only those topics which were posted in the last 12 hours.
* **last 24 hours** will limit the list to only those topics which were posted in the last 24 hours.
* **last 3 days** will limit the list to only those topics which were posted in the last 3 days.
* **all time** will not limit the list in any way: all posts which have ever been posted will be included.
* **other period** allows you to select a custom time period to limit the list. You can enter a number of hours, or you can enter a number of days by typing a number followed by "d" ("6" = 6 hours; "6d" = 6 days).

By combining the sorting options and the time-based filter, you can customise which topics will be displayed, and how they will be displayed. For example:

* "Votes" + "30 days" will show you all topics posted in the past 30 days, with the highest-voted topics at the top of the list.
* "Comments" + "3 days" will show you all topics posted in the past 3 days, with the topics that have the most comments posted under them (the ones with the most discussion) at the top of the list.
* "New" + "24 hours" will show you all topics posted in the past 24 hours, with the most recently posted topics at the top of the list.
* "All activity" + "90 days" will show you all topics posted in the past 90 days, with the topics that have had comments posted under them most recently at the top of the list.

There is also a feature to set a sorting option as your default. If you change your sorting option, "Set as default" will appear. If you click on this, the new sorting option you have just created will become the default sorting option for the page you are currently viewing - whether this is the front page (including all groups), or the page for a specific group. You can set a different default sort for every group, and for the front page.

### Filtering the topics

You can filter out some types of topics and prevent them from appearing on your front page. You do this using [topic tags](https://tildes.net/~tildes.official/wiki/instructions/posting_on_tildes#tagging_topics).

All topics should have tags. They'll describe what the topic is about. Some of them will be geographic identifiers ("usa", "china", "india"), some will indicate the subject matter ("biology", "social media", "history"), and some will indicate the type of topic ("ask", "introductions", "casual"). If there's a category of topic you don't want to see on your front page, you can apply a tag-based filter to block those posts.

Go to your user page by clicking on your username at the top of the sidebar on the righthand side of the screen. In the sidebar of your user page, there is a link that says "Settings" under the Misc heading. Click on that to go to your User Settings page. Alternatively, click on [this link](https://tildes.net/settings).

On your User Settings page, there is a link that says "Define topic tag filters" (probably at the bottom of the page). Click on that.

The filter is a single text box. You type the tag(s) you want to filter out in that box. If you want to filter out news about the USA, type "usa". If you want to filter out political topics, type "politics". If you want to filter out introductions topics, type "introductions". And so on.

You can have more than one filter: separate each tag by a comma, such as "*usa, politics, introductions*". This will filter out posts which are about the USA *or* which are political *or* which are introductions. Each tag in your filter is independent of all other tags.

Then click on 'Save filtered tags'.

You can change this list at any time, by adding or deleting tags.

Reference: [You can now define topic tag filters, which will hide topics with certain tags by default in your listings](https://tildes.net/~tildes.official/2a9/you_can_now_define_topic_tag_filters_which_will_hide_topics_with_certain_tags_by_default_in_your)
