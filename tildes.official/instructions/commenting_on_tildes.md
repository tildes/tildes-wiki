### Making a comment

There are two types of comment:

* Replies to a topic.
* Replies to another comment.

To reply to a topic, scroll to the bottom of the page to find the 'Post a comment' text box. This is at the bottom of the page by deliberate design: it's intended to make you read the other comments before adding your own. It reduces duplication: if other people have already said what you want to say, then you can add your vote to their comments. It increases engagement: if you see other interesting comments as you're scrolling down, you can join in the conversation.

To reply to a comment, click on the 'Reply' link just below it.

### Formatting comments

While writing your comment, you can apply formatting using a variant of Markdown: [Text Formatting](https://tildes.net/~tildes.official/wiki/instructions/text_formatting).


### Labelling comments

You can apply labels to other people's comments (but not your own). These labels affect the sorting and display of those comments.

Note that labels are only available to users with accounts more than 7 days old. You can't apply labels during your first week on Tildes.

To apply a label to a comment, click on the 'Label' link below the comment, and choose one of the 5 labels:

* Exemplary.
* Offtopic.
* Joke.
* Noise.
* Malice.

You can apply more than one of these labels to a comment (for example, you might label a comment as "joke" *and* "noise" if that's appropriate). Labels generally become active when the collective label weight of all users that applied this label exceeds 1.0. The default weight of new users is 0.5, so two users applying a label should be enough to trigger an effect.

#### "Exemplary" label

The "Exemplary" label is for recognising excellent comments, which are of high quality and which add something significant to the discussion.

You will need to type a brief note when applying an "Exemplary" label to a comment. Your note will be visible to the comment's author (but noone else). Your note can be as short as "good comment", or as long as you like. The note is anonymous: the comment author will not know who wrote it (although you can type that information in your note if you want).

After applying an "Exemplary" label to a comment, you must wait 8 hours before being able to apply another "Exemplary" label.

The "Exemplary" label is visible on comments as a cyan stripe on the left side of the post. Only the person who wrote the comment will be able to see the number of times a comment has been labelled "Exemplary" and the accompanying messages from the labellers.

Each "Exemplary" label applied to a comment increases its weighting in vote-based sorting: the amount of votes is multiplied with the total Exemplary label weight. Going with the default weight, 0.5:

* *A comment with 0 "Exemplary" labels will have its vote weight multiplied by 1 + (0 x 0.5) = 1.0. If this comment has 10 votes, its weighted vote will be 10 x 1.0 = 10 votes.*

* *A comment with 1 "Exemplary" label will have its vote weight multiplied by 1 + (1 x 0.5) = 1.5. If this comment has 10 votes, its weighted vote will be 10 x 1.5 = 15 votes.*

* *A comment with 2 "Exemplary" labels will have its vote weight multiplied by 1 + (2 x 0.5) = 2.0.  If this comment has 10 votes, its weighted vote will be 10 x 2.0 = 20 votes.*

... and so on. The effect of "Exemplary" labels is cumulative: the more "Exemplary" labels a comment has, the higher its voting weight will be.

#### "Offtopic" label

The "Offtopic" label is for identifying comments which have veered away from the main subject of discussion in a thread. They might be good-quality or bad-quality comments, but they're about a different subject.

The "Offtopic" label is not visible on comments.

If the Offtopic label is active on a comment, it will cause the comment to be sorted below replies with no labels, acting as if it had -1 total votes.

#### "Joke" label

The "Joke" label is for comments which exist only to make a joke. They're not adding anything to the discussion, they're there just to deliver a punchline. The "joke" label should *not* be applied to an otherwise good comment which happens to include a minor humorous remark, only for comments where humour is the only ingredient.

The "Joke" label is not visible on comments.

If the "Joke" label is active on a comment, it decreases its weighting in the relevance sorting by a factor of 0.5: the vote weight on any comment labelled as a joke will be halved. The effect of "Joke" labels is not cumulative.

#### "Noise" label

The "Noise" label is for comments which make no difference to the discussion. These might include remarks such as "This^" or "Thanks." or "Good news!" or "This sucks."

The "Noise" label is not visible on comments.

If the Noise label is active on a comment, it will cause the comment to be sorted below unlabelled and Offtopic replies, acting as if it had -2 total votes. Also, the comment will be autocollapsed, so that it is hidden (all its child comments will also be hidden). Collapsed comments can be manually expanded and viewed.

#### "Malice" label

The "Malice" label is for comments which contain negative language. This includes:

* Personal attacks on other Tilders.

* Bigoted language (such as racism, homophobia, transphobia, misogyny, religious slurs, and so on).

* Incitement to violence.

* Hate speech.

The "Malice" label is not visible on comments.

Each application of a "Malice" label on any comment will send a notification to the moderator/s, who will review the comment and take appropriate action. Currently, the only person with the ability to view Malice labels is @Deimos, the site admin.

#### References

Reference: [Comment tags now affect sorting, more changes coming](https://tildes.net/~tildes.official/6hn/comment_tags_now_affect_sorting_more_changes_coming)

Reference: [Many updates to The Feature Formerly Known as Comment Tagging](https://tildes.net/~tildes.official/6ue/many_updates_to_the_feature_formerly_known_as_comment_tagging)

Reference: [Comment labels](https://tildes.net/~tildes.official/wiki/instructions/commenting_on_tildes#labelling_comments)
