Posts on Tildes are called "topics". There are two types of topics:

* **Link topics**, which are links to an offsite article or video.

* **Text topics**, which contain text that is written and hosted on Tildes.

(Note: you can use text topics to post links, if you want to provide an introduction or some context for what you're posting. Simply include the URL in the text field as part of what you type.)

### Creating a topic

To create a topic on Tildes, you must first [navigate to the group](https://tildes.net/~tildes.official/wiki/instructions/navigating_tildes#viewing_a_group) you want to post the topic in. All topics must be posted in a group or sub-group. (It's okay if you're not sure which group to post your topic in. Just pick the group or sub-group you think is most appropriate. If there's a better place for your topic, someone else will probably come along and [move it for you](https://tildes.net/~tildes.official/wiki/instructions/posting_on_tildes#moving_topics).)

In the sidebar of the group, click on the "Post a new topic" button to create a topic. There are three fields to complete: Title, Link, and Text.

* For a **link topic**, complete the Title and Link fields. Type a title for the topic: this will usually be the title of the article or video you're posting. Paste the URL of the article or video into the Link field.

* For a **text topic**, complete the Title and Text fields. Type a title for your topic. Enter your text in the Text field. Use this option if you're asking a question, or presenting your own topic for discussion.

* If you want to post a link topic, and simultaneously post the first comment under your topic, complete all three fields: Title, Link, and Text. Type the title for the topic from the title of the article or video you're posting; paste the URL of the article or video into the Link field; type your opening comment into the Text field.

To submit your topic, click on the "Post topic" button under the entry fields.

Reference: [Daily Tildes discussion (and changelog) - "new topic" page and process updated](https://tildes.net/~tildes.official/2kh/daily_tildes_discussion_and_changelog_new_topic_page_and_process_updated)

### Tagging topics

You can apply tags to your post, which can be used by people [to filter their front page](https://tildes.net/~tildes.official/wiki/instructions/tildes_front_page#filtering_the_topics). Tags are not compulsory, but they are *extremely* useful and helpful. If you don't enter tags on your topic, someone else will probably come along and [add some for you](https://tildes.net/~tildes.official/wiki/instructions/posting_on_tildes#changing_tags)!

Tags go in the 'Tags' field.

Tags should describe what the topic is about. They can be geographic identifiers ("usa", "china", "india"), or they can indicate the subject matter ("biology", "social media", "history"), or they can indicate the type of topic ("ask", "introductions", "casual").

Tags are hierarchical: there can be sub-tags. For instance, a geographical tag can include the country *and* the state/province/county. Or a tag for the type of topic can show the sub-type as well. To include a sub-tag in a tag, use a full stop / period (".") character between the top-level tag and a sub-level tag. They can go to multiple levels.

For example:

* "canada" shows that this topic is about something in the country of Canada.
* "canada.qc" shows that this topic is about something in the province of Quebec within the country of Canada.
* "canada.qc.montreal" shows that this topic is about something in the city of Montreal in the province of Quebec within the country of Canada.
* "ask" shows that this topic is asking a question.
* "ask.recommendations" shows that this topic is asking for recommendations.
* "ask.survey" shows that this topic is asking for people's opinions ("best", "worst", "favourite").

Tags can include *only* letters, numbers, and the space character. No special characters are permitted. "smartphones" and "smart phones" are valid tags; "smart-phones" is not a valid tag.

Tags are not case sensitive: any uppercase letters you enter will be converted to lower case letters.

As you start typing a tag, you will see a list of suggested tags starting with the letter(s) you have typed. This list of suggestions is taken from the most popular tags in the group you are posting to. With each extra letter you type, the list will narrow to match your typing. For example:

* When you type "**s**", you will see a long list of tags starting with "s".
* When you type "s**o**", you will see a medium list of tags starting with "so".
* When you type "so**c**", you will see a short list of tags starting with "soc".

You can apply multiple tags to a topic: separate each tag with a comma.

Reference: [Topic tags](https://tildes.net/~tildes.official/wiki/instructions/posting_on_tildes#tagging_topics)

Reference: [Autocomplete for topic tagging is now available](https://tildes.net/~tildes.official/cpl/autocomplete_for_topic_tagging_is_now_available)

### Editing topics

Some people are able to edit topics after they are posted. This ability is not available to all people. In the long term, this ability will be earned through a proposed trust system; for now, Deimos must manually give this permission to someone.

There are three available ways to edit topics:

* Change the tags on the topic
* Edit the title of the topic
* Move the topic to a different group

All edits to a topic will be recorded in the Topic log, which is found in the sidebar of the topic. The changes made, and the user who made them, will remain visible in the log for 30 days. After 30 days, the record of changes will be removed in accord with Tildes' [privacy policy](https://docs.tildes.net/policies/privacy-policy).

#### Changing tags

To change a tag on a topic, click on the 'Tag' link below the topic's text or URL. All tags will become editable: you can remove old tags and add new tags. Refer to the [Tagging Topics section](https://tildes.net/~tildes.official/wiki/instructions/posting_on_tildes#tagging_topics) for more information.

#### Editing titles

To edit the title of a topic, click on the 'Edit title' link below the topic's text or URL. A text field will open, with the title's text. You can make any changes to this text. When you finish editing, click on the 'Update Title' button to save your changes.

#### Moving topics

To move a topic from one group to another group, click on the 'Move' link below the topic's text or URL. Type in the name of the new group or sub-group for the topic, then click on the 'Move Topic' button to save the changes.

Reference: [Users can now be (manually) granted permissions to re-tag topics, move them between groups, and edit titles](https://tildes.net/~tildes.official/53r/users_can_now_be_manually_granted_permissions_to_re_tag_topics_move_them_between_groups_and_edit)
