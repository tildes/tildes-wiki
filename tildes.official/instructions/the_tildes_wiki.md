**Note**: This feature is currently being experimented with, and is available to users who message @Deimos

Wikis are useful for common databases of information, rules and traditions that a community might have. Unlike comments, all content in the wikis are shared under the [Creative Commons Attribution-ShareAlike 4.0](https://creativecommons.org/licenses/by-sa/4.0/) license and they have a [git history available publicly](https://gitlab.com/tildes/tildes-wiki/commits/master) (so do not post personal data because it can't be deleted!).

Currently there are quirks with the system which should be fixed in the future :

- Groups with no wikis have to be manually navigated to to create pages in them such as [https://tildes.net/~tildes.official/wiki](https://tildes.net/~tildes.official/wiki), if you have permissions, you'll have access to a button to create new pages.

- Pages can't be renamed or deleted, you'll need to [message Deimos](https://tildes.net/user/Deimos/new_message) for that.

- Permissions are global, there is no permission system per-group.



Reference: [A basic wiki system is now available for groups](https://tildes.net/~tildes.official/drm/a_basic_wiki_system_is_now_available_for_groups)
