### Viewing links and comments

When you're viewing the front page, you can click on topics to view the linked item, or to view the comments on the topic.

To open **the linked item** in a topic (article, video, blog), click on the big bold title. Depending on your [user settings](https://tildes.net/~tildes.official/wiki/instructions/user_settings#opening_links), the linked item can open in either a new tab or the current tab.

To open **the comments** on a topic, click on the small link that says 'X comments' below the title. Depending on your [user settings](https://tildes.net/~tildes.official/wiki/instructions/user_settings#opening_links), the comments page can open in either a new tab or the current tab.

### Sorting comments

When viewing a topic page, you can sort the comments in four ways:

* **most votes** sorts the comments by the highest number of votes. Top-level comments with the most votes appear at the top of the page, and top-level comments with the fewest votes appear at the bottom of the page.
* **newest first** sorts the comments so that the most recent top-level comment is at the top of the page and the oldest top-level comment is at the bottom of the page.
* **order posted** sorts the comments so that the oldest top-level comment is at the top of the page and the most recent top-level comment is at the bottom of the page.
* **relevance** sorts the comments by the highest number of votes, including any effects from [comment labels](https://tildes.net/~tildes.official/wiki/instructions/commenting_on_tildes#labelling_comments).


### Finding new comments

There is a user setting which will allows you to highlight new comments in a thread (comments which have been posted since the last time you looked at the thread). You can find more information about that in the '[Marking new comments](https://tildes.net/~tildes.official/wiki/instructions/user_settings#marking_new_comments)' section on the User Settings page of these instructions.

### Collapsing replies

At the top of the comments on a topic page, there are two buttons:

* Collapse replies
* Expand all

If you click on "Collapse replies", all reply comments will be hidden, leaving only top-level comments visible. You can restore any old comments by clicking on the "+" button on the comment stub.

If you click on "Expand all", all comments will be restored to normal visibility.

[Added buttons to collapse all reply comments and expand all comments](https://tildes.net/~tildes.official/5c8/added_buttons_to_collapse_all_reply_comments_and_expand_all_comments)

### Comment highlights

Some comments have a distinctive "stripe" down their left edge, indicating that there is something special about that comment. The colors may vary depending on the theme you are using (and if you're logged in, you can look at [the theme preview page](/settings/theme_previews) to see examples of most for a specific theme), but in general:

* Blue stripe = Comment labeled as "Exemplary" by other users
* Red stripe = New comment
* Purple stripe = Comment posted by you
* Yellow stripe = "Target" comment that was linked to specifically
* Thicker stripe, same color as text = Original Poster's (OP's) comment

### Viewing a group

To view a group on Tildes, go to the front page, and look in the sidebar for the button that says 'Browse the list of groups' (at the bottom). The 'Browse groups' page lists all groups on Tildes.

If you're already subscribed to the group, it will appear directly in the sidebar of your front page. Click on the link in the sidebar.
