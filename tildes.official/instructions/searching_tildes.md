There are two methods available for searching Tildes:

* You can search all topics on the site, or topics inside a specific group (and its sub-groups, if any) by using the search box at the top of the sidebar. Searching from the home page will search the whole site, while searching from inside a group will search inside that group (and offer the option to switch to a full-site search instead).
* You can search the topics or comments that you've posted by going to your user page, selecting Topics or Comments at the top of the page, and then using the search box at the top of the list of posts.

When searching topics, the search applies to the title, tags, and text (for text topics). Comment search only applies to the text of the comment itself.

### Search capabilities

By default, searches look for all of the entered terms. Searching for `one two` will find posts that include *both* "one" and "two".

To search for *any* of the terms instead, separate them with "or". Searching for `one or two` will find posts that include either "one" or "two" (or both).

To search for a phrase, use double-quotes around the phrase. Searching for `"one two"` will find posts that include the exact phrase "one two". Posts that include the words but not next to each other and in the specified order will not be found.

To exclude posts containing a term, put a minus symbol in front of that term. Searching for `one -two` will find posts that include "one" but *do not* include "two".

These capabilities can be combined. For example, you can search for two alternative phrases with `"one two" or "three four"` or exclude a phrase with `one -"two three"`.
