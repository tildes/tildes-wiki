**There is no Tildes mobile app**, and there is no plan to make an official app for Tildes.

To use Tildes on your tablet or smartphone, simply open your internet browser and navigate to https://tildes.net. The Tildes website will automatically adapt its display settings to fit your screen, whether large or small. Some browsers also allow you to create a shortcut on the device's home screen, which will take you directly to the website.

Reference: [The site is the main mobile interface, not an app](https://docs.tildes.net/philosophy/site-implementation#the-site-is-the-main-mobile-interface-not-an-app)
