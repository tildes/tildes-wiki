**To request an invite to the Tildes alpha, email [invites@tildes.net](mailto:invites@tildes.net).** Do not email any of the other addresses below to ask for an invite.

If you've discovered a security issue on Tildes, please disclose it responsibly by emailing [security@tildes.net](mailto:security@tildes.net). Tildes does not offer a bug bounty.

If you already have a Tildes account but have forgotten your password, please send an email (you must include your username) to [password@tildes.net](mailto:password@tildes.net).

For questions related to donations, email [donate@tildes.net](mailto:donate@tildes.net).

If you're a journalist, blogger, etc. looking for information about Tildes, email [press@tildes.net](mailto:press@tildes.net).

To report copyright infringement or other abuse on Tildes, email [abuse@tildes.net](mailto:abuse@tildes.net). Please see [the "Copyright infringement claims" section of the Terms of Use](https://docs.tildes.net/policies/terms-of-use#copyright-infringement-claims) for details about the required form of the notice and actions expected from Tildes in response.

For all other purposes, please email [contact@tildes.net](mailto:contact@tildes.net).

---

Tildes has an official Twitter account at [@TildesNet](https://twitter.com/TildesNet). It will, generally, only tweet blog posts and site updates.
