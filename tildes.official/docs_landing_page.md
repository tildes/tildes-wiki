This is the Tildes Docs site, which contains all sorts of official documentation-type pages, as well as personal musings by the creator. 

The pages are grouped by subject:

* [The philosophy of Tildes.](https://tildes.net/~tildes.official/wiki/philosophy/index) Why things are done the way they are, and the thinking behind the design and implementation of the website. 

* [Future plans for Tildes.](https://tildes.net/~tildes.official/wiki/future_plans) Where Tildes is going, and what features will be added in the future.

* [Tildes policies.](https://docs.tildes.net/policies) What rules apply to using Tildes.

* [Instructions.](https://tildes.net/~tildes.official/wiki/instructions/index) How to use Tildes. How to set up a Tildes development environment. 

* [Donate.](https://tildes.net/~tildes.official/wiki/donate) How to donate to Tildes.

* [Contact.](https://tildes.net/~tildes.official/wiki/contact) How to contact the administrator of Tildes. 
