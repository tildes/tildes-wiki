This page describes the requirements and steps to set up your own local development version of Tildes.

## Install prerequisites

You'll need to have the following things installed and the exact process for each will vary depending on your OS:

* [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
* [Vagrant](https://www.vagrantup.com/docs/installation/)
* [Virtualbox](https://www.virtualbox.org/wiki/Downloads)
* Whatever editor/IDE you want to use to edit code, as well as any other development tools (but you don't need to worry about installing Python or any of the other tech used for the site, that will all be self-contained inside the dev VM)

## Fork and clone the repository

From [the main page of the repository](https://gitlab.com/tildes/tildes), click the "Fork" button below the title to make a copy of the code to your own account. This isn't strictly necessary, but if you end up wanting to contribute any of your changes, you'll need to have a fork that you can create merge requests from.

Next you need to clone your fork to your computer. [Follow the instructions to do that here](https://docs.gitlab.com/ee/gitlab-basics/command-line-commands.html#start-working-on-your-project), making sure that you're cloning the forked repository from under your own username, not the official repository.

You'll probably also want to set things up so that you can keep your fork in sync with any changes to the official repository. To do that, follow the instructions in these two pages (make sure to change github references to gitlab):

* [Configuring a remote for a fork](https://help.github.com/articles/configuring-a-remote-for-a-fork/)
* [Syncing a fork](https://help.github.com/articles/syncing-a-fork/)

## Creating the Vagrant environment

Now that you have the code, the next step is to use Vagrant to set up the development instance.

If you're not familiar with Vagrant, it creates a virtual machine, installs Linux and all the necessary components to run the site inside that VM and configures everything, so the development environment will be completely self-contained and identical to what all other developers are using. One extremely nice benefit of this approach is that if you ever mess up your dev environment, you don't need to worry about trying to fix it. You can always just completely destroy it and recreate a new one from scratch to get back to a working state (without even losing your code changes).

From a command prompt / terminal, get into the directory you cloned the repository to (if you're in the right directory, there will be a file named "Vagrantfile"). Then run `vagrant up`. This first run will probably take a fair amount of time to complete (10 minutes or more), since it involves downloading a base Ubuntu virtual machine, setting it up, and then downloading/installing/configuring all other software needed.

After the run finishes, your VM should be configured and running. To verify, you can run `vagrant status`, which should say that the machine is running, or `vagrant ssh` to actually SSH into the VM (type `exit` to get back out).

Next, run `vagrant provision` which will check to make sure the state of the VM is correct. Assuming everything went as it should, this should finish very quickly and the summary at the end of the output should report no changed or failed states.

## Add a security exception for the dev site

The site only works over HTTPS, but your development version doesn't have a proper SSL certificate, so you need to add a security exception for it in your browser. In your browser, visit [https://localhost:4443](https://localhost:4443) - Vagrant will have set it up so that port 4443 is being forwarded into the VM, and you should see a big scary warning telling you that the connection is not secure/private.

The process will be slightly different depending on your browser, but there should be a button like "Advanced" to click where you can choose to add an exception and proceed to the site anyway. After that, you should see a page from the site being served from your development VM.

## Log in / test

At this point, your development version is running, and an initial user and group have been created for you to be able to test with. You should be able to log in with username `TestUser` and password `password`, and a group is available at [https://localhost:4443/~testing](https://localhost:4443/~testing) that you can post to.

## Set up a custom user and group (optional)

If you'd like to add a different user or group, first run `vagrant ssh` to SSH into the VM. At this point, your prompt should look like: `(tildes) vagrant@ubuntu-xenial:/opt/tildes$`.

Now, open an interactive Python shell inside the application environment by running `invoke shell`. This should put you into an IPython shell with a prompt of `In [1]:`. You now need to run the following commands (feel free to replace the username/password and group name with something different):

```python
from tildes.models.group import Group

from tildes.models.user import User

request.db_session.add(User('MyUserName', 'mypassword'))

request.db_session.add(Group('anothergroup'))

request.tm.commit()
```

After running these, you can type `exit` to get out of the shell. If you visit the [https://localhost:4443/groups](https://localhost:4443/groups) page in your browser, you should see the new one you created appear in the list. You should also be able to log in using the username/password you specified in the `User()` call.

## Set up git hooks (optional, but strongly recommended)

The repository also includes some git hooks that will ensure the tests, type checks, and style checks all pass before committing/pushing. It's not completely necessary that you use these hooks, but merge requests will not be accepted if any of these checks fail, so it's nice to have them checked automatically for you. Even if you choose not to use the hooks, make sure to run the checks manually.

If you want to use the hooks, go into the `git_hooks/` directory in the base folder of the repository, and copy (or symlink) all the files from there into the `.git/hooks/` directory (also from the base folder). Note that the '.git' directory is hidden so you may have to enable viewing hidden files/folders in order to see it. This will cause the checks to be run automatically on the relevant git events.

###  Start developing!

That's it for initial setup, you should now be able to start working on your development version. For more general information and knowledge that will be useful throughout the process, see the [General Development Info](https://tildes.net/~tildes.official/wiki/development/general_development) page.
