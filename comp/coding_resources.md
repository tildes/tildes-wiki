This wiki entry is a place to collect various coding related resources, tips, tricks, lessons, challenges, etc. shared on Tildes.

## Recurring topics

### Programming Challenges

(to add all [challenge.programming](https://tildes.net/?tag=challenge.programming) topics here)

### Code Quality Tips

An irregularly recurring "Tips" series by @Emerald_Knight on Code Quality.

https://tildes.net/?tag=programming.code_quality_tips

[Reflections on past lessons regarding code quality.](https://tildes.net/~comp/6av/reflections_on_past_lessons_regarding_code_quality)
[An informal look at the concept of reduction (alternatively: problem-solving for beginners).](https://tildes.net/~comp/6x7/an_informal_look_at_the_concept_of_reduction_alternatively_problem_solving_for_beginners)
[Light Analysis of a Recent Code Refactor](https://tildes.net/~comp/7x9/light_analysis_of_a_recent_code_refactor)
[Code Quality Tip: Wrapping external libraries.](https://tildes.net/~comp/8h8/code_quality_tip_wrapping_external_libraries)
[An Alternative Approach to Configuration Management](https://tildes.net/~comp/8tx/an_alternative_approach_to_configuration_management)
[A Brief Look at Webhook Security](https://tildes.net/~comp/9sf/a_brief_look_at_webhook_security)
[Code Quality Tip: Cyclomatic complexity in depth.](https://tildes.net/~comp/az5/code_quality_tip_cyclomatic_complexity_in_depth)
[Conceptualizing Data: Simplifying the way we think about complex data structures.](https://tildes.net/~comp/bwb/conceptualizing_data_simplifying_the_way_we_think_about_complex_data_structures)
[Code Quality Tip: The importance of understanding correctness vs. accuracy.](https://tildes.net/~comp/f0y/code_quality_tip_the_importance_of_understanding_correctness_vs_accuracy)
