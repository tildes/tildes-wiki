>Topics focused on the more technical side of computers, things of interest to programmers, sysadmins, etc.

## Notable Topics

  * [What operating system do you use?](https://tildes.net/~comp/3t3)
  * [Linux distro of choice?](https://tildes.net/~comp/2dh)
  * [Post your setup!](https://tildes.net/~comp/jp)
  * [What's the most important alias/function in your bashrc?](https://tild.es/aco)
  * [What do you think every programmer should know to do?](https://tild.es/amx)

## Where to start

Here is a list of places to get started with different programming languages/concepts

  * [Web development - freeCodeCamp](https://freecodecamp.com)
  * [Python - Automate the Boring Stuff With Python](https://automatetheboringstuff.com)
  * [Free online courses - Coursera](https://www.coursera.org)
  * [Neural Networks explained - 3Blue1Brown](https://www.youtube.com/watch?v=aircAruvnKk)




## Programming Challenges

  * [Translate 24-hour time into words](https://tildes.net/~comp/we)
  * [Undo this "Caesar" cipher](https://tildes.net/~comp/ze)
  * [Creative FizzBuzz](https://tildes.net/~comp/25q)
  * [Implementing bitwise operators](https://tildes.net/~comp/2cg)
  * [Given a triangle of numbers, find the path from top to bottom with the largest sum](https://tildes.net/~comp/2ob)
  * [Anagram checking](https://tildes.net/~comp/2yo)
  * [Markov Chain Text Generator](https://tildes.net/~comp/2zz)
  * [Construct and traverse a binary tree](https://tildes.net/~comp/387)
  * [Freestyle textual analysis](https://tildes.net/~comp/3ip)
  * [Let's build some AI](https://tildes.net/~comp/3q2)
  * [Making our own data format](https://tildes.net/~comp/4hw)
  * [Two Wizards algorithm challenge](https://tildes.net/~comp/4ya)
  * [TicTacToeBot](https://tildes.net/~comp/58b)
  * [Make a game in 1 hour](https://tildes.net/~comp/5li)
  * [Merge an arbitrary number of arrays in sorted order](https://tildes.net/~comp/628)
  * [Reverse Polish Notation Calculator](https://tildes.net/~comp/6ba)
  * [KnightBot](https://tildes.net/~comp/6g2)
  * [Compute the shortest path to visit all target spots on a grid](https://tildes.net/~comp/6vt)
  * [Counting isolated regions](https://tildes.net/~comp/77v)
  * [Dice Roller](https://tild.es/dgd)
