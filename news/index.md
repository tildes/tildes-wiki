>All the world's current events, including politics as well as analysis or opinion pieces. Please tag topics with the name of the relevant country or countries.

## Recurring topics

### This Week In Election Night, 2020

> in the interest of trying to slightly curtail the domination of politics in ~news for people who don't care for it while also consolidating discussion for people who potentially do, i think we should try one of those weekly threads that's so hip and popular on the rest of tildes, so here we go.

A summary of news and events about and around the [2020 US Presidential Election](https://tildes.net/?tag=2020_us_presidential_election), run by @alyaza.  
So far: [Week 1](https://tild.es/bpc), [Week 2](https://tild.es/c2d), [Week 3](https://tild.es/c97), [Week 4](https://tild.es/cfh), [Week 5](https://tild.es/cox), [Week 6](https://tild.es/cxe), [Week 7](https://tild.es/d56), [Week 8](https://tild.es/dcn), [Week 9](https://tild.es/dli), [Week 10](https://tild.es/duc), [Week 11](https://tild.es/e4h), [Week 12](https://tild.es/eew). [Week 13](https://tild.es/enf), [Week 14](https://tild.es/evr) and [Week 15](https://tild.es/f66).
