**NOTE: This page is a work in progress. More definitions will be added in the future.**

----

Some political groups are hard to pin down either because their definition is relatively fluid or because they overlap significantly with other political groups. In particular, this tends to be true of far-right groups; however, there are other political positions like antifascism that are similarly difficult to handle. Accordingly, this page is meant to serve as a reference for how these terms are used, mostly with respect to the tagging system used on Tildes.

Definitions are taken from the Southern Poverty Law Center and Wikipedia.
# Definitions for far-right groups
### Alt-Right
The Alternative Right, commonly known as the "alt-right," is a set of far-right ideologies, groups and individuals whose core belief is that “white identity” is under attack by multicultural forces using “political correctness” and “social justice” to undermine white people and “their” civilization.

### Neo-Nazi
Neo-Nazi groups share a hatred for Jews and a love for Adolf Hitler and Nazi Germany. While they also hate other minorities, gays and lesbians and even sometimes Christians, they perceive "the Jew" as their cardinal enemy.

### White supremacy
White supremacy or white supremacism is the racist belief that white people are superior to people of other races and therefore should be dominant over them. White supremacy has roots in scientific racism, and it often relies on pseudoscientific arguments. Like most similar movements such as neo-Nazism, white supremacists typically oppose members of other races as well as Jews.

The term is also typically used to describe a political ideology that perpetuates and maintains the social, political, historical, or institutional domination by white people (as evidenced by historical and contemporary sociopolitical structures such as the Atlantic slave trade, Jim Crow laws in the United States, and apartheid in South Africa).

### White nationalist
White nationalist groups espouse white supremacist or white separatist ideologies, often focusing on the alleged inferiority of nonwhites. Groups listed in a variety of other categories - Ku Klux Klan, neo-Confederate, neo-Nazi, racist skinhead, and Christian Identity - could also be fairly described as white nationalist.

### Far right [use only for politics]
Far-right politics are politics further on the right of the left-right spectrum than the standard political right, particularly in terms of extreme nationalism, nativist ideologies, and authoritarian tendencies.

The term is often used to describe Nazism, neo-Nazism, fascism, neo-fascism and other ideologies or organizations that feature ultranationalist, chauvinist, xenophobic, racist, anti-communist, or reactionary views. These can lead to oppression, violence, forced assimilation, ethnic cleansing, and even genocide against groups of people based on their supposed inferiority, or their perceived threat to the native ethnic group, nation, state, national religion, dominant culture or ultraconservative traditional social institutions.
