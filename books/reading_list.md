This page will contain a listing of all the texts mentioned in the "What are you reading currently" threads.  I've just started collecting data, and there is quite a bit of it, so any help is appreciated: if you're willing, just PM me @cadadr and I'll assign you a topic.  The task will involve skimming the toplevel comments on the topic, and adding the books mentioned to a spreadsheet---I (@cadadr) use LibreOffice, but anything that can export to `.ods` is fine---using a given format:

- week number
- post date (mm/yy)
- commenter user name
- comment link
- title
- author
- links (any links relevant to the title)

Data will be collected from toplevel comments only, as descending into the children comments will render the task rather hard to keep up with, and it becomes harder to decide what is just a recommendation and what is something someone is "currently reading".

I (@cadadr) have not made my mind up as to how the data will be represented eventually.  Can you make tables in Markdown?  In any case, HTML tables should work, and I (@cadadr) can write a script to generate one from the data in the spreadsheet. Ideally, books themselves get the spotlight, and poster(s) and the week(s) they appear are metadata.

Once the table for weeks so far is complete, the contents of this page will change to only include a description and a listing of the books.
