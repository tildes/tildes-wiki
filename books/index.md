>Discussion, news and articles related to books (fiction or non-fiction)

## Notable Discussion Topics

  * [Best book you've ever read?](https://tildes.net/~books/2fs/best_book_youve_ever_read)
  * [What is a book that left an impression on you?](https://tildes.net/~books/aef/what_is_a_book_that_left_an_impression_on_you)
  * [What are you planning to read this year? (2019)](https://tildes.net/~books/ab6/what_are_you_planning_to_read_this_year)

### E-Reader and Ebook Discussion

  * [Do you prefer an Ebook or a Paper book and why?](https://tildes.net/~books/1fw/do_you_prefer_an_ebook_or_a_paper_book_and_why)
  * [What is your experience with jailbreaking your e-reader?](https://tildes.net/~books/4x5/what_is_your_experience_with_jailbreaking_your_e_reader)
  * [How do you read?](https://tildes.net/~books/2e1/how_do_you_read)
  * [Where do you purchase your books online?](https://tildes.net/~books/ahe/where_do_you_purchase_your_books_online)
