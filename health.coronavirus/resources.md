A list of useful resources for information and updates on the COVID-19 coronavirus.

* [Flatten the Curve](https://www.flattenthecurve.com/) - Coronavirus (COVID-19) updates and thorough guidance to limit the spread
* [Map of Coronavirus COVID-19 global cases by the Center for Systems Science and Engineering (CSSE) at Johns Hopkins University (JHU)](https://gisanddata.maps.arcgis.com/apps/opsdashboard/index.html#/bda7594740fd40299423467b48e9ecf6)
* [Om Malik's live blog](https://om.co/2020/03/18/live-blog-stay-smart-about-covid-19-pandemic/) - "links to articles, research, commentary, and videos that come from experts — scientists, immunologists, viral disease researchers, and sources that could only be said to be biased toward logic and caution" ([previous link, for March 9 - 16](https://om.co/2020/03/09/live-blog-get-smart-about-the-coronavirus-pandemic/))
* [Coronavirus FAQ’s](https://www.abundance.org/coronavirus-faqs-by-dr-megan-murray-harvard-infectious-disease-specialist/) - Frequently asked questions on the novel Coronavirus, COVID-19,
by Harvard Infectious Disease specialist, Megan Murray, MD, MPH, ScD
* [World Health Organization: COVID-19 Myth busters](https://www.who.int/emergencies/diseases/novel-coronavirus-2019/advice-for-public/myth-busters) - Debunking common misconceptions and misinformation
* [What does COVID-19 do to the body and what's it like to have the illness?](https://www.smh.com.au/national/what-does-covid-19-do-to-the-body-and-what-s-it-like-to-have-the-illness-20200302-p5465a.html)
* [Official Public Health Agency of Canada: Coronavirus disease page](https://www.canada.ca/en/public-health/services/diseases/coronavirus-disease-covid-19.html) - Outbreak updates, travel advice, symptoms & treatment, prevention & risk, Canada's response, and live updates on number of confirmed cases in Canada.
* [Serious Eats - Food Safety and Coronavirus: A Comprehensive Guide](https://www.seriouseats.com/2020/03/food-safety-and-coronavirus-a-comprehensive-guide.html)
