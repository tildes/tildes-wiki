>Technology news and discussions, intended for a general audience. Posts requiring deep technical knowledge should go in more-specific groups (e.g. ~comp).

## Notable Topics

  * [FOSS Alternatives to popular services](/~tech/wiki/foss_alternatives)
  * [Recommended Extensions from the Tildes Community](/~tech/wiki/recommended_extensions)

## Wiki Original Content

  * [Customizing Tildes (awesome-tildes successor)](/~tech/wiki/customizing_tildes)
