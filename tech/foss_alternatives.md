### Notice 
Hi, message from @Tenar, since I started going through the alts list from the aforementioned post to make it into tables, sections, etc. 
Please feel free to help out! I was hoping to finish off that list, then see if we could go through a few other lists to fill in more stuff. Notably privacytools.io, prism-break.org, and switching.social. Maybe this list & intro need a rework to be written a bit better anyways, and mention those three (and other potential lists or sources that could be useful?). Thanks for helping on this section, and PM me on site if you'd like to discuss anything!
Quick list lest I forget: I also want to add <https://mycroft.ai> as an alexa/google home alt

## Instant Messaging and Communication 

|Proprietary App|FOSS Alternative|License|
|--|--|--|
|Telegram/Whatsapp|[Signal](http://signal.org/)|GPL 3.0|
||[Wire](https://wire.com/en/)|AGPLv3 backend, GPLv3 clients|
|Discord|[Riot](http://riot.im/)|Apache 2.0|
||[Rocket.Chat](https://rocket.chat/)|MIT|


## Search Engine 

|Proprietary App|FOSS Alternative|License|
|--|--|--|
|Google|[searx](http://searx.me/)|AGPL 3.0|

## Social Media 

|Proprietary App|FOSS Alternative|License|
|--|--|--|
|Reddit|[~](https://tildes.net)|GNU AGPLv3|
|Twitter|[Mastodon](https://joinmastodon.org/)|GNU AGPLv3|

## Browsers

|Proprietary App|FOSS Alternative|License|
|--|--|--|
|Chrome|[Firefox](https://firefox.com)|Mozilla Public License v2|
||[IceCat](https://www.gnu.org/software/gnuzilla/)|GPLv3 & MPLv2|
||[Chromium](https://github.com/chromium/chromium)|Chromium License|
||[Epiphany](https://github.com/GNOME/epiphany)|GNU GPLv2|

## General Utilities

|Proprietary App|FOSS Alternative|License|
|--|--|--|
|LastPass & other password managers|[Bitwarden](https://bitwarden.com/)|GPL 3.0|
||[KeePass](https://keepass.info/)|Depends on client|
